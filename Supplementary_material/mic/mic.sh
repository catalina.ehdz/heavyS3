#!/bin/bash

counter=0
while [ $counter -lt $1 ]
do
	./newProject DarkS3_$counter
	cp ./DarkS3/work/models/*.* ./DarkS3_$counter/work/models/
	cp ./DarkS3/DarkS3.c ./DarkS3_$counter
	cd ./DarkS3_$counter
	make main=DarkS3.c
	cd ../
	((counter++))
done
