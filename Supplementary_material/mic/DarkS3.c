
#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

#include"../include/micromegas.h"
#include"../include/micromegas_aux.h"
#include"lib/pmodel.h"

   
int
main(int argc, char ** argv)
{



    if (argc != 4 ) {
           fprintf(stderr, "\nUsage: DarkS3 [Les-Houches.in] [MyPath] [outFile]\n");
           return 0;
    }


  /*  
   *  Modify accordingly
   * 
  */
  static const char HOMEDIR[]     = "/home/catalina";
  static const char RUNDIR[]      = "/home/catalina/heavyS3";
  static const char SPhenoDIR[]   = "/home/catalina/sph";
  static const char MicroMegDIR[] = "/home/catalina/mic";
  static const char HiggsBouDIR[] = "/home/catalina/hig";


    //double PI = M_PI;

    //double uu;

    int flag, flagO, flagO2, flagwithinO;
    int ii, jj, kk, i;

    char chann[1000];
    char *nameLSP = (char *)malloc(20*sizeof(char));
    double massLSP;
    int len;


    char cdmName[10];
    int spin2, charge3, cdim;
    int nw;

    int fast=1;
    double Beps=1.E-5, cut=0.0001;
    double Omega,Xf=25; 


    int errmicro,imicro;
    //char txt[100];

    FILE * fp;
    FILE * fpHB;
    
    char strHB[1000];
    char *pHB;
    size_t lenHB;

    int nHB, HBres, HBchann, HBncom;
    float m1HB, m2HB, m3HB, m4HB, m5HB, m6HB, m7HB, obsratioHB;

    ForceUG=0;   /* to Force Unitary Gauge assign 1 */
    nPROCSS=0;   /* to switch off multiprocessor calculations */

    Omega  = -1;


    //int Zinv, Lsnp;
    //double csLim;
    int NH0,NHch;  // number of neutral and charged Higgs particles.
    double HB_result,HB_obsratio,HS_observ,HS_chi2, HS_pval;
    char HB_chan[100]={""}, HB_version[50], HS_version[50]; 

   int nHiggs=0;
   int nHch=0;
   char buff[2000];

   //int ret;

/* ------------------------------------------------------- */
/* ------------------------------------------------------- */
/* ------------------------------------------------------- */


   //printf("\n========= SLHA file input =========\n");

   flag = 1;
   flagO = 0;
   
   //printf("Initial file  \"%s\"\n",argv[1]);
   errmicro = slhaRead(argv[1],0);
   if(errmicro) flag = 0;

   //printf("\n\nerr slha: %d\n\n",errmicro);
          
   if(errmicro==-1)     { 
    //printf("Can not open the file\n"); 
    flag = 0;}
   else if(errmicro>0)  { 
      //printf("Wrong file contents at line %d\n",errmicro);
      flag = 0;}
  
   if(errmicro == 0) flag = 1;
   if(flag)
   {
    //printf("Warnings from spectrum calculator:\n");
    //nw=slhaWarnings(stdout);
    //if(nw==0) printf(" .....none\n");

    //printf("\n\nsortOdd ... %d\n\n",errmicro);
       
    errmicro=sortOddParticles(cdmName);

    if(errmicro) { 
      //printf("Can't calculate %s\n",cdmName); 
      flag=0;}
    }

   //printf("\n\nend sortOdd ... %d\n\n",errmicro);

/* ------------------------------------------------------- */
/* ------------------------------------------------------- */
/* ------------------------------------------------------- */

  if(flag)
  {
       // Find Higgses 

   for(i=0;i<nModelParticles;i++) {
    if(ModelPrtcls[i].name[0]!='~' && ModelPrtcls[i].spin2==0 
                                   && ModelPrtcls[i].cdim==1)
   { 
       if(strcmp(ModelPrtcls[i].name,ModelPrtcls[i].aname)==0)
       {  
          nHiggs++;
       } else
       {  
          nHch++;
       }   
    }
   }

   NH0 = nHiggs;
   NHch = nHch;

   char strRand[4000];

   //printf("\n\nN Higgs          =  %d\n",NH0);
   //printf("N Higgs charged  =  %d\n",NHch);

   sprintf(buff,"%s/HiggsBounds  LandH effC  %d  %d '%s'", HiggsBouDIR, NH0, NHch, argv[2]);
   //sprintf(buff,"./HiggsBounds-4.3.1/HiggsBounds  LandH effC  %d  %d ''   > hb.stdout",NH0,NHch);
   system(buff);

   sprintf(buff,"%s/HiggsBounds_results.dat", argv[2]);

   int flagERR=1;
   fpHB = fopen(buff , "r");
   if(fpHB == NULL) {
      flagERR=0;
      flag=0;
   }

    if(flagERR)
    {

    strHB[1000] = 0;

    while (fgets (strHB, 1000, fpHB) != NULL) 
    {
        pHB = strHB;

        lenHB = strlen(strHB);

        while (lenHB > 0 && (strHB[lenHB-1] == '\n' || strHB[lenHB-1] == '\r'))
            strHB[--lenHB] = 0;    /* strip newline or carriage rtn    */


        while (isspace (*pHB))    /* advance to first non-whitespace  */
            pHB++;

        /* skip lines beginning with '#' or '@' or blank lines  */
        if (*pHB == '#' || !*pHB)
            continue;
    }

        sscanf( strHB, "%d %f %f %f %f %f %f %f %d %d %f %d", &nHB, &m1HB, &m2HB, &m3HB, &m4HB, &m5HB, &m6HB, &m7HB, &HBres, &HBchann, &obsratioHB, &HBncom);

        if(HBres == 1 && obsratioHB<1)
        {
          flag = 1;
          flagO2 = 1;
          //printf("\n\nSI PASO HB.....\n\n");
        } else {
                flag = 1;
                flagO2 = 0;
                //printf("\n\nNOOOOOOOOOO PASO HB.....\n\n");
               }

    }  // flagERR


   }
/* ------------------------------------------------------- */
/* ------------------------------------------------------- */
/* ------------------------------------------------------- */


    if(flag)
    {
     qNumbers(cdmName,&spin2, &charge3, &cdim);
     //printf("\nDark matter candidate is '%s' with spin=%d/2  mass=%.2E\n", cdmName, spin2, Mcdm); 
    

     if(charge3) { 
      //printf("Dark Matter has electric charge %d/3\n",charge3); 
      flag = 0;}
    }

    if(flag)
    {
    if(cdim!=1) { 
      //printf("Dark Matter is a color particle\n"); 
      flag=0;}
    }

    if(flag)
    {
  
// to exclude processes with virtual W/Z in DM   annihilation      
    VZdecay=0; VWdecay=0; cleanDecayTable(); 
    
// to include processes with virtual W/Z  also  in co-annihilation 
//   VZdecay=2; VWdecay=2; cleanDecayTable(); 
    
    //printf("\n==== Calculation of relic density =====\n");  


    Omega=darkOmega(&Xf,fast,Beps);
    //printf("Xf=%.2e Omega=%.2e\n",Xf,Omega);
    //if(Omega>0)printChannels(Xf,cut,Beps,1,stdout);
   

// to restore default switches  

    VZdecay=1; VWdecay=1; cleanDecayTable();

    if( Omega > (0.12 - 0.012)  &&  Omega < (0.12 + 0.012) )
    {
     flagwithinO = 1;
    } else {flagwithinO = 0;}

    if( Omega < (0.12 + 0.012) ){
      flagO = 1;
    } else{flagO = 0;}

    } //end if flag

/*  End relic  */

/* ------------------------------------------------------- */
/* ------------------------------------------------------- */
/* ------------------------------------------------------- */

    strcpy(nameLSP, nextOdd(0, &massLSP));
    fp = fopen (argv[3],"w");
    fprintf(fp,"%d %d %d %d %lE %lE\n", flag, flagwithinO, flagO, flagO2, massLSP, Omega);
    fclose(fp);

    //printf("\n\nfreeing nameLSP\n\n");

    free(nameLSP);

/* ------------------------------------------------------- */
/* ------------------------------------------------------- */
/* ------------------------------------------------------- */ 

/* End main() */

return 0;

}












































