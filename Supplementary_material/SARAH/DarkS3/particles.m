(* SARAH generated template for particles.m file for 4HDM_S3 *)
(* File created at 15:59 on 22.2.2018  *) 

(* IMPORTANT: *)
(* check/adjust in particular the lines which contain "CHECK!" *)
(* the correct information is needed there to have correct results! *)


(* ###############            EWSB       ################# *) 

ParticleDefinitions[EWSB] = { 
 
(* ----------- Already defined particles in existing particles.m -------------- *) 



{Fd,{ 
     Description -> "Down-Quarks", 
     FeynArtsNr -> 4, 
     LaTeX -> "d", 
     Mass -> {0., 0., 4.2}, 
     OutputName -> "d", 
     PDG -> {1, 3, 5}, 
     ElectricCharge -> -1/3, 
     Width -> {0, 0, 0}}}, 

{Fe,{ 
     Description -> "Leptons", 
     FeynArtsNr -> 2, 
     LaTeX -> "e", 
     Mass -> {0., 0., 1.776}, 
     OutputName -> "e", 
     PDG -> {11, 13, 15}, 
     ElectricCharge -> -1, 
     Width -> {0, 0, 0}}}, 

{Fu,{ 
     Description -> "Up-Quarks", 
     FeynArtsNr -> 3, 
     LaTeX -> "u", 
     Mass -> {0., 0., 171.2}, 
     OutputName -> "u", 
     PDG -> {2, 4, 6}, 
     ElectricCharge -> 2/3, 
     Width -> {0, 0, 1.51}}}, 

{Fv,{ 
     Description -> "Neutrinos", 
     FeynArtsNr -> 1, 
     LaTeX -> "\\nu", 
     Mass -> {0, 0, 0}, 
     OutputName -> "nu", 
     PDG -> {12, 14, 16}, 
     ElectricCharge -> 0, 
     Width -> {0, 0, 0}}}, 

{gG,{ 
     Description -> "Gluon Ghost", 
     FeynArtsNr -> 5, 
     LaTeX -> "\\eta^G", 
     Mass -> 0, 
     OutputName -> "gG", 
     PDG -> {0}, 
     ElectricCharge -> 0, 
     Width -> 0}}, 

{gP,{ 
     Description -> "Photon Ghost", 
     FeynArtsNr -> 1, 
     LaTeX -> "\\eta^{\\gamma}", 
     Mass -> 0, 
     OutputName -> "gA", 
     PDG -> {0}, 
     ElectricCharge -> 0, 
     Width -> 0}}, 

{gWp,{ 
     Description -> "Positive W+ - Boson Ghost", 
     FeynArtsNr -> 3, 
     LaTeX -> "\\eta^+", 
     Mass -> Mass[VWp],
     OutputName -> "gWp", 
     PDG -> {0}, 
     ElectricCharge -> 1, 
     Width -> Automatic}}, 

{gWpC,{ 
     Description -> "Negative W+ - Boson Ghost", 
     FeynArtsNr -> 4, 
     LaTeX -> "\\eta^-", 
     Mass -> Mass[VWp],
     OutputName -> "gWC", 
     PDG -> {0}, 
     ElectricCharge -> -1, 
     Width -> Automatic}}, 

{gZ,{ 
     Description -> "Z-Boson Ghost", 
     FeynArtsNr -> 2, 
     LaTeX -> "\\eta^Z", 
     Mass -> Mass[VZ], 
     OutputName -> "gZ", 
     PDG -> {0}, 
     ElectricCharge -> 0, 
     Width -> Automatic}}, 

{Hap,{ 
     FeynArtsNr -> 6, 
     LaTeX -> {"H^+_a","H^-_a"}, 
     Mass -> {LesHouches}, 
     OutputName -> {"HHp","HHm"}, 
     PDG -> {9900037}, 
     ElectricCharge -> 1, 
     Width -> {External}}}, 

{hha,{ 
     FeynArtsNr -> 8, 
     LaTeX -> "h_a", 
     Mass -> LesHouches, 
     OutputName -> "hha", 
     PDG -> {9900035}, 
     ElectricCharge -> 0, 
     Width -> Automatic}}, 


{Ah,{ 
     Description -> "Pseudo-Scalar Higgs", 
     FeynArtsNr -> 2, 
     LaTeX -> "A^0", 
     Mass -> {0, LesHouches}, 
     OutputName -> "Ah", 
     PDG -> {0, 36, 9900026}, 
     ElectricCharge -> 0, 
     Width -> {0, External}}}, 

(*
{Ah[{1}],{ 
     LaTeX -> "G^0", 
     Mass -> {0}, 
     OutputName -> "G0", 
     PDG -> {0}, 
     ElectricCharge -> 0, 
     Width -> {0}}}, 

{Ah[{2}],{ 
     LaTeX -> "A", 
     Mass -> {LesHouches}, 
     OutputName -> "A", 
     PDG -> {36}, 
     ElectricCharge -> 0, 
     Width -> {External}}}, 

{Ah[{3}],{ 
     LaTeX -> "A3", 
     Mass -> {LesHouches}, 
     OutputName -> "A3", 
     PDG -> {9900026}, 
     ElectricCharge -> 0, 
     Width -> {External}}}, 
*)

{HhSM,{ 
     Description -> "Higgs", 
     FeynArtsNr -> 1, 
     LaTeX -> "h", 
     Mass -> LesHouches, 
     OutputName -> "h", 
     PDG -> {25, 35, 9900025}, 
     ElectricCharge -> 0, 
     Width -> Automatic}},
 

(*
{HhSM[{1}],{ 
     FeynArtsNr -> 1, 
     LaTeX -> "h", 
     Mass -> LesHouches, 
     OutputName -> "h", 
     PDG -> {25}, 
     ElectricCharge -> 0, 
     Width -> Automatic}}, 


{HhSM[{2}],{ 
     FeynArtsNr -> 2, 
     LaTeX -> "H", 
     Mass -> LesHouches, 
     OutputName -> "H", 
     PDG -> {35}, 
     ElectricCharge -> 0, 
     Width -> Automatic}}, 


{HhSM[{3}],{ 
     FeynArtsNr -> 3, 
     LaTeX -> "H3", 
     Mass -> LesHouches, 
     OutputName -> "H3", 
     PDG -> {9900025}, 
     ElectricCharge -> 0, 
     Width -> Automatic}}, 
*)



{Hp,{ 
     Description -> "Charged Higgs", 
     FeynArtsNr -> 3, 
     LaTeX -> {"H^+","H^-"}, 
     Mass -> {0, LesHouches}, 
     OutputName -> {"Hp","Hm"}, 
     PDG -> {0, 37, 9900027}, 
     ElectricCharge -> 1, 
     Width -> {0, External}}}, 

(*
{Hp[{1}],{ 
     LaTeX -> "G^w", 
     Mass -> {0}, 
     OutputName -> "Gw", 
     PDG -> {0}, 
     ElectricCharge -> 1, 
     Width -> {0}}}, 

{Hp[{2}],{ 
     LaTeX -> "H^\\pm", 
     Mass -> {LesHouches}, 
     OutputName -> "Hp", 
     PDG -> {37}, 
     ElectricCharge -> 1, 
     Width -> {External}}}, 

{Hp[{3}],{ 
     LaTeX -> "H_3^\\pm", 
     Mass -> {LesHouches}, 
     OutputName -> "Hp3", 
     PDG -> {9900027}, 
     ElectricCharge -> 1, 
     Width -> {External}}}, 
*)

{siga,{ 
     FeynArtsNr -> 10, 
     LaTeX -> "A^0_a", 
     Mass -> {LesHouches}, 
     OutputName -> "AAha", 
     PDG -> {9900036}, 
     ElectricCharge -> 0, 
     Width -> {External}}}, 

{VG,{ 
     Description -> "Gluon", 
     FeynArtsNr -> 5, 
     LaTeX -> "g", 
     Mass -> 0, 
     OutputName -> "g", 
     PDG -> {21}, 
     ElectricCharge -> 0, 
     Width -> 0}}, 

{VP,{ 
     Description -> "Photon", 
     FeynArtsNr -> 1, 
     LaTeX -> "\\gamma", 
     Mass -> 0, 
     OutputName -> "A", 
     PDG -> {22}, 
     ElectricCharge -> 0, 
     Width -> 0}}, 

{VWp,{ 
     Description -> "W+ - Boson", 
     Goldstone -> Hp[{1}], 
     FeynArtsNr -> 3, 
     LaTeX -> {"W^+","W^-"}, 
     Mass -> Dependence, 
     MassDependence -> Sqrt[Mass[VZ]^2/2 + Sqrt[-((Pi*Mass[VZ]^2)/(Sqrt[2]*aEWinv*Gf)) + Mass[VZ]^4/4]], 
     OutputName -> {"Wp","Wm"}, 
     PDG -> {24}, 
     ElectricCharge -> 1, 
     Width -> 2.141}}, 

{VZ,{ 
     Description -> "Z-Boson", 
     Goldstone -> Ah[{1}], 
     FeynArtsNr -> 2, 
     LaTeX -> "Z", 
     Mass -> 91.1876, 
     OutputName -> "Z", 
     PDG -> {23}, 
     ElectricCharge -> 0, 
     Width -> 2.4952}} 
}; 




(* ###############            GaugeES       ################# *) 

ParticleDefinitions[GaugeES] = { 
 
(* ----------- Already defined particles in existing particles.m -------------- *) 

{dL,{ 
     FeynArtsNr -> 109, 
     LaTeX -> "dl", 
     Mass -> LesHouches, 
     OutputName -> "dl", 
     PDG -> {45, 46, 47}, 
     Width -> Automatic}}, 

{dR,{ 
     FeynArtsNr -> 111, 
     LaTeX -> "dr", 
     Mass -> LesHouches, 
     OutputName -> "dr", 
     PDG -> {51, 52, 53}, 
     Width -> Automatic}}, 

{eL,{ 
     FeynArtsNr -> 113, 
     LaTeX -> "el", 
     Mass -> LesHouches, 
     OutputName -> "el", 
     PDG -> {57, 58, 59}, 
     Width -> Automatic}}, 

{eR,{ 
     FeynArtsNr -> 115, 
     LaTeX -> "er", 
     Mass -> LesHouches, 
     OutputName -> "er", 
     PDG -> {63, 64, 65}, 
     Width -> Automatic}}, 

{gB,{ 
     Description -> "B-Boson Ghost", 
     FeynArtsNr -> 1, 
     LaTeX -> "\\eta^B", 
     Mass -> 0, 
     OutputName -> "gB", 
     PDG -> {0}, 
     Width -> 0}}, 

{gG,{ 
     Description -> "Gluon Ghost", 
     FeynArtsNr -> 5, 
     LaTeX -> "\\eta^G", 
     Mass -> 0, 
     OutputName -> "gG", 
     PDG -> {0}, 
     ElectricCharge -> 0, 
     Width -> 0}}, 

{gWB,{ 
     Description -> "W-Boson Ghost", 
     FeynArtsNr -> 2, 
     LaTeX -> "\\eta^W", 
     Mass -> 0, 
     OutputName -> "gW", 
     PDG -> {0}   (* adjusted number of PDGs to number of generations  *), 
     Width -> 0}}, 

{H10,{ 
     FeynArtsNr -> 106, 
     LaTeX -> "H_{10}", 
     Mass -> LesHouches, 
     OutputName -> "H10", 
     PDG -> {23}, 
     Width -> Automatic}}, 

{H1p,{ 
     FeynArtsNr -> 105, 
     LaTeX -> "H_{1p}", 
     Mass -> LesHouches, 
     OutputName -> "H1p", 
     PDG -> {25}, 
     Width -> Automatic}}, 

{H20,{ 
     FeynArtsNr -> 108, 
     LaTeX -> "H_{20}", 
     Mass -> LesHouches, 
     OutputName -> "H20", 
     PDG -> {27}, 
     Width -> Automatic}}, 

{H2p,{ 
     FeynArtsNr -> 107, 
     LaTeX -> "H_{2p}", 
     Mass -> LesHouches, 
     OutputName -> "H2p", 
     PDG -> {29}, 
     Width -> Automatic}}, 

{Ha0,{ 
     FeynArtsNr -> 104, 
     LaTeX -> "H_{a0}", 
     Mass -> LesHouches, 
     OutputName -> "Ha0", 
     PDG -> {31}, 
     Width -> Automatic}}, 

{Hap,{ 
     FeynArtsNr -> 6, 
     LaTeX -> {"H^+_a","H^-_a"}, 
     Mass -> {LesHouches}, 
     OutputName -> {"HHp","HHm"}, 
     PDG -> {9900037}, 
     ElectricCharge -> 1, 
     Width -> {External}}}, 

{Hs0,{ 
     FeynArtsNr -> 102, 
     LaTeX -> "H_{s0}", 
     Mass -> LesHouches, 
     OutputName -> "Hs0", 
     PDG -> {35}, 
     Width -> Automatic}}, 

{Hsp,{ 
     FeynArtsNr -> 101, 
     LaTeX -> "H_{sp}", 
     Mass -> LesHouches, 
     OutputName -> "Hsp", 
     PDG -> {37}, 
     Width -> Automatic}}, 

{uL,{ 
     FeynArtsNr -> 117, 
     LaTeX -> "ul", 
     Mass -> LesHouches, 
     OutputName -> "ul", 
     PDG -> {69, 70, 71}, 
     Width -> Automatic}}, 

{uR,{ 
     FeynArtsNr -> 119, 
     LaTeX -> "ur", 
     Mass -> LesHouches, 
     OutputName -> "ur", 
     PDG -> {75, 76, 77}, 
     Width -> Automatic}}, 

{VB,{ 
     Description -> "B-Boson", 
     FeynArtsNr -> 1, 
     LaTeX -> "B", 
     Mass -> 0, 
     OutputName -> "B", 
     PDG -> {0}, 
     Width -> 0}}, 

{VG,{ 
     Description -> "Gluon", 
     FeynArtsNr -> 5, 
     LaTeX -> "g", 
     Mass -> 0, 
     OutputName -> "g", 
     PDG -> {21}, 
     ElectricCharge -> 0, 
     Width -> 0}}, 

{vL,{ 
     FeynArtsNr -> 121, 
     LaTeX -> "v_{l}", 
     Mass -> LesHouches, 
     OutputName -> "vl", 
     PDG -> {81, 82, 83}, 
     Width -> Automatic}}, 

{VWB,{ 
     Description -> "W-Bosons", 
     FeynArtsNr -> 2, 
     LaTeX -> "W", 
     Mass -> 0, 
     OutputName -> "W", 
     PDG -> {0, 40, 41}, 
     Width -> 0}} 
}; 




(* ###############            Weyl Spinors and intermediate states       ################# *) 

WeylFermionAndIndermediate = { 
 
(* ----------- Already defined particles in existing particles.m -------------- *) 

{dL,{ 
     LaTeX -> "d_L"}}, 

{uL,{ 
     LaTeX -> "u_L"}}, 

{eL,{ 
     LaTeX -> "e_L"}}, 

{vL,{ 
     LaTeX -> "\\nu_L"}}, 

{dR,{ 
     LaTeX -> "d_R"}}, 

{uR,{ 
     LaTeX -> "u_R"}}, 

{eR,{ 
     LaTeX -> "e_R"}}, 

{DL,{ 
     LaTeX -> "D_L"}}, 

{DR,{ 
     LaTeX -> "D_R"}}, 

{UL,{ 
     LaTeX -> "U_L"}}, 

{UR,{ 
     LaTeX -> "U_R"}}, 

{EL,{ 
     LaTeX -> "E_L"}}, 

{ER,{ 
     LaTeX -> "E_R"}}, 

{sigs,{ 
     LaTeX -> "Ah_s"}}, 

{hhs,{ 
     LaTeX -> "hh_s"}}, 

{sig1,{ 
     LaTeX -> "Ah_1"}}, 

{hh1,{ 
     LaTeX -> "hh_1"}}, 

{sig2,{ 
     LaTeX -> "Ah_2"}}, 

{hh2,{ 
     LaTeX -> "hh_2"}}, 

{d,{ 
     LaTeX -> "d"}}, 

{e,{ 
     LaTeX -> "e"}}, 

{H1,{ 
     LaTeX -> "h1"}}, 

{H2,{ 
     LaTeX -> "h2"}}, 

{Ha,{ 
     LaTeX -> "ha"}}, 

{Hs,{ 
     LaTeX -> "hs"}}, 

{l,{ 
     LaTeX -> "l"}}, 

{q,{ 
     LaTeX -> "q"}}, 

{u,{ 
     LaTeX -> "u"}}, 

{VB,{ 
     LaTeX -> "B"}}, 

{VG,{ 
     LaTeX -> "G"}}, 

{VWB,{ 
     LaTeX -> "WB"}} 
}; 




