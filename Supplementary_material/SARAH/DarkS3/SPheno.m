(* ::Package:: *)

OnlyLowEnergySPheno = True;



MINPAR={{1,Lambda1Input},
        {2,Lambda2Input},
        {3,Lambda3Input},
        {4,Lambda4Input},
        {5,Lambda5Input},
        {6,Lambda6Input},
        {7,Lambda7Input},
        {8,Lambda8Input},
        {10,Lambda10Input},
        {11,Lambda11Input},
        {12,Lambda12Input},
        {13,Lambda13Input},
        {14,Lambda14Input},
        {22,Mu2Input},
        {30,TanBeta}};



RealParameters = {TanBeta};

ParametersToSolveTadpoles = {mu0,mu1};

Tad1Loop[3]=Tad1Loop[2]/Sqrt[3];


BoundaryLowScaleInput={
 {lam1,Lambda1Input},
 {lam2,Lambda2Input},
 {lam3,Lambda3Input},
 {lam4,Lambda4Input},
 {lam5,Lambda5Input},
 {lam6,Lambda6Input},
 {lam7,Lambda7Input},
 {lam8,Lambda8Input},
 {lam10,Lambda10Input},
 {lam11,Lambda11Input},
 {lam12,Lambda12Input},
 {lam13,Lambda13Input},
 {lam14,Lambda14Input},
 {mu2, Mu2Input}
};





DEFINITION[MatchingConditions]= {
 {v1,vSM*Cos[ArcTan[TanBeta]]},
 {v2,vSM*Sin[ArcTan[TanBeta]]/2},
 {Ye, YeSM*vSM/v1},
 {Yd, YdSM*vSM/v1},
 {Yu, YuSM*vSM/v1},
 {g1, g1SM},
 {g2, g2SM},
 {g3, g3SM}
 };


AddTreeLevelUnitarityLimits=True;


ListDecayParticles = {Fu,Fe,Fd,VZ,VWp,HhSM,Ah,Hp};
ListDecayParticles3B = {{Fu,"Fu.f90"},{Fe,"Fe.f90"},{Fd,"Fd.f90"}};



DefaultInputValues ={Lambda1Input -> 7.766082, 
                     Lambda2Input -> 2.475097, 
                     Lambda3Input -> -7.375995, 
                     Lambda4Input -> -2.471215, 
                     Lambda5Input -> 8.402460,  
                     Lambda6Input -> 9.959566, 
                     Lambda7Input -> 1.800647, 
                     Lambda8Input -> 7.076688, 
                     Lambda10Input -> 11.899794, 
                     Lambda11Input -> 6.242374, 
                     Lambda12Input -> -3.894172, 
                     Lambda13Input -> 6.923109, 
                     Lambda14Input -> 5.062383, 
                     Mu2Input -> -2.717574, 
                     TanBeta-> 14.033698};


RenConditionsDecays={
{dCosTW, 1/2*Cos[ThetaW] * (PiVWp/(Mvwp^2) - PiVZ/(mVZ^2)) },
{dSinTW, -dCosTW/Tan[ThetaW]},
{dg2, 1/2*g2*(derPiVPheavy0 + PiVPlightMZ/MVZ^2 - (-(PiVWp/Mvwp^2) + PiVZ/MVZ^2)/Tan[ThetaW]^2 + (2*PiVZVP*Tan[ThetaW])/MVZ^2)  },
{dg1, dg2*Tan[ThetaW]+g2*dSinTW/Cos[ThetaW]- dCosTW*g2*Tan[ThetaW]/Cos[ThetaW]}
};



