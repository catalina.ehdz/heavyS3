(* ----------------------------------------------------------------------------- *) 
(* This model file was automatically created by SARAH version4.14.3  *) 
(* SARAH References: arXiv:0806.0538, 0909.2863, 1002.0840, 1207.0906, 1309.7223 *) 
(* (c) Florian Staub, 2013  *) 
(* ----------------------------------------------------------------------------- *) 
(* File created at 18:06 on 25.1.2020  *) 
(* --------------------------------------------------------------------------------- *) 
(* --------------------------------------------------------------------------------- *) 
(* Modified manually to include specific particle labels (C. Espinoza) on 25.1.2020  *) 
(* --------------------------------------------------------------------------------- *) 
(* --------------------------------------------------------------------------------- *) 
 
 
IndexRange[  Index[Colour]  ] =NoUnfold[Range[3]]; 
IndexStyle[  Index[Colour, i_Integer ] ] := Greek[i];  
IndexRange[  Index[I3Gen]  ] =Range[3]; 
IndexStyle[  Index[I3Gen, i_Integer ] ] := Alph[ 8+i];  
IndexRange[  Index[Gluon]  ] =NoUnfold[Range[8]]; 
IndexStyle[  Index[Gluon, i_Integer ] ] := Alph[ 8+i];  

 
(* Definitions for trigonometric functions  
Sin[ThetaW]: STW
Sin[2*ThetaW]: S2TW
Sin[TW]: STW
Sin[2*TW]: S2TW
Cos[ThetaW]: CTW
Cos[2*ThetaW]: C2TW
Cos[TW]: CTW
Cos[2*TW]: C2TW
*) 
 
Conjugate[STW] ^= STW
Conjugate[S2TW] ^= S2TW
Conjugate[STW] ^= STW
Conjugate[S2TW] ^= S2TW
Conjugate[CTW] ^= CTW
Conjugate[C2TW] ^= C2TW
Conjugate[CTW] ^= CTW
Conjugate[C2TW] ^= C2TW
 
 
Lam[a_,b_,c_]:=2*SUNT[a,b,c]; 
fSU3[a_,b_,c_]:=SUNF[a,b,c]; 
LambdaProd[a_,b_][c_,d_]:=4*SUNT[a,b,c,d]; 
 
 
M$ClassesDescription= {
S[6] == {SelfConjugate -> False,
Indices -> {},
Mass -> MassHap,
PropagatorLabel->ComposedChar["H", "a", "\\pm"],
PropagatorType -> ScalarDash,
PropagatorArrow -> Forward},

 
S[10] == {SelfConjugate -> True,
Indices -> {},
Mass -> Masssiga,
PropagatorLabel->ComposedChar["A", Null,"a"],
PropagatorType -> ScalarDash,
PropagatorArrow -> None},

 
S[8] == {SelfConjugate -> True,
Indices -> {},
Mass -> Masshha,
PropagatorLabel->ComposedChar["h", Null, "a"],
PropagatorType -> ScalarDash,
PropagatorArrow -> None},

 
S[1] == {SelfConjugate -> True,
Indices -> {Index[I3Gen]},
Mass -> MassHhSM,
PropagatorLabel->ComposedChar["h",Index[I3Gen]],
PropagatorType -> ScalarDash,
PropagatorArrow -> None},

 
S[2] == {SelfConjugate -> True,
Indices -> {Index[I3Gen]},
Mass -> MassAh,
PropagatorLabel->ComposedChar["A",Index[I3Gen],"0"],
PropagatorType -> ScalarDash,
PropagatorArrow -> None},

 
S[3] == {SelfConjugate -> False,
Indices -> {Index[I3Gen]},
Mass -> MassHp,
PropagatorLabel->ComposedChar["H",Index[I3Gen],"+"],
PropagatorType -> ScalarDash,
PropagatorArrow -> Forward},

 
F[1] == {SelfConjugate -> False,
Indices -> {Index[I3Gen]},
Mass -> MassFv,
PropagatorLabel->ComposedChar["\\nu",Index[I3Gen]],
PropagatorType -> Straight,
PropagatorArrow -> Forward},

 
F[4] == {SelfConjugate -> False,
Indices -> {Index[I3Gen], Index[Colour]},
Mass -> MassFd,
PropagatorLabel->ComposedChar["d",Index[I3Gen],Index[Colour]],
PropagatorType -> Straight,
PropagatorArrow -> Forward},

 
F[3] == {SelfConjugate -> False,
Indices -> {Index[I3Gen], Index[Colour]},
Mass -> MassFu,
PropagatorLabel->ComposedChar["u",Index[I3Gen],Index[Colour]],
PropagatorType -> Straight,
PropagatorArrow -> Forward},

 
F[2] == {SelfConjugate -> False,
Indices -> {Index[I3Gen]},
Mass -> MassFe,
PropagatorLabel->ComposedChar["e",Index[I3Gen]],
PropagatorType -> Straight,
PropagatorArrow -> Forward},

 
V[5] == {SelfConjugate -> True,
Indices -> {Index[Gluon]},
Mass -> 0,
PropagatorLabel->ComposedChar["G",Index[Gluon]],
PropagatorType -> Sine,
PropagatorArrow -> None},

 
V[1] == {SelfConjugate -> True,
Indices -> {},
Mass -> 0,
PropagatorLabel->ComposedChar["\\gamma"],
PropagatorType -> Sine,
PropagatorArrow -> None},

 
V[2] == {SelfConjugate -> True,
Indices -> {},
Mass -> MassVZ,
PropagatorLabel->ComposedChar["Z"],
PropagatorType -> Sine,
PropagatorArrow -> None},

 
V[3] == {SelfConjugate -> False,
Indices -> {},
Mass -> MassVWp,
PropagatorLabel->ComposedChar["W","+"],
PropagatorType -> Sine,
PropagatorArrow -> Forward},

 
U[5] == {SelfConjugate -> False,
Indices -> {Index[Gluon]},
Mass -> 0,
PropagatorLabel->ComposedChar["\\eta",Index[Gluon],"G"],
PropagatorType -> GhostDash,
PropagatorArrow -> Forward},

 
U[1] == {SelfConjugate -> False,
Indices -> {},
Mass -> 0,
PropagatorLabel->ComposedChar["\\eta","\\gamma"],
PropagatorType -> GhostDash,
PropagatorArrow -> Forward},

 
U[2] == {SelfConjugate -> False,
Indices -> {},
Mass -> MassVZ,
PropagatorLabel->ComposedChar["\\eta","Z"],
PropagatorType -> GhostDash,
PropagatorArrow -> Forward},

 
U[3] == {SelfConjugate -> False,
Indices -> {},
Mass -> MassVWp,
PropagatorLabel->ComposedChar["\\eta","+"],
PropagatorType -> GhostDash,
PropagatorArrow -> Forward},

 
U[4] == {SelfConjugate -> False,
Indices -> {},
Mass -> MassVWp,
PropagatorLabel->ComposedChar["\\eta","-"],
PropagatorType -> GhostDash,
PropagatorArrow -> Forward}
 
}

 
MassFd[gen_, y_] = MassFd[gen]
MassFu[gen_, y_] = MassFu[gen]


TheLabel[ S[1, {1}] ] = "H"; 
TheLabel[ S[1, {2}] ] = ComposedChar["H", "3"];  
TheLabel[ S[1, {3}] ] = "h"; 
TheLabel[ S[2, {1}] ] = ComposedChar["G", Null, "0"];  
TheLabel[ S[2, {2}] ] = ComposedChar["A", "3"];  
TheLabel[ S[2, {3}] ] = "A"; 
TheLabel[ S[3, {1}] ] = ComposedChar["G", Null, "w"];  
TheLabel[ S[3, {2}] ] = ComposedChar["H", "3",  "\\pm"];  
TheLabel[ S[3, {3}] ] = ComposedChar["H", Null, "\\pm"];




GaugeXi[S[6,___]] = 1 
GaugeXi[S[10,___]] = 1 
GaugeXi[S[8,___]] = 1 
GaugeXi[S[1,___]] = 1 
GaugeXi[S[2,{a_Integer}]] = 1 /; a > 1 
GaugeXi[S[2,1]] = GaugeXi[VZ] /; a > 1 
GaugeXi[S[3,{a_Integer}]] = 1 /; a > 1 
GaugeXi[S[3,1]] = GaugeXi[VWp] /; a > 1 


GaugeXi[V[5,___]] = GaugeXi[G]
GaugeXi[V[1,___]] = GaugeXi[P]
GaugeXi[V[2,___]] = GaugeXi[Z]
GaugeXi[V[3,___]] = GaugeXi[Wp]


M$CouplingMatrices= {
C[V[5, {ct1}], V[5, {ct2}], V[5, {ct3}], V[5, {ct4}]] == {{(-I)*g3^2*IndexSum[fSU3[ct1, ct4, j1]*fSU3[ct2, ct3, j1], {j1, 8}] - I*g3^2*IndexSum[fSU3[ct1, ct3, j1]*fSU3[ct2, ct4, j1], {j1, 8}]}, {I*g3^2*IndexSum[fSU3[ct1, ct4, j1]*fSU3[ct2, ct3, j1], {j1, 8}] - I*g3^2*IndexSum[fSU3[ct1, ct2, j1]*fSU3[ct3, ct4, j1], {j1, 8}]}, {I*g3^2*IndexSum[fSU3[ct1, ct3, j1]*fSU3[ct2, ct4, j1], {j1, 8}] + I*g3^2*IndexSum[fSU3[ct1, ct2, j1]*fSU3[ct3, ct4, j1], {j1, 8}]}},
 C[-V[3], V[1], V[1], V[3]] == {{I*g2^2*STW^2}, {I*g2^2*STW^2}, {(-2*I)*g2^2*STW^2}},
 C[-V[3], V[1], V[3], V[2]] == {{I*CTW*g2^2*STW}, {(-2*I)*CTW*g2^2*STW}, {I*CTW*g2^2*STW}},
 C[-V[3], -V[3], V[3], V[3]] == {{(2*I)*g2^2}, {(-I)*g2^2}, {(-I)*g2^2}},
 C[-V[3], V[3], V[2], V[2]] == {{(-2*I)*CTW^2*g2^2}, {I*CTW^2*g2^2}, {I*CTW^2*g2^2}},
 C[S[2, {gt1}], S[2, {gt2}], -V[3], V[3]] == {{(I/2)*g2^2*ZA[gt1, 1]*ZA[gt2, 1] + (I/2)*g2^2*ZA[gt1, 2]*ZA[gt2, 2] + (I/2)*g2^2*ZA[gt1, 3]*ZA[gt2, 3]}},
 C[S[2, {gt1}], S[2, {gt2}], V[2], V[2]] == {{(I/4)*g1^2*ZA[gt1, 1]*ZA[gt2, 1] - (I/4)*CTW^2*g1^2*ZA[gt1, 1]*ZA[gt2, 1] + (I/4)*g2^2*ZA[gt1, 1]*ZA[gt2, 1] + (I/4)*CTW^2*g2^2*ZA[gt1, 1]*ZA[gt2, 1] + I*CTW*g1*g2*STW*ZA[gt1, 1]*ZA[gt2, 1] + (I/4)*g1^2*STW^2*ZA[gt1, 1]*ZA[gt2, 1] - (I/4)*g2^2*STW^2*ZA[gt1, 1]*ZA[gt2, 1] + (I/4)*g1^2*ZA[gt1, 2]*ZA[gt2, 2] - (I/4)*CTW^2*g1^2*ZA[gt1, 2]*ZA[gt2, 2] + (I/4)*g2^2*ZA[gt1, 2]*ZA[gt2, 2] + (I/4)*CTW^2*g2^2*ZA[gt1, 2]*ZA[gt2, 2] + I*CTW*g1*g2*STW*ZA[gt1, 2]*ZA[gt2, 2] + (I/4)*g1^2*STW^2*ZA[gt1, 2]*ZA[gt2, 2] - (I/4)*g2^2*STW^2*ZA[gt1, 2]*ZA[gt2, 2] + (I/4)*g1^2*ZA[gt1, 3]*ZA[gt2, 3] - (I/4)*CTW^2*g1^2*ZA[gt1, 3]*ZA[gt2, 3] + (I/4)*g2^2*ZA[gt1, 3]*ZA[gt2, 3] + (I/4)*CTW^2*g2^2*ZA[gt1, 3]*ZA[gt2, 3] + I*CTW*g1*g2*STW*ZA[gt1, 3]*ZA[gt2, 3] + (I/4)*g1^2*STW^2*ZA[gt1, 3]*ZA[gt2, 3] - (I/4)*g2^2*STW^2*ZA[gt1, 3]*ZA[gt2, 3]}},
 C[S[2, {gt1}], S[3, {gt2}], -V[3], V[1]] == {{(CTW*g1*g2*ZA[gt1, 1]*ZP[gt2, 1])/2 + (CTW*g1*g2*ZA[gt1, 2]*ZP[gt2, 2])/2 + (CTW*g1*g2*ZA[gt1, 3]*ZP[gt2, 3])/2}},
 C[S[2, {gt1}], S[3, {gt2}], -V[3], V[2]] == {{-(g1*g2*STW*ZA[gt1, 1]*ZP[gt2, 1])/2 - (g1*g2*STW*ZA[gt1, 2]*ZP[gt2, 2])/2 - (g1*g2*STW*ZA[gt1, 3]*ZP[gt2, 3])/2}},
 C[S[2, {gt1}], -S[3, {gt2}], V[1], V[3]] == {{-(CTW*g1*g2*ZA[gt1, 1]*ZP[gt2, 1])/2 - (CTW*g1*g2*ZA[gt1, 2]*ZP[gt2, 2])/2 - (CTW*g1*g2*ZA[gt1, 3]*ZP[gt2, 3])/2}},
 C[S[2, {gt1}], -S[3, {gt2}], V[3], V[2]] == {{(g1*g2*STW*ZA[gt1, 1]*ZP[gt2, 1])/2 + (g1*g2*STW*ZA[gt1, 2]*ZP[gt2, 2])/2 + (g1*g2*STW*ZA[gt1, 3]*ZP[gt2, 3])/2}},
 C[S[6], S[8], -V[3], V[1]] == {{(I/2)*CTW*g1*g2}},
 C[S[6], S[8], -V[3], V[2]] == {{(-I/2)*g1*g2*STW}},
 C[S[6], S[10], -V[3], V[1]] == {{(CTW*g1*g2)/2}},
 C[S[6], S[10], -V[3], V[2]] == {{-(g1*g2*STW)/2}},
 C[S[6], -S[6], V[1], V[1]] == {{(I/4)*g1^2 + (I/4)*CTW^2*g1^2 + (I/4)*g2^2 - (I/4)*CTW^2*g2^2 + I*CTW*g1*g2*STW - (I/4)*g1^2*STW^2 + (I/4)*g2^2*STW^2}},
 C[S[6], -S[6], V[1], V[2]] == {{(I/2)*CTW^2*g1*g2 - (I/2)*CTW*g1^2*STW + (I/2)*CTW*g2^2*STW - (I/2)*g1*g2*STW^2}},
 C[S[6], -S[6], -V[3], V[3]] == {{(I/2)*g2^2}},
 C[S[6], -S[6], V[2], V[2]] == {{(I/4)*g1^2 - (I/4)*CTW^2*g1^2 + (I/4)*g2^2 + (I/4)*CTW^2*g2^2 - I*CTW*g1*g2*STW + (I/4)*g1^2*STW^2 - (I/4)*g2^2*STW^2}},
 C[S[8], S[8], -V[3], V[3]] == {{(I/2)*g2^2}},
 C[S[8], S[8], V[2], V[2]] == {{(I/4)*g1^2 - (I/4)*CTW^2*g1^2 + (I/4)*g2^2 + (I/4)*CTW^2*g2^2 + I*CTW*g1*g2*STW + (I/4)*g1^2*STW^2 - (I/4)*g2^2*STW^2}},
 C[S[8], -S[6], V[1], V[3]] == {{(I/2)*CTW*g1*g2}},
 C[S[8], -S[6], V[3], V[2]] == {{(-I/2)*g1*g2*STW}},
 C[S[1, {gt1}], S[1, {gt2}], -V[3], V[3]] == {{(I/2)*g2^2*ZH[gt1, 1]*ZH[gt2, 1] + (I/2)*g2^2*ZH[gt1, 2]*ZH[gt2, 2] + (I/2)*g2^2*ZH[gt1, 3]*ZH[gt2, 3]}},
 C[S[1, {gt1}], S[1, {gt2}], V[2], V[2]] == {{(I/4)*g1^2*ZH[gt1, 1]*ZH[gt2, 1] - (I/4)*CTW^2*g1^2*ZH[gt1, 1]*ZH[gt2, 1] + (I/4)*g2^2*ZH[gt1, 1]*ZH[gt2, 1] + (I/4)*CTW^2*g2^2*ZH[gt1, 1]*ZH[gt2, 1] + I*CTW*g1*g2*STW*ZH[gt1, 1]*ZH[gt2, 1] + (I/4)*g1^2*STW^2*ZH[gt1, 1]*ZH[gt2, 1] - (I/4)*g2^2*STW^2*ZH[gt1, 1]*ZH[gt2, 1] + (I/4)*g1^2*ZH[gt1, 2]*ZH[gt2, 2] - (I/4)*CTW^2*g1^2*ZH[gt1, 2]*ZH[gt2, 2] + (I/4)*g2^2*ZH[gt1, 2]*ZH[gt2, 2] + (I/4)*CTW^2*g2^2*ZH[gt1, 2]*ZH[gt2, 2] + I*CTW*g1*g2*STW*ZH[gt1, 2]*ZH[gt2, 2] + (I/4)*g1^2*STW^2*ZH[gt1, 2]*ZH[gt2, 2] - (I/4)*g2^2*STW^2*ZH[gt1, 2]*ZH[gt2, 2] + (I/4)*g1^2*ZH[gt1, 3]*ZH[gt2, 3] - (I/4)*CTW^2*g1^2*ZH[gt1, 3]*ZH[gt2, 3] + (I/4)*g2^2*ZH[gt1, 3]*ZH[gt2, 3] + (I/4)*CTW^2*g2^2*ZH[gt1, 3]*ZH[gt2, 3] + I*CTW*g1*g2*STW*ZH[gt1, 3]*ZH[gt2, 3] + (I/4)*g1^2*STW^2*ZH[gt1, 3]*ZH[gt2, 3] - (I/4)*g2^2*STW^2*ZH[gt1, 3]*ZH[gt2, 3]}},
 C[S[1, {gt1}], S[3, {gt2}], -V[3], V[1]] == {{(I/2)*CTW*g1*g2*ZH[gt1, 1]*ZP[gt2, 1] + (I/2)*CTW*g1*g2*ZH[gt1, 2]*ZP[gt2, 2] + (I/2)*CTW*g1*g2*ZH[gt1, 3]*ZP[gt2, 3]}},
 C[S[1, {gt1}], S[3, {gt2}], -V[3], V[2]] == {{(-I/2)*g1*g2*STW*ZH[gt1, 1]*ZP[gt2, 1] - (I/2)*g1*g2*STW*ZH[gt1, 2]*ZP[gt2, 2] - (I/2)*g1*g2*STW*ZH[gt1, 3]*ZP[gt2, 3]}},
 C[S[1, {gt1}], -S[3, {gt2}], V[1], V[3]] == {{(I/2)*CTW*g1*g2*ZH[gt1, 1]*ZP[gt2, 1] + (I/2)*CTW*g1*g2*ZH[gt1, 2]*ZP[gt2, 2] + (I/2)*CTW*g1*g2*ZH[gt1, 3]*ZP[gt2, 3]}},
 C[S[1, {gt1}], -S[3, {gt2}], V[3], V[2]] == {{(-I/2)*g1*g2*STW*ZH[gt1, 1]*ZP[gt2, 1] - (I/2)*g1*g2*STW*ZH[gt1, 2]*ZP[gt2, 2] - (I/2)*g1*g2*STW*ZH[gt1, 3]*ZP[gt2, 3]}},
 C[S[3, {gt1}], -S[3, {gt2}], V[1], V[1]] == {{(I/4)*g1^2*ZP[gt1, 1]*ZP[gt2, 1] + (I/4)*CTW^2*g1^2*ZP[gt1, 1]*ZP[gt2, 1] + (I/4)*g2^2*ZP[gt1, 1]*ZP[gt2, 1] - (I/4)*CTW^2*g2^2*ZP[gt1, 1]*ZP[gt2, 1] + I*CTW*g1*g2*STW*ZP[gt1, 1]*ZP[gt2, 1] - (I/4)*g1^2*STW^2*ZP[gt1, 1]*ZP[gt2, 1] + (I/4)*g2^2*STW^2*ZP[gt1, 1]*ZP[gt2, 1] + (I/4)*g1^2*ZP[gt1, 2]*ZP[gt2, 2] + (I/4)*CTW^2*g1^2*ZP[gt1, 2]*ZP[gt2, 2] + (I/4)*g2^2*ZP[gt1, 2]*ZP[gt2, 2] - (I/4)*CTW^2*g2^2*ZP[gt1, 2]*ZP[gt2, 2] + I*CTW*g1*g2*STW*ZP[gt1, 2]*ZP[gt2, 2] - (I/4)*g1^2*STW^2*ZP[gt1, 2]*ZP[gt2, 2] + (I/4)*g2^2*STW^2*ZP[gt1, 2]*ZP[gt2, 2] + (I/4)*g1^2*ZP[gt1, 3]*ZP[gt2, 3] + (I/4)*CTW^2*g1^2*ZP[gt1, 3]*ZP[gt2, 3] + (I/4)*g2^2*ZP[gt1, 3]*ZP[gt2, 3] - (I/4)*CTW^2*g2^2*ZP[gt1, 3]*ZP[gt2, 3] + I*CTW*g1*g2*STW*ZP[gt1, 3]*ZP[gt2, 3] - (I/4)*g1^2*STW^2*ZP[gt1, 3]*ZP[gt2, 3] + (I/4)*g2^2*STW^2*ZP[gt1, 3]*ZP[gt2, 3]}},
 C[S[3, {gt1}], -S[3, {gt2}], V[1], V[2]] == {{(I/2)*CTW^2*g1*g2*ZP[gt1, 1]*ZP[gt2, 1] - (I/2)*CTW*g1^2*STW*ZP[gt1, 1]*ZP[gt2, 1] + (I/2)*CTW*g2^2*STW*ZP[gt1, 1]*ZP[gt2, 1] - (I/2)*g1*g2*STW^2*ZP[gt1, 1]*ZP[gt2, 1] + (I/2)*CTW^2*g1*g2*ZP[gt1, 2]*ZP[gt2, 2] - (I/2)*CTW*g1^2*STW*ZP[gt1, 2]*ZP[gt2, 2] + (I/2)*CTW*g2^2*STW*ZP[gt1, 2]*ZP[gt2, 2] - (I/2)*g1*g2*STW^2*ZP[gt1, 2]*ZP[gt2, 2] + (I/2)*CTW^2*g1*g2*ZP[gt1, 3]*ZP[gt2, 3] - (I/2)*CTW*g1^2*STW*ZP[gt1, 3]*ZP[gt2, 3] + (I/2)*CTW*g2^2*STW*ZP[gt1, 3]*ZP[gt2, 3] - (I/2)*g1*g2*STW^2*ZP[gt1, 3]*ZP[gt2, 3]}},
 C[S[3, {gt1}], -S[3, {gt2}], -V[3], V[3]] == {{(I/2)*g2^2*ZP[gt1, 1]*ZP[gt2, 1] + (I/2)*g2^2*ZP[gt1, 2]*ZP[gt2, 2] + (I/2)*g2^2*ZP[gt1, 3]*ZP[gt2, 3]}},
 C[S[3, {gt1}], -S[3, {gt2}], V[2], V[2]] == {{(I/4)*g1^2*ZP[gt1, 1]*ZP[gt2, 1] - (I/4)*CTW^2*g1^2*ZP[gt1, 1]*ZP[gt2, 1] + (I/4)*g2^2*ZP[gt1, 1]*ZP[gt2, 1] + (I/4)*CTW^2*g2^2*ZP[gt1, 1]*ZP[gt2, 1] - I*CTW*g1*g2*STW*ZP[gt1, 1]*ZP[gt2, 1] + (I/4)*g1^2*STW^2*ZP[gt1, 1]*ZP[gt2, 1] - (I/4)*g2^2*STW^2*ZP[gt1, 1]*ZP[gt2, 1] + (I/4)*g1^2*ZP[gt1, 2]*ZP[gt2, 2] - (I/4)*CTW^2*g1^2*ZP[gt1, 2]*ZP[gt2, 2] + (I/4)*g2^2*ZP[gt1, 2]*ZP[gt2, 2] + (I/4)*CTW^2*g2^2*ZP[gt1, 2]*ZP[gt2, 2] - I*CTW*g1*g2*STW*ZP[gt1, 2]*ZP[gt2, 2] + (I/4)*g1^2*STW^2*ZP[gt1, 2]*ZP[gt2, 2] - (I/4)*g2^2*STW^2*ZP[gt1, 2]*ZP[gt2, 2] + (I/4)*g1^2*ZP[gt1, 3]*ZP[gt2, 3] - (I/4)*CTW^2*g1^2*ZP[gt1, 3]*ZP[gt2, 3] + (I/4)*g2^2*ZP[gt1, 3]*ZP[gt2, 3] + (I/4)*CTW^2*g2^2*ZP[gt1, 3]*ZP[gt2, 3] - I*CTW*g1*g2*STW*ZP[gt1, 3]*ZP[gt2, 3] + (I/4)*g1^2*STW^2*ZP[gt1, 3]*ZP[gt2, 3] - (I/4)*g2^2*STW^2*ZP[gt1, 3]*ZP[gt2, 3]}},
 C[S[10], S[10], -V[3], V[3]] == {{(I/2)*g2^2}},
 C[S[10], S[10], V[2], V[2]] == {{(I/4)*g1^2 - (I/4)*CTW^2*g1^2 + (I/4)*g2^2 + (I/4)*CTW^2*g2^2 + I*CTW*g1*g2*STW + (I/4)*g1^2*STW^2 - (I/4)*g2^2*STW^2}},
 C[S[10], -S[6], V[1], V[3]] == {{-(CTW*g1*g2)/2}},
 C[S[10], -S[6], V[3], V[2]] == {{(g1*g2*STW)/2}},
 C[-F[4, {gt1, ct1}], F[4, {gt2, ct2}], S[2, {gt3}]] == {{-((IndexDelta[ct1, ct2]*IndexSum[Conjugate[ZDL[gt2, j2]]*IndexSum[Conjugate[ZDR[gt1, j1]]*Yd[j1, j2], {j1, 3}], {j2, 3}]*ZA[gt3, 1])/Sqrt[2])}, {(IndexDelta[ct1, ct2]*IndexSum[IndexSum[Conjugate[Yd[j1, j2]]*ZDR[gt2, j1], {j1, 3}]*ZDL[gt1, j2], {j2, 3}]*ZA[gt3, 1])/Sqrt[2]}},
 C[-F[2, {gt1}], F[2, {gt2}], S[2, {gt3}]] == {{-((IndexSum[Conjugate[ZEL[gt2, j2]]*IndexSum[Conjugate[ZER[gt1, j1]]*Ye[j1, j2], {j1, 3}], {j2, 3}]*ZA[gt3, 1])/Sqrt[2])}, {(IndexSum[IndexSum[Conjugate[Ye[j1, j2]]*ZER[gt2, j1], {j1, 3}]*ZEL[gt1, j2], {j2, 3}]*ZA[gt3, 1])/Sqrt[2]}},
 C[-F[3, {gt1, ct1}], F[3, {gt2, ct2}], S[2, {gt3}]] == {{(IndexDelta[ct1, ct2]*IndexSum[Conjugate[ZUL[gt2, j2]]*IndexSum[Conjugate[ZUR[gt1, j1]]*Yu[j1, j2], {j1, 3}], {j2, 3}]*ZA[gt3, 1])/Sqrt[2]}, {-((IndexDelta[ct1, ct2]*IndexSum[IndexSum[Conjugate[Yu[j1, j2]]*ZUR[gt2, j1], {j1, 3}]*ZUL[gt1, j2], {j2, 3}]*ZA[gt3, 1])/Sqrt[2])}},
 C[-F[4, {gt1, ct1}], F[4, {gt2, ct2}], S[1, {gt3}]] == {{((-I)*IndexDelta[ct1, ct2]*IndexSum[Conjugate[ZDL[gt2, j2]]*IndexSum[Conjugate[ZDR[gt1, j1]]*Yd[j1, j2], {j1, 3}], {j2, 3}]*ZH[gt3, 1])/Sqrt[2]}, {((-I)*IndexDelta[ct1, ct2]*IndexSum[IndexSum[Conjugate[Yd[j1, j2]]*ZDR[gt2, j1], {j1, 3}]*ZDL[gt1, j2], {j2, 3}]*ZH[gt3, 1])/Sqrt[2]}},
 C[-F[3, {gt1, ct1}], F[4, {gt2, ct2}], S[3, {gt3}]] == {{I*IndexDelta[ct1, ct2]*IndexSum[Conjugate[ZDL[gt2, j2]]*IndexSum[Conjugate[ZUR[gt1, j1]]*Yu[j1, j2], {j1, 3}], {j2, 3}]*ZP[gt3, 1]}, {(-I)*IndexDelta[ct1, ct2]*IndexSum[IndexSum[Conjugate[Yd[j1, j2]]*ZDR[gt2, j1], {j1, 3}]*ZUL[gt1, j2], {j2, 3}]*ZP[gt3, 1]}},
 C[-F[2, {gt1}], F[2, {gt2}], S[1, {gt3}]] == {{((-I)*IndexSum[Conjugate[ZEL[gt2, j2]]*IndexSum[Conjugate[ZER[gt1, j1]]*Ye[j1, j2], {j1, 3}], {j2, 3}]*ZH[gt3, 1])/Sqrt[2]}, {((-I)*IndexSum[IndexSum[Conjugate[Ye[j1, j2]]*ZER[gt2, j1], {j1, 3}]*ZEL[gt1, j2], {j2, 3}]*ZH[gt3, 1])/Sqrt[2]}},
 C[-F[1, {gt1}], F[2, {gt2}], S[3, {gt3}]] == {{0}, {(-I)*IndexSum[Conjugate[Ye[j1, gt1]]*ZER[gt2, j1], {j1, 3}]*ZP[gt3, 1]}},
 C[-F[3, {gt1, ct1}], F[3, {gt2, ct2}], S[1, {gt3}]] == {{((-I)*IndexDelta[ct1, ct2]*IndexSum[Conjugate[ZUL[gt2, j2]]*IndexSum[Conjugate[ZUR[gt1, j1]]*Yu[j1, j2], {j1, 3}], {j2, 3}]*ZH[gt3, 1])/Sqrt[2]}, {((-I)*IndexDelta[ct1, ct2]*IndexSum[IndexSum[Conjugate[Yu[j1, j2]]*ZUR[gt2, j1], {j1, 3}]*ZUL[gt1, j2], {j2, 3}]*ZH[gt3, 1])/Sqrt[2]}},
 C[-F[4, {gt1, ct1}], F[3, {gt2, ct2}], -S[3, {gt3}]] == {{(-I)*IndexDelta[ct1, ct2]*IndexSum[Conjugate[ZUL[gt2, j2]]*IndexSum[Conjugate[ZDR[gt1, j1]]*Yd[j1, j2], {j1, 3}], {j2, 3}]*ZP[gt3, 1]}, {I*IndexDelta[ct1, ct2]*IndexSum[IndexSum[Conjugate[Yu[j1, j2]]*ZUR[gt2, j1], {j1, 3}]*ZDL[gt1, j2], {j2, 3}]*ZP[gt3, 1]}},
 C[-F[2, {gt1}], F[1, {gt2}], -S[3, {gt3}]] == {{(-I)*IndexSum[Conjugate[ZER[gt1, j1]]*Ye[j1, gt2], {j1, 3}]*ZP[gt3, 1]}, {0}},
 C[-F[4, {gt1, ct1}], F[4, {gt2, ct2}], V[5, {ct3}]] == {{(-I/2)*g3*IndexDelta[gt1, gt2]*Lam[ct3, ct1, ct2]}, {(-I/2)*g3*IndexDelta[gt1, gt2]*Lam[ct3, ct1, ct2]}},
 C[-F[4, {gt1, ct1}], F[4, {gt2, ct2}], V[1]] == {{(-I/6)*CTW*g1*IndexDelta[ct1, ct2]*IndexDelta[gt1, gt2] + (I/2)*g2*STW*IndexDelta[ct1, ct2]*IndexDelta[gt1, gt2]}, {(I/3)*CTW*g1*IndexDelta[ct1, ct2]*IndexDelta[gt1, gt2]}},
 C[-F[3, {gt1, ct1}], F[4, {gt2, ct2}], V[3]] == {{((-I)*g2*IndexDelta[ct1, ct2]*IndexSum[Conjugate[ZDL[gt2, j1]]*ZUL[gt1, j1], {j1, 3}])/Sqrt[2]}, {0}},
 C[-F[4, {gt1, ct1}], F[4, {gt2, ct2}], V[2]] == {{(I/2)*CTW*g2*IndexDelta[ct1, ct2]*IndexDelta[gt1, gt2] + (I/6)*g1*STW*IndexDelta[ct1, ct2]*IndexDelta[gt1, gt2]}, {(-I/3)*g1*STW*IndexDelta[ct1, ct2]*IndexDelta[gt1, gt2]}},
 C[-F[2, {gt1}], F[2, {gt2}], V[1]] == {{(I/2)*CTW*g1*IndexDelta[gt1, gt2] + (I/2)*g2*STW*IndexDelta[gt1, gt2]}, {I*CTW*g1*IndexDelta[gt1, gt2]}},
 C[-F[1, {gt1}], F[2, {gt2}], V[3]] == {{((-I)*g2*Conjugate[ZEL[gt2, gt1]])/Sqrt[2]}, {0}},
 C[-F[2, {gt1}], F[2, {gt2}], V[2]] == {{(I/2)*CTW*g2*IndexDelta[gt1, gt2] - (I/2)*g1*STW*IndexDelta[gt1, gt2]}, {(-I)*g1*STW*IndexDelta[gt1, gt2]}},
 C[-F[3, {gt1, ct1}], F[3, {gt2, ct2}], V[5, {ct3}]] == {{(-I/2)*g3*IndexDelta[gt1, gt2]*Lam[ct3, ct1, ct2]}, {(-I/2)*g3*IndexDelta[gt1, gt2]*Lam[ct3, ct1, ct2]}},
 C[-F[3, {gt1, ct1}], F[3, {gt2, ct2}], V[1]] == {{(-I/6)*CTW*g1*IndexDelta[ct1, ct2]*IndexDelta[gt1, gt2] - (I/2)*g2*STW*IndexDelta[ct1, ct2]*IndexDelta[gt1, gt2]}, {((-2*I)/3)*CTW*g1*IndexDelta[ct1, ct2]*IndexDelta[gt1, gt2]}},
 C[-F[3, {gt1, ct1}], F[3, {gt2, ct2}], V[2]] == {{(-I/2)*CTW*g2*IndexDelta[ct1, ct2]*IndexDelta[gt1, gt2] + (I/6)*g1*STW*IndexDelta[ct1, ct2]*IndexDelta[gt1, gt2]}, {((2*I)/3)*g1*STW*IndexDelta[ct1, ct2]*IndexDelta[gt1, gt2]}},
 C[-F[4, {gt1, ct1}], F[3, {gt2, ct2}], -V[3]] == {{((-I)*g2*IndexDelta[ct1, ct2]*IndexSum[Conjugate[ZUL[gt2, j1]]*ZDL[gt1, j1], {j1, 3}])/Sqrt[2]}, {0}},
 C[-F[1, {gt1}], F[1, {gt2}], V[2]] == {{(-I/2)*CTW*g2*IndexDelta[gt1, gt2] - (I/2)*g1*STW*IndexDelta[gt1, gt2]}, {0}},
 C[-F[2, {gt1}], F[1, {gt2}], -V[3]] == {{((-I)*g2*ZEL[gt1, gt2])/Sqrt[2]}, {0}},
 C[S[2, {gt1}], S[2, {gt2}], S[1, {gt3}]] == {{(-2*I)*lam8*v1*ZA[gt1, 1]*ZA[gt2, 1]*ZH[gt3, 1] - (2*I)*Sqrt[3]*lam7*v2*ZA[gt1, 2]*ZA[gt2, 1]*ZH[gt3, 1] - (2*I)*lam7*v2*ZA[gt1, 3]*ZA[gt2, 1]*ZH[gt3, 1] - (2*I)*Sqrt[3]*lam7*v2*ZA[gt1, 1]*ZA[gt2, 2]*ZH[gt3, 1] - I*lam5*v1*ZA[gt1, 2]*ZA[gt2, 2]*ZH[gt3, 1] - I*lam6*v1*ZA[gt1, 2]*ZA[gt2, 2]*ZH[gt3, 1] + (2*I)*lam7*v1*ZA[gt1, 2]*ZA[gt2, 2]*ZH[gt3, 1] - I*lam4*v2*ZA[gt1, 2]*ZA[gt2, 2]*ZH[gt3, 1] - I*Sqrt[3]*lam4*v2*ZA[gt1, 3]*ZA[gt2, 2]*ZH[gt3, 1] - (2*I)*lam7*v2*ZA[gt1, 1]*ZA[gt2, 3]*ZH[gt3, 1] - I*Sqrt[3]*lam4*v2*ZA[gt1, 2]*ZA[gt2, 3]*ZH[gt3, 1] - I*lam5*v1*ZA[gt1, 3]*ZA[gt2, 3]*ZH[gt3, 1] - I*lam6*v1*ZA[gt1, 3]*ZA[gt2, 3]*ZH[gt3, 1] + (2*I)*lam7*v1*ZA[gt1, 3]*ZA[gt2, 3]*ZH[gt3, 1] + I*lam4*v2*ZA[gt1, 3]*ZA[gt2, 3]*ZH[gt3, 1] - I*Sqrt[3]*lam5*v2*ZA[gt1, 1]*ZA[gt2, 1]*ZH[gt3, 2] - I*Sqrt[3]*lam6*v2*ZA[gt1, 1]*ZA[gt2, 1]*ZH[gt3, 2] + (2*I)*Sqrt[3]*lam7*v2*ZA[gt1, 1]*ZA[gt2, 1]*ZH[gt3, 2] - (2*I)*lam7*v1*ZA[gt1, 2]*ZA[gt2, 1]*ZH[gt3, 2] - I*lam4*v2*ZA[gt1, 2]*ZA[gt2, 1]*ZH[gt3, 2] - I*Sqrt[3]*lam4*v2*ZA[gt1, 3]*ZA[gt2, 1]*ZH[gt3, 2] - (2*I)*lam7*v1*ZA[gt1, 1]*ZA[gt2, 2]*ZH[gt3, 2] - I*lam4*v2*ZA[gt1, 1]*ZA[gt2, 2]*ZH[gt3, 2] - (2*I)*Sqrt[3]*lam1*v2*ZA[gt1, 2]*ZA[gt2, 2]*ZH[gt3, 2] - (2*I)*Sqrt[3]*lam3*v2*ZA[gt1, 2]*ZA[gt2, 2]*ZH[gt3, 2] - I*lam4*v1*ZA[gt1, 3]*ZA[gt2, 2]*ZH[gt3, 2] - (2*I)*lam2*v2*ZA[gt1, 3]*ZA[gt2, 2]*ZH[gt3, 2] - (2*I)*lam3*v2*ZA[gt1, 3]*ZA[gt2, 2]*ZH[gt3, 2] - I*Sqrt[3]*lam4*v2*ZA[gt1, 1]*ZA[gt2, 3]*ZH[gt3, 2] - I*lam4*v1*ZA[gt1, 2]*ZA[gt2, 3]*ZH[gt3, 2] - (2*I)*lam2*v2*ZA[gt1, 2]*ZA[gt2, 3]*ZH[gt3, 2] - (2*I)*lam3*v2*ZA[gt1, 2]*ZA[gt2, 3]*ZH[gt3, 2] - (2*I)*Sqrt[3]*lam1*v2*ZA[gt1, 3]*ZA[gt2, 3]*ZH[gt3, 2] + (4*I)*Sqrt[3]*lam2*v2*ZA[gt1, 3]*ZA[gt2, 3]*ZH[gt3, 2] + (2*I)*Sqrt[3]*lam3*v2*ZA[gt1, 3]*ZA[gt2, 3]*ZH[gt3, 2] - I*lam5*v2*ZA[gt1, 1]*ZA[gt2, 1]*ZH[gt3, 3] - I*lam6*v2*ZA[gt1, 1]*ZA[gt2, 1]*ZH[gt3, 3] + (2*I)*lam7*v2*ZA[gt1, 1]*ZA[gt2, 1]*ZH[gt3, 3] - I*Sqrt[3]*lam4*v2*ZA[gt1, 2]*ZA[gt2, 1]*ZH[gt3, 3] - (2*I)*lam7*v1*ZA[gt1, 3]*ZA[gt2, 1]*ZH[gt3, 3] + I*lam4*v2*ZA[gt1, 3]*ZA[gt2, 1]*ZH[gt3, 3] - I*Sqrt[3]*lam4*v2*ZA[gt1, 1]*ZA[gt2, 2]*ZH[gt3, 3] - I*lam4*v1*ZA[gt1, 2]*ZA[gt2, 2]*ZH[gt3, 3] - (2*I)*lam1*v2*ZA[gt1, 2]*ZA[gt2, 2]*ZH[gt3, 3] + (4*I)*lam2*v2*ZA[gt1, 2]*ZA[gt2, 2]*ZH[gt3, 3] + (2*I)*lam3*v2*ZA[gt1, 2]*ZA[gt2, 2]*ZH[gt3, 3] - (2*I)*Sqrt[3]*lam2*v2*ZA[gt1, 3]*ZA[gt2, 2]*ZH[gt3, 3] - (2*I)*Sqrt[3]*lam3*v2*ZA[gt1, 3]*ZA[gt2, 2]*ZH[gt3, 3] - (2*I)*lam7*v1*ZA[gt1, 1]*ZA[gt2, 3]*ZH[gt3, 3] + I*lam4*v2*ZA[gt1, 1]*ZA[gt2, 3]*ZH[gt3, 3] - (2*I)*Sqrt[3]*lam2*v2*ZA[gt1, 2]*ZA[gt2, 3]*ZH[gt3, 3] - (2*I)*Sqrt[3]*lam3*v2*ZA[gt1, 2]*ZA[gt2, 3]*ZH[gt3, 3] + I*lam4*v1*ZA[gt1, 3]*ZA[gt2, 3]*ZH[gt3, 3] - (2*I)*lam1*v2*ZA[gt1, 3]*ZA[gt2, 3]*ZH[gt3, 3] - (2*I)*lam3*v2*ZA[gt1, 3]*ZA[gt2, 3]*ZH[gt3, 3]}},
 C[S[2, {gt1}], S[8], S[10]] == {{(-2*I)*Sqrt[3]*lam12*v2*ZA[gt1, 2] - (2*I)*lam12*v2*ZA[gt1, 3]}},
 C[S[2, {gt1}], S[3, {gt2}], -S[3, {gt3}]] == {{(Sqrt[3]*lam6*v2*ZA[gt1, 1]*ZP[gt2, 2]*ZP[gt3, 1])/2 - Sqrt[3]*lam7*v2*ZA[gt1, 1]*ZP[gt2, 2]*ZP[gt3, 1] - (lam6*v1*ZA[gt1, 2]*ZP[gt2, 2]*ZP[gt3, 1])/2 + lam7*v1*ZA[gt1, 2]*ZP[gt2, 2]*ZP[gt3, 1] + (lam6*v2*ZA[gt1, 1]*ZP[gt2, 3]*ZP[gt3, 1])/2 - lam7*v2*ZA[gt1, 1]*ZP[gt2, 3]*ZP[gt3, 1] - (lam6*v1*ZA[gt1, 3]*ZP[gt2, 3]*ZP[gt3, 1])/2 + lam7*v1*ZA[gt1, 3]*ZP[gt2, 3]*ZP[gt3, 1] - (Sqrt[3]*lam6*v2*ZA[gt1, 1]*ZP[gt2, 1]*ZP[gt3, 2])/2 + Sqrt[3]*lam7*v2*ZA[gt1, 1]*ZP[gt2, 1]*ZP[gt3, 2] + (lam6*v1*ZA[gt1, 2]*ZP[gt2, 1]*ZP[gt3, 2])/2 - lam7*v1*ZA[gt1, 2]*ZP[gt2, 1]*ZP[gt3, 2] - 2*lam2*v2*ZA[gt1, 2]*ZP[gt2, 3]*ZP[gt3, 2] + 2*Sqrt[3]*lam2*v2*ZA[gt1, 3]*ZP[gt2, 3]*ZP[gt3, 2] - (lam6*v2*ZA[gt1, 1]*ZP[gt2, 1]*ZP[gt3, 3])/2 + lam7*v2*ZA[gt1, 1]*ZP[gt2, 1]*ZP[gt3, 3] + (lam6*v1*ZA[gt1, 3]*ZP[gt2, 1]*ZP[gt3, 3])/2 - lam7*v1*ZA[gt1, 3]*ZP[gt2, 1]*ZP[gt3, 3] + 2*lam2*v2*ZA[gt1, 2]*ZP[gt2, 2]*ZP[gt3, 3] - 2*Sqrt[3]*lam2*v2*ZA[gt1, 3]*ZP[gt2, 2]*ZP[gt3, 3]}},
 C[S[6], S[8], -S[3, {gt3}]] == {{(-I/2)*lam14*v1*ZP[gt3, 1] - (I/2)*Sqrt[3]*lam11*v2*ZP[gt3, 2] - I*Sqrt[3]*lam12*v2*ZP[gt3, 2] - (I/2)*lam11*v2*ZP[gt3, 3] - I*lam12*v2*ZP[gt3, 3]}},
 C[S[6], S[1, {gt2}], -S[6]] == {{(-I)*Sqrt[3]*lam10*v2*ZH[gt2, 2] - I*lam10*v2*ZH[gt2, 3]}},
 C[S[6], S[10], -S[3, {gt3}]] == {{-(lam14*v1*ZP[gt3, 1])/2 - (Sqrt[3]*lam11*v2*ZP[gt3, 2])/2 + Sqrt[3]*lam12*v2*ZP[gt3, 2] - (lam11*v2*ZP[gt3, 3])/2 + lam12*v2*ZP[gt3, 3]}},
 C[S[8], S[8], S[1, {gt3}]] == {{(-I)*lam14*v1*ZH[gt3, 1] - I*Sqrt[3]*lam10*v2*ZH[gt3, 2] - I*Sqrt[3]*lam11*v2*ZH[gt3, 2] - (2*I)*Sqrt[3]*lam12*v2*ZH[gt3, 2] - I*lam10*v2*ZH[gt3, 3] - I*lam11*v2*ZH[gt3, 3] - (2*I)*lam12*v2*ZH[gt3, 3]}},
 C[S[8], S[3, {gt2}], -S[6]] == {{(-I/2)*lam14*v1*ZP[gt2, 1] - (I/2)*Sqrt[3]*lam11*v2*ZP[gt2, 2] - I*Sqrt[3]*lam12*v2*ZP[gt2, 2] - (I/2)*lam11*v2*ZP[gt2, 3] - I*lam12*v2*ZP[gt2, 3]}},
 C[S[1, {gt1}], S[1, {gt2}], S[1, {gt3}]] == {{(-6*I)*lam8*v1*ZH[gt1, 1]*ZH[gt2, 1]*ZH[gt3, 1] - I*Sqrt[3]*lam5*v2*ZH[gt1, 2]*ZH[gt2, 1]*ZH[gt3, 1] - I*Sqrt[3]*lam6*v2*ZH[gt1, 2]*ZH[gt2, 1]*ZH[gt3, 1] - (2*I)*Sqrt[3]*lam7*v2*ZH[gt1, 2]*ZH[gt2, 1]*ZH[gt3, 1] - I*lam5*v2*ZH[gt1, 3]*ZH[gt2, 1]*ZH[gt3, 1] - I*lam6*v2*ZH[gt1, 3]*ZH[gt2, 1]*ZH[gt3, 1] - (2*I)*lam7*v2*ZH[gt1, 3]*ZH[gt2, 1]*ZH[gt3, 1] - I*Sqrt[3]*lam5*v2*ZH[gt1, 1]*ZH[gt2, 2]*ZH[gt3, 1] - I*Sqrt[3]*lam6*v2*ZH[gt1, 1]*ZH[gt2, 2]*ZH[gt3, 1] - (2*I)*Sqrt[3]*lam7*v2*ZH[gt1, 1]*ZH[gt2, 2]*ZH[gt3, 1] - I*lam5*v1*ZH[gt1, 2]*ZH[gt2, 2]*ZH[gt3, 1] - I*lam6*v1*ZH[gt1, 2]*ZH[gt2, 2]*ZH[gt3, 1] - (2*I)*lam7*v1*ZH[gt1, 2]*ZH[gt2, 2]*ZH[gt3, 1] - (3*I)*lam4*v2*ZH[gt1, 2]*ZH[gt2, 2]*ZH[gt3, 1] - (3*I)*Sqrt[3]*lam4*v2*ZH[gt1, 3]*ZH[gt2, 2]*ZH[gt3, 1] - I*lam5*v2*ZH[gt1, 1]*ZH[gt2, 3]*ZH[gt3, 1] - I*lam6*v2*ZH[gt1, 1]*ZH[gt2, 3]*ZH[gt3, 1] - (2*I)*lam7*v2*ZH[gt1, 1]*ZH[gt2, 3]*ZH[gt3, 1] - (3*I)*Sqrt[3]*lam4*v2*ZH[gt1, 2]*ZH[gt2, 3]*ZH[gt3, 1] - I*lam5*v1*ZH[gt1, 3]*ZH[gt2, 3]*ZH[gt3, 1] - I*lam6*v1*ZH[gt1, 3]*ZH[gt2, 3]*ZH[gt3, 1] - (2*I)*lam7*v1*ZH[gt1, 3]*ZH[gt2, 3]*ZH[gt3, 1] + (3*I)*lam4*v2*ZH[gt1, 3]*ZH[gt2, 3]*ZH[gt3, 1] - I*Sqrt[3]*lam5*v2*ZH[gt1, 1]*ZH[gt2, 1]*ZH[gt3, 2] - I*Sqrt[3]*lam6*v2*ZH[gt1, 1]*ZH[gt2, 1]*ZH[gt3, 2] - (2*I)*Sqrt[3]*lam7*v2*ZH[gt1, 1]*ZH[gt2, 1]*ZH[gt3, 2] - I*lam5*v1*ZH[gt1, 2]*ZH[gt2, 1]*ZH[gt3, 2] - I*lam6*v1*ZH[gt1, 2]*ZH[gt2, 1]*ZH[gt3, 2] - (2*I)*lam7*v1*ZH[gt1, 2]*ZH[gt2, 1]*ZH[gt3, 2] - (3*I)*lam4*v2*ZH[gt1, 2]*ZH[gt2, 1]*ZH[gt3, 2] - (3*I)*Sqrt[3]*lam4*v2*ZH[gt1, 3]*ZH[gt2, 1]*ZH[gt3, 2] - I*lam5*v1*ZH[gt1, 1]*ZH[gt2, 2]*ZH[gt3, 2] - I*lam6*v1*ZH[gt1, 1]*ZH[gt2, 2]*ZH[gt3, 2] - (2*I)*lam7*v1*ZH[gt1, 1]*ZH[gt2, 2]*ZH[gt3, 2] - (3*I)*lam4*v2*ZH[gt1, 1]*ZH[gt2, 2]*ZH[gt3, 2] - (6*I)*Sqrt[3]*lam1*v2*ZH[gt1, 2]*ZH[gt2, 2]*ZH[gt3, 2] - (6*I)*Sqrt[3]*lam3*v2*ZH[gt1, 2]*ZH[gt2, 2]*ZH[gt3, 2] - (3*I)*lam4*v1*ZH[gt1, 3]*ZH[gt2, 2]*ZH[gt3, 2] - (2*I)*lam1*v2*ZH[gt1, 3]*ZH[gt2, 2]*ZH[gt3, 2] - (2*I)*lam3*v2*ZH[gt1, 3]*ZH[gt2, 2]*ZH[gt3, 2] - (3*I)*Sqrt[3]*lam4*v2*ZH[gt1, 1]*ZH[gt2, 3]*ZH[gt3, 2] - (3*I)*lam4*v1*ZH[gt1, 2]*ZH[gt2, 3]*ZH[gt3, 2] - (2*I)*lam1*v2*ZH[gt1, 2]*ZH[gt2, 3]*ZH[gt3, 2] - (2*I)*lam3*v2*ZH[gt1, 2]*ZH[gt2, 3]*ZH[gt3, 2] - (2*I)*Sqrt[3]*lam1*v2*ZH[gt1, 3]*ZH[gt2, 3]*ZH[gt3, 2] - (2*I)*Sqrt[3]*lam3*v2*ZH[gt1, 3]*ZH[gt2, 3]*ZH[gt3, 2] - I*lam5*v2*ZH[gt1, 1]*ZH[gt2, 1]*ZH[gt3, 3] - I*lam6*v2*ZH[gt1, 1]*ZH[gt2, 1]*ZH[gt3, 3] - (2*I)*lam7*v2*ZH[gt1, 1]*ZH[gt2, 1]*ZH[gt3, 3] - (3*I)*Sqrt[3]*lam4*v2*ZH[gt1, 2]*ZH[gt2, 1]*ZH[gt3, 3] - I*lam5*v1*ZH[gt1, 3]*ZH[gt2, 1]*ZH[gt3, 3] - I*lam6*v1*ZH[gt1, 3]*ZH[gt2, 1]*ZH[gt3, 3] - (2*I)*lam7*v1*ZH[gt1, 3]*ZH[gt2, 1]*ZH[gt3, 3] + (3*I)*lam4*v2*ZH[gt1, 3]*ZH[gt2, 1]*ZH[gt3, 3] - (3*I)*Sqrt[3]*lam4*v2*ZH[gt1, 1]*ZH[gt2, 2]*ZH[gt3, 3] - (3*I)*lam4*v1*ZH[gt1, 2]*ZH[gt2, 2]*ZH[gt3, 3] - (2*I)*lam1*v2*ZH[gt1, 2]*ZH[gt2, 2]*ZH[gt3, 3] - (2*I)*lam3*v2*ZH[gt1, 2]*ZH[gt2, 2]*ZH[gt3, 3] - (2*I)*Sqrt[3]*lam1*v2*ZH[gt1, 3]*ZH[gt2, 2]*ZH[gt3, 3] - (2*I)*Sqrt[3]*lam3*v2*ZH[gt1, 3]*ZH[gt2, 2]*ZH[gt3, 3] - I*lam5*v1*ZH[gt1, 1]*ZH[gt2, 3]*ZH[gt3, 3] - I*lam6*v1*ZH[gt1, 1]*ZH[gt2, 3]*ZH[gt3, 3] - (2*I)*lam7*v1*ZH[gt1, 1]*ZH[gt2, 3]*ZH[gt3, 3] + (3*I)*lam4*v2*ZH[gt1, 1]*ZH[gt2, 3]*ZH[gt3, 3] - (2*I)*Sqrt[3]*lam1*v2*ZH[gt1, 2]*ZH[gt2, 3]*ZH[gt3, 3] - (2*I)*Sqrt[3]*lam3*v2*ZH[gt1, 2]*ZH[gt2, 3]*ZH[gt3, 3] + (3*I)*lam4*v1*ZH[gt1, 3]*ZH[gt2, 3]*ZH[gt3, 3] - (6*I)*lam1*v2*ZH[gt1, 3]*ZH[gt2, 3]*ZH[gt3, 3] - (6*I)*lam3*v2*ZH[gt1, 3]*ZH[gt2, 3]*ZH[gt3, 3]}},
 C[S[1, {gt1}], S[3, {gt2}], -S[3, {gt3}]] == {{(-2*I)*lam8*v1*ZH[gt1, 1]*ZP[gt2, 1]*ZP[gt3, 1] - I*Sqrt[3]*lam5*v2*ZH[gt1, 2]*ZP[gt2, 1]*ZP[gt3, 1] - I*lam5*v2*ZH[gt1, 3]*ZP[gt2, 1]*ZP[gt3, 1] - (I/2)*Sqrt[3]*lam6*v2*ZH[gt1, 1]*ZP[gt2, 2]*ZP[gt3, 1] - I*Sqrt[3]*lam7*v2*ZH[gt1, 1]*ZP[gt2, 2]*ZP[gt3, 1] - (I/2)*lam6*v1*ZH[gt1, 2]*ZP[gt2, 2]*ZP[gt3, 1] - I*lam7*v1*ZH[gt1, 2]*ZP[gt2, 2]*ZP[gt3, 1] - I*lam4*v2*ZH[gt1, 2]*ZP[gt2, 2]*ZP[gt3, 1] - I*Sqrt[3]*lam4*v2*ZH[gt1, 3]*ZP[gt2, 2]*ZP[gt3, 1] - (I/2)*lam6*v2*ZH[gt1, 1]*ZP[gt2, 3]*ZP[gt3, 1] - I*lam7*v2*ZH[gt1, 1]*ZP[gt2, 3]*ZP[gt3, 1] - I*Sqrt[3]*lam4*v2*ZH[gt1, 2]*ZP[gt2, 3]*ZP[gt3, 1] - (I/2)*lam6*v1*ZH[gt1, 3]*ZP[gt2, 3]*ZP[gt3, 1] - I*lam7*v1*ZH[gt1, 3]*ZP[gt2, 3]*ZP[gt3, 1] + I*lam4*v2*ZH[gt1, 3]*ZP[gt2, 3]*ZP[gt3, 1] - (I/2)*Sqrt[3]*lam6*v2*ZH[gt1, 1]*ZP[gt2, 1]*ZP[gt3, 2] - I*Sqrt[3]*lam7*v2*ZH[gt1, 1]*ZP[gt2, 1]*ZP[gt3, 2] - (I/2)*lam6*v1*ZH[gt1, 2]*ZP[gt2, 1]*ZP[gt3, 2] - I*lam7*v1*ZH[gt1, 2]*ZP[gt2, 1]*ZP[gt3, 2] - I*lam4*v2*ZH[gt1, 2]*ZP[gt2, 1]*ZP[gt3, 2] - I*Sqrt[3]*lam4*v2*ZH[gt1, 3]*ZP[gt2, 1]*ZP[gt3, 2] - I*lam5*v1*ZH[gt1, 1]*ZP[gt2, 2]*ZP[gt3, 2] - I*lam4*v2*ZH[gt1, 1]*ZP[gt2, 2]*ZP[gt3, 2] - (2*I)*Sqrt[3]*lam1*v2*ZH[gt1, 2]*ZP[gt2, 2]*ZP[gt3, 2] - (2*I)*Sqrt[3]*lam3*v2*ZH[gt1, 2]*ZP[gt2, 2]*ZP[gt3, 2] - I*lam4*v1*ZH[gt1, 3]*ZP[gt2, 2]*ZP[gt3, 2] - (2*I)*lam1*v2*ZH[gt1, 3]*ZP[gt2, 2]*ZP[gt3, 2] + (2*I)*lam3*v2*ZH[gt1, 3]*ZP[gt2, 2]*ZP[gt3, 2] - I*Sqrt[3]*lam4*v2*ZH[gt1, 1]*ZP[gt2, 3]*ZP[gt3, 2] - I*lam4*v1*ZH[gt1, 2]*ZP[gt2, 3]*ZP[gt3, 2] - (2*I)*lam3*v2*ZH[gt1, 2]*ZP[gt2, 3]*ZP[gt3, 2] - (2*I)*Sqrt[3]*lam3*v2*ZH[gt1, 3]*ZP[gt2, 3]*ZP[gt3, 2] - (I/2)*lam6*v2*ZH[gt1, 1]*ZP[gt2, 1]*ZP[gt3, 3] - I*lam7*v2*ZH[gt1, 1]*ZP[gt2, 1]*ZP[gt3, 3] - I*Sqrt[3]*lam4*v2*ZH[gt1, 2]*ZP[gt2, 1]*ZP[gt3, 3] - (I/2)*lam6*v1*ZH[gt1, 3]*ZP[gt2, 1]*ZP[gt3, 3] - I*lam7*v1*ZH[gt1, 3]*ZP[gt2, 1]*ZP[gt3, 3] + I*lam4*v2*ZH[gt1, 3]*ZP[gt2, 1]*ZP[gt3, 3] - I*Sqrt[3]*lam4*v2*ZH[gt1, 1]*ZP[gt2, 2]*ZP[gt3, 3] - I*lam4*v1*ZH[gt1, 2]*ZP[gt2, 2]*ZP[gt3, 3] - (2*I)*lam3*v2*ZH[gt1, 2]*ZP[gt2, 2]*ZP[gt3, 3] - (2*I)*Sqrt[3]*lam3*v2*ZH[gt1, 3]*ZP[gt2, 2]*ZP[gt3, 3] - I*lam5*v1*ZH[gt1, 1]*ZP[gt2, 3]*ZP[gt3, 3] + I*lam4*v2*ZH[gt1, 1]*ZP[gt2, 3]*ZP[gt3, 3] - (2*I)*Sqrt[3]*lam1*v2*ZH[gt1, 2]*ZP[gt2, 3]*ZP[gt3, 3] + (2*I)*Sqrt[3]*lam3*v2*ZH[gt1, 2]*ZP[gt2, 3]*ZP[gt3, 3] + I*lam4*v1*ZH[gt1, 3]*ZP[gt2, 3]*ZP[gt3, 3] - (2*I)*lam1*v2*ZH[gt1, 3]*ZP[gt2, 3]*ZP[gt3, 3] - (2*I)*lam3*v2*ZH[gt1, 3]*ZP[gt2, 3]*ZP[gt3, 3]}},
 C[S[1, {gt1}], S[10], S[10]] == {{(-I)*lam14*v1*ZH[gt1, 1] - I*Sqrt[3]*lam10*v2*ZH[gt1, 2] - I*Sqrt[3]*lam11*v2*ZH[gt1, 2] + (2*I)*Sqrt[3]*lam12*v2*ZH[gt1, 2] - I*lam10*v2*ZH[gt1, 3] - I*lam11*v2*ZH[gt1, 3] + (2*I)*lam12*v2*ZH[gt1, 3]}},
 C[S[3, {gt1}], S[10], -S[6]] == {{(lam14*v1*ZP[gt1, 1])/2 + (Sqrt[3]*lam11*v2*ZP[gt1, 2])/2 - Sqrt[3]*lam12*v2*ZP[gt1, 2] + (lam11*v2*ZP[gt1, 3])/2 - lam12*v2*ZP[gt1, 3]}},
 C[S[2, {gt1}], S[1, {gt2}], V[2]] == {{-(CTW*g2*ZA[gt1, 1]*ZH[gt2, 1])/2 - (g1*STW*ZA[gt1, 1]*ZH[gt2, 1])/2 - (CTW*g2*ZA[gt1, 2]*ZH[gt2, 2])/2 - (g1*STW*ZA[gt1, 2]*ZH[gt2, 2])/2 - (CTW*g2*ZA[gt1, 3]*ZH[gt2, 3])/2 - (g1*STW*ZA[gt1, 3]*ZH[gt2, 3])/2}},
 C[S[2, {gt1}], S[3, {gt2}], -V[3]] == {{(g2*ZA[gt1, 1]*ZP[gt2, 1])/2 + (g2*ZA[gt1, 2]*ZP[gt2, 2])/2 + (g2*ZA[gt1, 3]*ZP[gt2, 3])/2}},
 C[S[2, {gt1}], -S[3, {gt2}], V[3]] == {{(g2*ZA[gt1, 1]*ZP[gt2, 1])/2 + (g2*ZA[gt1, 2]*ZP[gt2, 2])/2 + (g2*ZA[gt1, 3]*ZP[gt2, 3])/2}},
 C[S[6], S[8], -V[3]] == {{(-I/2)*g2}},
 C[S[6], S[10], -V[3]] == {{-g2/2}},
 C[S[6], -S[6], V[1]] == {{(-I/2)*CTW*g1 - (I/2)*g2*STW}},
 C[S[6], -S[6], V[2]] == {{(-I/2)*CTW*g2 + (I/2)*g1*STW}},
 C[S[8], S[10], V[2]] == {{(CTW*g2)/2 + (g1*STW)/2}},
 C[S[8], -S[6], V[3]] == {{(-I/2)*g2}},
 C[S[1, {gt1}], S[3, {gt2}], -V[3]] == {{(I/2)*g2*ZH[gt1, 1]*ZP[gt2, 1] + (I/2)*g2*ZH[gt1, 2]*ZP[gt2, 2] + (I/2)*g2*ZH[gt1, 3]*ZP[gt2, 3]}},
 C[S[1, {gt1}], -S[3, {gt2}], V[3]] == {{(-I/2)*g2*ZH[gt1, 1]*ZP[gt2, 1] - (I/2)*g2*ZH[gt1, 2]*ZP[gt2, 2] - (I/2)*g2*ZH[gt1, 3]*ZP[gt2, 3]}},
 C[S[3, {gt1}], -S[3, {gt2}], V[1]] == {{(-I/2)*CTW*g1*ZP[gt1, 1]*ZP[gt2, 1] - (I/2)*g2*STW*ZP[gt1, 1]*ZP[gt2, 1] - (I/2)*CTW*g1*ZP[gt1, 2]*ZP[gt2, 2] - (I/2)*g2*STW*ZP[gt1, 2]*ZP[gt2, 2] - (I/2)*CTW*g1*ZP[gt1, 3]*ZP[gt2, 3] - (I/2)*g2*STW*ZP[gt1, 3]*ZP[gt2, 3]}},
 C[S[3, {gt1}], -S[3, {gt2}], V[2]] == {{(-I/2)*CTW*g2*ZP[gt1, 1]*ZP[gt2, 1] + (I/2)*g1*STW*ZP[gt1, 1]*ZP[gt2, 1] - (I/2)*CTW*g2*ZP[gt1, 2]*ZP[gt2, 2] + (I/2)*g1*STW*ZP[gt1, 2]*ZP[gt2, 2] - (I/2)*CTW*g2*ZP[gt1, 3]*ZP[gt2, 3] + (I/2)*g1*STW*ZP[gt1, 3]*ZP[gt2, 3]}},
 C[S[10], -S[6], V[3]] == {{g2/2}},
 C[S[1, {gt1}], -V[3], V[3]] == {{(I/2)*g2^2*v1*ZH[gt1, 1] + (I/2)*Sqrt[3]*g2^2*v2*ZH[gt1, 2] + (I/2)*g2^2*v2*ZH[gt1, 3]}},
 C[S[1, {gt1}], V[2], V[2]] == {{(I/4)*g1^2*v1*ZH[gt1, 1] - (I/4)*CTW^2*g1^2*v1*ZH[gt1, 1] + (I/4)*g2^2*v1*ZH[gt1, 1] + (I/4)*CTW^2*g2^2*v1*ZH[gt1, 1] + I*CTW*g1*g2*STW*v1*ZH[gt1, 1] + (I/4)*g1^2*STW^2*v1*ZH[gt1, 1] - (I/4)*g2^2*STW^2*v1*ZH[gt1, 1] + (I/4)*Sqrt[3]*g1^2*v2*ZH[gt1, 2] - (I/4)*Sqrt[3]*CTW^2*g1^2*v2*ZH[gt1, 2] + (I/4)*Sqrt[3]*g2^2*v2*ZH[gt1, 2] + (I/4)*Sqrt[3]*CTW^2*g2^2*v2*ZH[gt1, 2] + I*Sqrt[3]*CTW*g1*g2*STW*v2*ZH[gt1, 2] + (I/4)*Sqrt[3]*g1^2*STW^2*v2*ZH[gt1, 2] - (I/4)*Sqrt[3]*g2^2*STW^2*v2*ZH[gt1, 2] + (I/4)*g1^2*v2*ZH[gt1, 3] - (I/4)*CTW^2*g1^2*v2*ZH[gt1, 3] + (I/4)*g2^2*v2*ZH[gt1, 3] + (I/4)*CTW^2*g2^2*v2*ZH[gt1, 3] + I*CTW*g1*g2*STW*v2*ZH[gt1, 3] + (I/4)*g1^2*STW^2*v2*ZH[gt1, 3] - (I/4)*g2^2*STW^2*v2*ZH[gt1, 3]}},
 C[S[3, {gt1}], -V[3], V[1]] == {{(I/2)*CTW*g1*g2*v1*ZP[gt1, 1] + (I/2)*Sqrt[3]*CTW*g1*g2*v2*ZP[gt1, 2] + (I/2)*CTW*g1*g2*v2*ZP[gt1, 3]}},
 C[S[3, {gt1}], -V[3], V[2]] == {{(-I/2)*g1*g2*STW*v1*ZP[gt1, 1] - (I/2)*Sqrt[3]*g1*g2*STW*v2*ZP[gt1, 2] - (I/2)*g1*g2*STW*v2*ZP[gt1, 3]}},
 C[-S[3, {gt1}], V[1], V[3]] == {{(I/2)*CTW*g1*g2*v1*ZP[gt1, 1] + (I/2)*Sqrt[3]*CTW*g1*g2*v2*ZP[gt1, 2] + (I/2)*CTW*g1*g2*v2*ZP[gt1, 3]}},
 C[-S[3, {gt1}], V[3], V[2]] == {{(-I/2)*g1*g2*STW*v1*ZP[gt1, 1] - (I/2)*Sqrt[3]*g1*g2*STW*v2*ZP[gt1, 2] - (I/2)*g1*g2*STW*v2*ZP[gt1, 3]}},
 C[S[2, {gt1}], S[2, {gt2}], S[2, {gt3}], S[2, {gt4}]] == {{(-6*I)*lam8*ZA[gt1, 1]*ZA[gt2, 1]*ZA[gt3, 1]*ZA[gt4, 1] - I*lam5*ZA[gt1, 2]*ZA[gt2, 2]*ZA[gt3, 1]*ZA[gt4, 1] - I*lam6*ZA[gt1, 2]*ZA[gt2, 2]*ZA[gt3, 1]*ZA[gt4, 1] - (2*I)*lam7*ZA[gt1, 2]*ZA[gt2, 2]*ZA[gt3, 1]*ZA[gt4, 1] - I*lam5*ZA[gt1, 3]*ZA[gt2, 3]*ZA[gt3, 1]*ZA[gt4, 1] - I*lam6*ZA[gt1, 3]*ZA[gt2, 3]*ZA[gt3, 1]*ZA[gt4, 1] - (2*I)*lam7*ZA[gt1, 3]*ZA[gt2, 3]*ZA[gt3, 1]*ZA[gt4, 1] - I*lam5*ZA[gt1, 2]*ZA[gt2, 1]*ZA[gt3, 2]*ZA[gt4, 1] - I*lam6*ZA[gt1, 2]*ZA[gt2, 1]*ZA[gt3, 2]*ZA[gt4, 1] - (2*I)*lam7*ZA[gt1, 2]*ZA[gt2, 1]*ZA[gt3, 2]*ZA[gt4, 1] - I*lam5*ZA[gt1, 1]*ZA[gt2, 2]*ZA[gt3, 2]*ZA[gt4, 1] - I*lam6*ZA[gt1, 1]*ZA[gt2, 2]*ZA[gt3, 2]*ZA[gt4, 1] - (2*I)*lam7*ZA[gt1, 1]*ZA[gt2, 2]*ZA[gt3, 2]*ZA[gt4, 1] - (3*I)*lam4*ZA[gt1, 3]*ZA[gt2, 2]*ZA[gt3, 2]*ZA[gt4, 1] - (3*I)*lam4*ZA[gt1, 2]*ZA[gt2, 3]*ZA[gt3, 2]*ZA[gt4, 1] - I*lam5*ZA[gt1, 3]*ZA[gt2, 1]*ZA[gt3, 3]*ZA[gt4, 1] - I*lam6*ZA[gt1, 3]*ZA[gt2, 1]*ZA[gt3, 3]*ZA[gt4, 1] - (2*I)*lam7*ZA[gt1, 3]*ZA[gt2, 1]*ZA[gt3, 3]*ZA[gt4, 1] - (3*I)*lam4*ZA[gt1, 2]*ZA[gt2, 2]*ZA[gt3, 3]*ZA[gt4, 1] - I*lam5*ZA[gt1, 1]*ZA[gt2, 3]*ZA[gt3, 3]*ZA[gt4, 1] - I*lam6*ZA[gt1, 1]*ZA[gt2, 3]*ZA[gt3, 3]*ZA[gt4, 1] - (2*I)*lam7*ZA[gt1, 1]*ZA[gt2, 3]*ZA[gt3, 3]*ZA[gt4, 1] + (3*I)*lam4*ZA[gt1, 3]*ZA[gt2, 3]*ZA[gt3, 3]*ZA[gt4, 1] - I*lam5*ZA[gt1, 2]*ZA[gt2, 1]*ZA[gt3, 1]*ZA[gt4, 2] - I*lam6*ZA[gt1, 2]*ZA[gt2, 1]*ZA[gt3, 1]*ZA[gt4, 2] - (2*I)*lam7*ZA[gt1, 2]*ZA[gt2, 1]*ZA[gt3, 1]*ZA[gt4, 2] - I*lam5*ZA[gt1, 1]*ZA[gt2, 2]*ZA[gt3, 1]*ZA[gt4, 2] - I*lam6*ZA[gt1, 1]*ZA[gt2, 2]*ZA[gt3, 1]*ZA[gt4, 2] - (2*I)*lam7*ZA[gt1, 1]*ZA[gt2, 2]*ZA[gt3, 1]*ZA[gt4, 2] - (3*I)*lam4*ZA[gt1, 3]*ZA[gt2, 2]*ZA[gt3, 1]*ZA[gt4, 2] - (3*I)*lam4*ZA[gt1, 2]*ZA[gt2, 3]*ZA[gt3, 1]*ZA[gt4, 2] - I*lam5*ZA[gt1, 1]*ZA[gt2, 1]*ZA[gt3, 2]*ZA[gt4, 2] - I*lam6*ZA[gt1, 1]*ZA[gt2, 1]*ZA[gt3, 2]*ZA[gt4, 2] - (2*I)*lam7*ZA[gt1, 1]*ZA[gt2, 1]*ZA[gt3, 2]*ZA[gt4, 2] - (3*I)*lam4*ZA[gt1, 3]*ZA[gt2, 1]*ZA[gt3, 2]*ZA[gt4, 2] - (6*I)*lam1*ZA[gt1, 2]*ZA[gt2, 2]*ZA[gt3, 2]*ZA[gt4, 2] - (6*I)*lam3*ZA[gt1, 2]*ZA[gt2, 2]*ZA[gt3, 2]*ZA[gt4, 2] - (3*I)*lam4*ZA[gt1, 1]*ZA[gt2, 3]*ZA[gt3, 2]*ZA[gt4, 2] - (2*I)*lam1*ZA[gt1, 3]*ZA[gt2, 3]*ZA[gt3, 2]*ZA[gt4, 2] - (2*I)*lam3*ZA[gt1, 3]*ZA[gt2, 3]*ZA[gt3, 2]*ZA[gt4, 2] - (3*I)*lam4*ZA[gt1, 2]*ZA[gt2, 1]*ZA[gt3, 3]*ZA[gt4, 2] - (3*I)*lam4*ZA[gt1, 1]*ZA[gt2, 2]*ZA[gt3, 3]*ZA[gt4, 2] - (2*I)*lam1*ZA[gt1, 3]*ZA[gt2, 2]*ZA[gt3, 3]*ZA[gt4, 2] - (2*I)*lam3*ZA[gt1, 3]*ZA[gt2, 2]*ZA[gt3, 3]*ZA[gt4, 2] - (2*I)*lam1*ZA[gt1, 2]*ZA[gt2, 3]*ZA[gt3, 3]*ZA[gt4, 2] - (2*I)*lam3*ZA[gt1, 2]*ZA[gt2, 3]*ZA[gt3, 3]*ZA[gt4, 2] - I*lam5*ZA[gt1, 3]*ZA[gt2, 1]*ZA[gt3, 1]*ZA[gt4, 3] - I*lam6*ZA[gt1, 3]*ZA[gt2, 1]*ZA[gt3, 1]*ZA[gt4, 3] - (2*I)*lam7*ZA[gt1, 3]*ZA[gt2, 1]*ZA[gt3, 1]*ZA[gt4, 3] - (3*I)*lam4*ZA[gt1, 2]*ZA[gt2, 2]*ZA[gt3, 1]*ZA[gt4, 3] - I*lam5*ZA[gt1, 1]*ZA[gt2, 3]*ZA[gt3, 1]*ZA[gt4, 3] - I*lam6*ZA[gt1, 1]*ZA[gt2, 3]*ZA[gt3, 1]*ZA[gt4, 3] - (2*I)*lam7*ZA[gt1, 1]*ZA[gt2, 3]*ZA[gt3, 1]*ZA[gt4, 3] + (3*I)*lam4*ZA[gt1, 3]*ZA[gt2, 3]*ZA[gt3, 1]*ZA[gt4, 3] - (3*I)*lam4*ZA[gt1, 2]*ZA[gt2, 1]*ZA[gt3, 2]*ZA[gt4, 3] - (3*I)*lam4*ZA[gt1, 1]*ZA[gt2, 2]*ZA[gt3, 2]*ZA[gt4, 3] - (2*I)*lam1*ZA[gt1, 3]*ZA[gt2, 2]*ZA[gt3, 2]*ZA[gt4, 3] - (2*I)*lam3*ZA[gt1, 3]*ZA[gt2, 2]*ZA[gt3, 2]*ZA[gt4, 3] - (2*I)*lam1*ZA[gt1, 2]*ZA[gt2, 3]*ZA[gt3, 2]*ZA[gt4, 3] - (2*I)*lam3*ZA[gt1, 2]*ZA[gt2, 3]*ZA[gt3, 2]*ZA[gt4, 3] - I*lam5*ZA[gt1, 1]*ZA[gt2, 1]*ZA[gt3, 3]*ZA[gt4, 3] - I*lam6*ZA[gt1, 1]*ZA[gt2, 1]*ZA[gt3, 3]*ZA[gt4, 3] - (2*I)*lam7*ZA[gt1, 1]*ZA[gt2, 1]*ZA[gt3, 3]*ZA[gt4, 3] + (3*I)*lam4*ZA[gt1, 3]*ZA[gt2, 1]*ZA[gt3, 3]*ZA[gt4, 3] - (2*I)*lam1*ZA[gt1, 2]*ZA[gt2, 2]*ZA[gt3, 3]*ZA[gt4, 3] - (2*I)*lam3*ZA[gt1, 2]*ZA[gt2, 2]*ZA[gt3, 3]*ZA[gt4, 3] + (3*I)*lam4*ZA[gt1, 1]*ZA[gt2, 3]*ZA[gt3, 3]*ZA[gt4, 3] - (6*I)*lam1*ZA[gt1, 3]*ZA[gt2, 3]*ZA[gt3, 3]*ZA[gt4, 3] - (6*I)*lam3*ZA[gt1, 3]*ZA[gt2, 3]*ZA[gt3, 3]*ZA[gt4, 3]}},
 C[S[2, {gt1}], S[2, {gt2}], S[6], -S[6]] == {{(-I)*lam10*ZA[gt1, 2]*ZA[gt2, 2] - I*lam10*ZA[gt1, 3]*ZA[gt2, 3]}},
 C[S[2, {gt1}], S[2, {gt2}], S[8], S[8]] == {{(-I)*lam14*ZA[gt1, 1]*ZA[gt2, 1] - I*lam10*ZA[gt1, 2]*ZA[gt2, 2] - I*lam11*ZA[gt1, 2]*ZA[gt2, 2] + (2*I)*lam12*ZA[gt1, 2]*ZA[gt2, 2] - I*lam10*ZA[gt1, 3]*ZA[gt2, 3] - I*lam11*ZA[gt1, 3]*ZA[gt2, 3] + (2*I)*lam12*ZA[gt1, 3]*ZA[gt2, 3]}},
 C[S[2, {gt1}], S[2, {gt2}], S[1, {gt3}], S[1, {gt4}]] == {{(-2*I)*lam8*ZA[gt1, 1]*ZA[gt2, 1]*ZH[gt3, 1]*ZH[gt4, 1] - I*lam5*ZA[gt1, 2]*ZA[gt2, 2]*ZH[gt3, 1]*ZH[gt4, 1] - I*lam6*ZA[gt1, 2]*ZA[gt2, 2]*ZH[gt3, 1]*ZH[gt4, 1] + (2*I)*lam7*ZA[gt1, 2]*ZA[gt2, 2]*ZH[gt3, 1]*ZH[gt4, 1] - I*lam5*ZA[gt1, 3]*ZA[gt2, 3]*ZH[gt3, 1]*ZH[gt4, 1] - I*lam6*ZA[gt1, 3]*ZA[gt2, 3]*ZH[gt3, 1]*ZH[gt4, 1] + (2*I)*lam7*ZA[gt1, 3]*ZA[gt2, 3]*ZH[gt3, 1]*ZH[gt4, 1] - (2*I)*lam7*ZA[gt1, 2]*ZA[gt2, 1]*ZH[gt3, 2]*ZH[gt4, 1] - (2*I)*lam7*ZA[gt1, 1]*ZA[gt2, 2]*ZH[gt3, 2]*ZH[gt4, 1] - I*lam4*ZA[gt1, 3]*ZA[gt2, 2]*ZH[gt3, 2]*ZH[gt4, 1] - I*lam4*ZA[gt1, 2]*ZA[gt2, 3]*ZH[gt3, 2]*ZH[gt4, 1] - (2*I)*lam7*ZA[gt1, 3]*ZA[gt2, 1]*ZH[gt3, 3]*ZH[gt4, 1] - I*lam4*ZA[gt1, 2]*ZA[gt2, 2]*ZH[gt3, 3]*ZH[gt4, 1] - (2*I)*lam7*ZA[gt1, 1]*ZA[gt2, 3]*ZH[gt3, 3]*ZH[gt4, 1] + I*lam4*ZA[gt1, 3]*ZA[gt2, 3]*ZH[gt3, 3]*ZH[gt4, 1] - (2*I)*lam7*ZA[gt1, 2]*ZA[gt2, 1]*ZH[gt3, 1]*ZH[gt4, 2] - (2*I)*lam7*ZA[gt1, 1]*ZA[gt2, 2]*ZH[gt3, 1]*ZH[gt4, 2] - I*lam4*ZA[gt1, 3]*ZA[gt2, 2]*ZH[gt3, 1]*ZH[gt4, 2] - I*lam4*ZA[gt1, 2]*ZA[gt2, 3]*ZH[gt3, 1]*ZH[gt4, 2] - I*lam5*ZA[gt1, 1]*ZA[gt2, 1]*ZH[gt3, 2]*ZH[gt4, 2] - I*lam6*ZA[gt1, 1]*ZA[gt2, 1]*ZH[gt3, 2]*ZH[gt4, 2] + (2*I)*lam7*ZA[gt1, 1]*ZA[gt2, 1]*ZH[gt3, 2]*ZH[gt4, 2] - I*lam4*ZA[gt1, 3]*ZA[gt2, 1]*ZH[gt3, 2]*ZH[gt4, 2] - (2*I)*lam1*ZA[gt1, 2]*ZA[gt2, 2]*ZH[gt3, 2]*ZH[gt4, 2] - (2*I)*lam3*ZA[gt1, 2]*ZA[gt2, 2]*ZH[gt3, 2]*ZH[gt4, 2] - I*lam4*ZA[gt1, 1]*ZA[gt2, 3]*ZH[gt3, 2]*ZH[gt4, 2] - (2*I)*lam1*ZA[gt1, 3]*ZA[gt2, 3]*ZH[gt3, 2]*ZH[gt4, 2] + (4*I)*lam2*ZA[gt1, 3]*ZA[gt2, 3]*ZH[gt3, 2]*ZH[gt4, 2] + (2*I)*lam3*ZA[gt1, 3]*ZA[gt2, 3]*ZH[gt3, 2]*ZH[gt4, 2] - I*lam4*ZA[gt1, 2]*ZA[gt2, 1]*ZH[gt3, 3]*ZH[gt4, 2] - I*lam4*ZA[gt1, 1]*ZA[gt2, 2]*ZH[gt3, 3]*ZH[gt4, 2] - (2*I)*lam2*ZA[gt1, 3]*ZA[gt2, 2]*ZH[gt3, 3]*ZH[gt4, 2] - (2*I)*lam3*ZA[gt1, 3]*ZA[gt2, 2]*ZH[gt3, 3]*ZH[gt4, 2] - (2*I)*lam2*ZA[gt1, 2]*ZA[gt2, 3]*ZH[gt3, 3]*ZH[gt4, 2] - (2*I)*lam3*ZA[gt1, 2]*ZA[gt2, 3]*ZH[gt3, 3]*ZH[gt4, 2] - (2*I)*lam7*ZA[gt1, 3]*ZA[gt2, 1]*ZH[gt3, 1]*ZH[gt4, 3] - I*lam4*ZA[gt1, 2]*ZA[gt2, 2]*ZH[gt3, 1]*ZH[gt4, 3] - (2*I)*lam7*ZA[gt1, 1]*ZA[gt2, 3]*ZH[gt3, 1]*ZH[gt4, 3] + I*lam4*ZA[gt1, 3]*ZA[gt2, 3]*ZH[gt3, 1]*ZH[gt4, 3] - I*lam4*ZA[gt1, 2]*ZA[gt2, 1]*ZH[gt3, 2]*ZH[gt4, 3] - I*lam4*ZA[gt1, 1]*ZA[gt2, 2]*ZH[gt3, 2]*ZH[gt4, 3] - (2*I)*lam2*ZA[gt1, 3]*ZA[gt2, 2]*ZH[gt3, 2]*ZH[gt4, 3] - (2*I)*lam3*ZA[gt1, 3]*ZA[gt2, 2]*ZH[gt3, 2]*ZH[gt4, 3] - (2*I)*lam2*ZA[gt1, 2]*ZA[gt2, 3]*ZH[gt3, 2]*ZH[gt4, 3] - (2*I)*lam3*ZA[gt1, 2]*ZA[gt2, 3]*ZH[gt3, 2]*ZH[gt4, 3] - I*lam5*ZA[gt1, 1]*ZA[gt2, 1]*ZH[gt3, 3]*ZH[gt4, 3] - I*lam6*ZA[gt1, 1]*ZA[gt2, 1]*ZH[gt3, 3]*ZH[gt4, 3] + (2*I)*lam7*ZA[gt1, 1]*ZA[gt2, 1]*ZH[gt3, 3]*ZH[gt4, 3] + I*lam4*ZA[gt1, 3]*ZA[gt2, 1]*ZH[gt3, 3]*ZH[gt4, 3] - (2*I)*lam1*ZA[gt1, 2]*ZA[gt2, 2]*ZH[gt3, 3]*ZH[gt4, 3] + (4*I)*lam2*ZA[gt1, 2]*ZA[gt2, 2]*ZH[gt3, 3]*ZH[gt4, 3] + (2*I)*lam3*ZA[gt1, 2]*ZA[gt2, 2]*ZH[gt3, 3]*ZH[gt4, 3] + I*lam4*ZA[gt1, 1]*ZA[gt2, 3]*ZH[gt3, 3]*ZH[gt4, 3] - (2*I)*lam1*ZA[gt1, 3]*ZA[gt2, 3]*ZH[gt3, 3]*ZH[gt4, 3] - (2*I)*lam3*ZA[gt1, 3]*ZA[gt2, 3]*ZH[gt3, 3]*ZH[gt4, 3]}},
 C[S[2, {gt1}], S[2, {gt2}], S[3, {gt3}], -S[3, {gt4}]] == {{(-2*I)*lam8*ZA[gt1, 1]*ZA[gt2, 1]*ZP[gt3, 1]*ZP[gt4, 1] - I*lam5*ZA[gt1, 2]*ZA[gt2, 2]*ZP[gt3, 1]*ZP[gt4, 1] - I*lam5*ZA[gt1, 3]*ZA[gt2, 3]*ZP[gt3, 1]*ZP[gt4, 1] - (I/2)*lam6*ZA[gt1, 2]*ZA[gt2, 1]*ZP[gt3, 2]*ZP[gt4, 1] - I*lam7*ZA[gt1, 2]*ZA[gt2, 1]*ZP[gt3, 2]*ZP[gt4, 1] - (I/2)*lam6*ZA[gt1, 1]*ZA[gt2, 2]*ZP[gt3, 2]*ZP[gt4, 1] - I*lam7*ZA[gt1, 1]*ZA[gt2, 2]*ZP[gt3, 2]*ZP[gt4, 1] - I*lam4*ZA[gt1, 3]*ZA[gt2, 2]*ZP[gt3, 2]*ZP[gt4, 1] - I*lam4*ZA[gt1, 2]*ZA[gt2, 3]*ZP[gt3, 2]*ZP[gt4, 1] - (I/2)*lam6*ZA[gt1, 3]*ZA[gt2, 1]*ZP[gt3, 3]*ZP[gt4, 1] - I*lam7*ZA[gt1, 3]*ZA[gt2, 1]*ZP[gt3, 3]*ZP[gt4, 1] - I*lam4*ZA[gt1, 2]*ZA[gt2, 2]*ZP[gt3, 3]*ZP[gt4, 1] - (I/2)*lam6*ZA[gt1, 1]*ZA[gt2, 3]*ZP[gt3, 3]*ZP[gt4, 1] - I*lam7*ZA[gt1, 1]*ZA[gt2, 3]*ZP[gt3, 3]*ZP[gt4, 1] + I*lam4*ZA[gt1, 3]*ZA[gt2, 3]*ZP[gt3, 3]*ZP[gt4, 1] - (I/2)*lam6*ZA[gt1, 2]*ZA[gt2, 1]*ZP[gt3, 1]*ZP[gt4, 2] - I*lam7*ZA[gt1, 2]*ZA[gt2, 1]*ZP[gt3, 1]*ZP[gt4, 2] - (I/2)*lam6*ZA[gt1, 1]*ZA[gt2, 2]*ZP[gt3, 1]*ZP[gt4, 2] - I*lam7*ZA[gt1, 1]*ZA[gt2, 2]*ZP[gt3, 1]*ZP[gt4, 2] - I*lam4*ZA[gt1, 3]*ZA[gt2, 2]*ZP[gt3, 1]*ZP[gt4, 2] - I*lam4*ZA[gt1, 2]*ZA[gt2, 3]*ZP[gt3, 1]*ZP[gt4, 2] - I*lam5*ZA[gt1, 1]*ZA[gt2, 1]*ZP[gt3, 2]*ZP[gt4, 2] - I*lam4*ZA[gt1, 3]*ZA[gt2, 1]*ZP[gt3, 2]*ZP[gt4, 2] - (2*I)*lam1*ZA[gt1, 2]*ZA[gt2, 2]*ZP[gt3, 2]*ZP[gt4, 2] - (2*I)*lam3*ZA[gt1, 2]*ZA[gt2, 2]*ZP[gt3, 2]*ZP[gt4, 2] - I*lam4*ZA[gt1, 1]*ZA[gt2, 3]*ZP[gt3, 2]*ZP[gt4, 2] - (2*I)*lam1*ZA[gt1, 3]*ZA[gt2, 3]*ZP[gt3, 2]*ZP[gt4, 2] + (2*I)*lam3*ZA[gt1, 3]*ZA[gt2, 3]*ZP[gt3, 2]*ZP[gt4, 2] - I*lam4*ZA[gt1, 2]*ZA[gt2, 1]*ZP[gt3, 3]*ZP[gt4, 2] - I*lam4*ZA[gt1, 1]*ZA[gt2, 2]*ZP[gt3, 3]*ZP[gt4, 2] - (2*I)*lam3*ZA[gt1, 3]*ZA[gt2, 2]*ZP[gt3, 3]*ZP[gt4, 2] - (2*I)*lam3*ZA[gt1, 2]*ZA[gt2, 3]*ZP[gt3, 3]*ZP[gt4, 2] - (I/2)*lam6*ZA[gt1, 3]*ZA[gt2, 1]*ZP[gt3, 1]*ZP[gt4, 3] - I*lam7*ZA[gt1, 3]*ZA[gt2, 1]*ZP[gt3, 1]*ZP[gt4, 3] - I*lam4*ZA[gt1, 2]*ZA[gt2, 2]*ZP[gt3, 1]*ZP[gt4, 3] - (I/2)*lam6*ZA[gt1, 1]*ZA[gt2, 3]*ZP[gt3, 1]*ZP[gt4, 3] - I*lam7*ZA[gt1, 1]*ZA[gt2, 3]*ZP[gt3, 1]*ZP[gt4, 3] + I*lam4*ZA[gt1, 3]*ZA[gt2, 3]*ZP[gt3, 1]*ZP[gt4, 3] - I*lam4*ZA[gt1, 2]*ZA[gt2, 1]*ZP[gt3, 2]*ZP[gt4, 3] - I*lam4*ZA[gt1, 1]*ZA[gt2, 2]*ZP[gt3, 2]*ZP[gt4, 3] - (2*I)*lam3*ZA[gt1, 3]*ZA[gt2, 2]*ZP[gt3, 2]*ZP[gt4, 3] - (2*I)*lam3*ZA[gt1, 2]*ZA[gt2, 3]*ZP[gt3, 2]*ZP[gt4, 3] - I*lam5*ZA[gt1, 1]*ZA[gt2, 1]*ZP[gt3, 3]*ZP[gt4, 3] + I*lam4*ZA[gt1, 3]*ZA[gt2, 1]*ZP[gt3, 3]*ZP[gt4, 3] - (2*I)*lam1*ZA[gt1, 2]*ZA[gt2, 2]*ZP[gt3, 3]*ZP[gt4, 3] + (2*I)*lam3*ZA[gt1, 2]*ZA[gt2, 2]*ZP[gt3, 3]*ZP[gt4, 3] + I*lam4*ZA[gt1, 1]*ZA[gt2, 3]*ZP[gt3, 3]*ZP[gt4, 3] - (2*I)*lam1*ZA[gt1, 3]*ZA[gt2, 3]*ZP[gt3, 3]*ZP[gt4, 3] - (2*I)*lam3*ZA[gt1, 3]*ZA[gt2, 3]*ZP[gt3, 3]*ZP[gt4, 3]}},
 C[S[2, {gt1}], S[2, {gt2}], S[10], S[10]] == {{(-I)*lam14*ZA[gt1, 1]*ZA[gt2, 1] - I*lam10*ZA[gt1, 2]*ZA[gt2, 2] - I*lam11*ZA[gt1, 2]*ZA[gt2, 2] - (2*I)*lam12*ZA[gt1, 2]*ZA[gt2, 2] - I*lam10*ZA[gt1, 3]*ZA[gt2, 3] - I*lam11*ZA[gt1, 3]*ZA[gt2, 3] - (2*I)*lam12*ZA[gt1, 3]*ZA[gt2, 3]}},
 C[S[2, {gt1}], S[6], S[8], -S[3, {gt4}]] == {{(lam14*ZA[gt1, 1]*ZP[gt4, 1])/2 + (lam11*ZA[gt1, 2]*ZP[gt4, 2])/2 - lam12*ZA[gt1, 2]*ZP[gt4, 2] + (lam11*ZA[gt1, 3]*ZP[gt4, 3])/2 - lam12*ZA[gt1, 3]*ZP[gt4, 3]}},
 C[S[2, {gt1}], S[6], S[10], -S[3, {gt4}]] == {{(-I/2)*lam14*ZA[gt1, 1]*ZP[gt4, 1] - (I/2)*lam11*ZA[gt1, 2]*ZP[gt4, 2] - I*lam12*ZA[gt1, 2]*ZP[gt4, 2] - (I/2)*lam11*ZA[gt1, 3]*ZP[gt4, 3] - I*lam12*ZA[gt1, 3]*ZP[gt4, 3]}},
 C[S[2, {gt1}], S[8], S[1, {gt3}], S[10]] == {{(-2*I)*lam12*ZA[gt1, 2]*ZH[gt3, 2] - (2*I)*lam12*ZA[gt1, 3]*ZH[gt3, 3]}},
 C[S[2, {gt1}], S[8], S[3, {gt3}], -S[6]] == {{-(lam14*ZA[gt1, 1]*ZP[gt3, 1])/2 - (lam11*ZA[gt1, 2]*ZP[gt3, 2])/2 + lam12*ZA[gt1, 2]*ZP[gt3, 2] - (lam11*ZA[gt1, 3]*ZP[gt3, 3])/2 + lam12*ZA[gt1, 3]*ZP[gt3, 3]}},
 C[S[2, {gt1}], S[1, {gt2}], S[3, {gt3}], -S[3, {gt4}]] == {{-(lam6*ZA[gt1, 2]*ZH[gt2, 1]*ZP[gt3, 2]*ZP[gt4, 1])/2 + lam7*ZA[gt1, 2]*ZH[gt2, 1]*ZP[gt3, 2]*ZP[gt4, 1] + (lam6*ZA[gt1, 1]*ZH[gt2, 2]*ZP[gt3, 2]*ZP[gt4, 1])/2 - lam7*ZA[gt1, 1]*ZH[gt2, 2]*ZP[gt3, 2]*ZP[gt4, 1] - (lam6*ZA[gt1, 3]*ZH[gt2, 1]*ZP[gt3, 3]*ZP[gt4, 1])/2 + lam7*ZA[gt1, 3]*ZH[gt2, 1]*ZP[gt3, 3]*ZP[gt4, 1] + (lam6*ZA[gt1, 1]*ZH[gt2, 3]*ZP[gt3, 3]*ZP[gt4, 1])/2 - lam7*ZA[gt1, 1]*ZH[gt2, 3]*ZP[gt3, 3]*ZP[gt4, 1] + (lam6*ZA[gt1, 2]*ZH[gt2, 1]*ZP[gt3, 1]*ZP[gt4, 2])/2 - lam7*ZA[gt1, 2]*ZH[gt2, 1]*ZP[gt3, 1]*ZP[gt4, 2] - (lam6*ZA[gt1, 1]*ZH[gt2, 2]*ZP[gt3, 1]*ZP[gt4, 2])/2 + lam7*ZA[gt1, 1]*ZH[gt2, 2]*ZP[gt3, 1]*ZP[gt4, 2] + 2*lam2*ZA[gt1, 3]*ZH[gt2, 2]*ZP[gt3, 3]*ZP[gt4, 2] - 2*lam2*ZA[gt1, 2]*ZH[gt2, 3]*ZP[gt3, 3]*ZP[gt4, 2] + (lam6*ZA[gt1, 3]*ZH[gt2, 1]*ZP[gt3, 1]*ZP[gt4, 3])/2 - lam7*ZA[gt1, 3]*ZH[gt2, 1]*ZP[gt3, 1]*ZP[gt4, 3] - (lam6*ZA[gt1, 1]*ZH[gt2, 3]*ZP[gt3, 1]*ZP[gt4, 3])/2 + lam7*ZA[gt1, 1]*ZH[gt2, 3]*ZP[gt3, 1]*ZP[gt4, 3] - 2*lam2*ZA[gt1, 3]*ZH[gt2, 2]*ZP[gt3, 2]*ZP[gt4, 3] + 2*lam2*ZA[gt1, 2]*ZH[gt2, 3]*ZP[gt3, 2]*ZP[gt4, 3]}},
 C[S[2, {gt1}], S[3, {gt2}], S[10], -S[6]] == {{(-I/2)*lam14*ZA[gt1, 1]*ZP[gt2, 1] - (I/2)*lam11*ZA[gt1, 2]*ZP[gt2, 2] - I*lam12*ZA[gt1, 2]*ZP[gt2, 2] - (I/2)*lam11*ZA[gt1, 3]*ZP[gt2, 3] - I*lam12*ZA[gt1, 3]*ZP[gt2, 3]}},
 C[S[6], S[6], -S[6], -S[6]] == {{(-4*I)*lam13}},
 C[S[6], S[6], -S[3, {gt3}], -S[3, {gt4}]] == {{(-4*I)*lam12*ZP[gt3, 2]*ZP[gt4, 2] - (4*I)*lam12*ZP[gt3, 3]*ZP[gt4, 3]}},
 C[S[6], S[8], S[8], -S[6]] == {{(-2*I)*lam13}},
 C[S[6], S[8], S[1, {gt3}], -S[3, {gt4}]] == {{(-I/2)*lam14*ZH[gt3, 1]*ZP[gt4, 1] - (I/2)*lam11*ZH[gt3, 2]*ZP[gt4, 2] - I*lam12*ZH[gt3, 2]*ZP[gt4, 2] - (I/2)*lam11*ZH[gt3, 3]*ZP[gt4, 3] - I*lam12*ZH[gt3, 3]*ZP[gt4, 3]}},
 C[S[6], S[1, {gt2}], S[1, {gt3}], -S[6]] == {{(-I)*lam10*ZH[gt2, 2]*ZH[gt3, 2] - I*lam10*ZH[gt2, 3]*ZH[gt3, 3]}},
 C[S[6], S[1, {gt2}], S[10], -S[3, {gt4}]] == {{-(lam14*ZH[gt2, 1]*ZP[gt4, 1])/2 - (lam11*ZH[gt2, 2]*ZP[gt4, 2])/2 + lam12*ZH[gt2, 2]*ZP[gt4, 2] - (lam11*ZH[gt2, 3]*ZP[gt4, 3])/2 + lam12*ZH[gt2, 3]*ZP[gt4, 3]}},
 C[S[6], S[3, {gt2}], -S[6], -S[3, {gt4}]] == {{(-I)*lam14*ZP[gt2, 1]*ZP[gt4, 1] - I*lam10*ZP[gt2, 2]*ZP[gt4, 2] - I*lam11*ZP[gt2, 2]*ZP[gt4, 2] - I*lam10*ZP[gt2, 3]*ZP[gt4, 3] - I*lam11*ZP[gt2, 3]*ZP[gt4, 3]}},
 C[S[6], S[10], S[10], -S[6]] == {{(-2*I)*lam13}},
 C[S[8], S[8], S[8], S[8]] == {{(-6*I)*lam13}},
 C[S[8], S[8], S[1, {gt3}], S[1, {gt4}]] == {{(-I)*lam14*ZH[gt3, 1]*ZH[gt4, 1] - I*lam10*ZH[gt3, 2]*ZH[gt4, 2] - I*lam11*ZH[gt3, 2]*ZH[gt4, 2] - (2*I)*lam12*ZH[gt3, 2]*ZH[gt4, 2] - I*lam10*ZH[gt3, 3]*ZH[gt4, 3] - I*lam11*ZH[gt3, 3]*ZH[gt4, 3] - (2*I)*lam12*ZH[gt3, 3]*ZH[gt4, 3]}},
 C[S[8], S[8], S[3, {gt3}], -S[3, {gt4}]] == {{(-I)*lam10*ZP[gt3, 2]*ZP[gt4, 2] - I*lam10*ZP[gt3, 3]*ZP[gt4, 3]}},
 C[S[8], S[8], S[10], S[10]] == {{(-2*I)*lam13}},
 C[S[8], S[1, {gt2}], S[3, {gt3}], -S[6]] == {{(-I/2)*lam14*ZH[gt2, 1]*ZP[gt3, 1] - (I/2)*lam11*ZH[gt2, 2]*ZP[gt3, 2] - I*lam12*ZH[gt2, 2]*ZP[gt3, 2] - (I/2)*lam11*ZH[gt2, 3]*ZP[gt3, 3] - I*lam12*ZH[gt2, 3]*ZP[gt3, 3]}},
 C[S[1, {gt1}], S[1, {gt2}], S[1, {gt3}], S[1, {gt4}]] == {{(-6*I)*lam8*ZH[gt1, 1]*ZH[gt2, 1]*ZH[gt3, 1]*ZH[gt4, 1] - I*lam5*ZH[gt1, 2]*ZH[gt2, 2]*ZH[gt3, 1]*ZH[gt4, 1] - I*lam6*ZH[gt1, 2]*ZH[gt2, 2]*ZH[gt3, 1]*ZH[gt4, 1] - (2*I)*lam7*ZH[gt1, 2]*ZH[gt2, 2]*ZH[gt3, 1]*ZH[gt4, 1] - I*lam5*ZH[gt1, 3]*ZH[gt2, 3]*ZH[gt3, 1]*ZH[gt4, 1] - I*lam6*ZH[gt1, 3]*ZH[gt2, 3]*ZH[gt3, 1]*ZH[gt4, 1] - (2*I)*lam7*ZH[gt1, 3]*ZH[gt2, 3]*ZH[gt3, 1]*ZH[gt4, 1] - I*lam5*ZH[gt1, 2]*ZH[gt2, 1]*ZH[gt3, 2]*ZH[gt4, 1] - I*lam6*ZH[gt1, 2]*ZH[gt2, 1]*ZH[gt3, 2]*ZH[gt4, 1] - (2*I)*lam7*ZH[gt1, 2]*ZH[gt2, 1]*ZH[gt3, 2]*ZH[gt4, 1] - I*lam5*ZH[gt1, 1]*ZH[gt2, 2]*ZH[gt3, 2]*ZH[gt4, 1] - I*lam6*ZH[gt1, 1]*ZH[gt2, 2]*ZH[gt3, 2]*ZH[gt4, 1] - (2*I)*lam7*ZH[gt1, 1]*ZH[gt2, 2]*ZH[gt3, 2]*ZH[gt4, 1] - (3*I)*lam4*ZH[gt1, 3]*ZH[gt2, 2]*ZH[gt3, 2]*ZH[gt4, 1] - (3*I)*lam4*ZH[gt1, 2]*ZH[gt2, 3]*ZH[gt3, 2]*ZH[gt4, 1] - I*lam5*ZH[gt1, 3]*ZH[gt2, 1]*ZH[gt3, 3]*ZH[gt4, 1] - I*lam6*ZH[gt1, 3]*ZH[gt2, 1]*ZH[gt3, 3]*ZH[gt4, 1] - (2*I)*lam7*ZH[gt1, 3]*ZH[gt2, 1]*ZH[gt3, 3]*ZH[gt4, 1] - (3*I)*lam4*ZH[gt1, 2]*ZH[gt2, 2]*ZH[gt3, 3]*ZH[gt4, 1] - I*lam5*ZH[gt1, 1]*ZH[gt2, 3]*ZH[gt3, 3]*ZH[gt4, 1] - I*lam6*ZH[gt1, 1]*ZH[gt2, 3]*ZH[gt3, 3]*ZH[gt4, 1] - (2*I)*lam7*ZH[gt1, 1]*ZH[gt2, 3]*ZH[gt3, 3]*ZH[gt4, 1] + (3*I)*lam4*ZH[gt1, 3]*ZH[gt2, 3]*ZH[gt3, 3]*ZH[gt4, 1] - I*lam5*ZH[gt1, 2]*ZH[gt2, 1]*ZH[gt3, 1]*ZH[gt4, 2] - I*lam6*ZH[gt1, 2]*ZH[gt2, 1]*ZH[gt3, 1]*ZH[gt4, 2] - (2*I)*lam7*ZH[gt1, 2]*ZH[gt2, 1]*ZH[gt3, 1]*ZH[gt4, 2] - I*lam5*ZH[gt1, 1]*ZH[gt2, 2]*ZH[gt3, 1]*ZH[gt4, 2] - I*lam6*ZH[gt1, 1]*ZH[gt2, 2]*ZH[gt3, 1]*ZH[gt4, 2] - (2*I)*lam7*ZH[gt1, 1]*ZH[gt2, 2]*ZH[gt3, 1]*ZH[gt4, 2] - (3*I)*lam4*ZH[gt1, 3]*ZH[gt2, 2]*ZH[gt3, 1]*ZH[gt4, 2] - (3*I)*lam4*ZH[gt1, 2]*ZH[gt2, 3]*ZH[gt3, 1]*ZH[gt4, 2] - I*lam5*ZH[gt1, 1]*ZH[gt2, 1]*ZH[gt3, 2]*ZH[gt4, 2] - I*lam6*ZH[gt1, 1]*ZH[gt2, 1]*ZH[gt3, 2]*ZH[gt4, 2] - (2*I)*lam7*ZH[gt1, 1]*ZH[gt2, 1]*ZH[gt3, 2]*ZH[gt4, 2] - (3*I)*lam4*ZH[gt1, 3]*ZH[gt2, 1]*ZH[gt3, 2]*ZH[gt4, 2] - (6*I)*lam1*ZH[gt1, 2]*ZH[gt2, 2]*ZH[gt3, 2]*ZH[gt4, 2] - (6*I)*lam3*ZH[gt1, 2]*ZH[gt2, 2]*ZH[gt3, 2]*ZH[gt4, 2] - (3*I)*lam4*ZH[gt1, 1]*ZH[gt2, 3]*ZH[gt3, 2]*ZH[gt4, 2] - (2*I)*lam1*ZH[gt1, 3]*ZH[gt2, 3]*ZH[gt3, 2]*ZH[gt4, 2] - (2*I)*lam3*ZH[gt1, 3]*ZH[gt2, 3]*ZH[gt3, 2]*ZH[gt4, 2] - (3*I)*lam4*ZH[gt1, 2]*ZH[gt2, 1]*ZH[gt3, 3]*ZH[gt4, 2] - (3*I)*lam4*ZH[gt1, 1]*ZH[gt2, 2]*ZH[gt3, 3]*ZH[gt4, 2] - (2*I)*lam1*ZH[gt1, 3]*ZH[gt2, 2]*ZH[gt3, 3]*ZH[gt4, 2] - (2*I)*lam3*ZH[gt1, 3]*ZH[gt2, 2]*ZH[gt3, 3]*ZH[gt4, 2] - (2*I)*lam1*ZH[gt1, 2]*ZH[gt2, 3]*ZH[gt3, 3]*ZH[gt4, 2] - (2*I)*lam3*ZH[gt1, 2]*ZH[gt2, 3]*ZH[gt3, 3]*ZH[gt4, 2] - I*lam5*ZH[gt1, 3]*ZH[gt2, 1]*ZH[gt3, 1]*ZH[gt4, 3] - I*lam6*ZH[gt1, 3]*ZH[gt2, 1]*ZH[gt3, 1]*ZH[gt4, 3] - (2*I)*lam7*ZH[gt1, 3]*ZH[gt2, 1]*ZH[gt3, 1]*ZH[gt4, 3] - (3*I)*lam4*ZH[gt1, 2]*ZH[gt2, 2]*ZH[gt3, 1]*ZH[gt4, 3] - I*lam5*ZH[gt1, 1]*ZH[gt2, 3]*ZH[gt3, 1]*ZH[gt4, 3] - I*lam6*ZH[gt1, 1]*ZH[gt2, 3]*ZH[gt3, 1]*ZH[gt4, 3] - (2*I)*lam7*ZH[gt1, 1]*ZH[gt2, 3]*ZH[gt3, 1]*ZH[gt4, 3] + (3*I)*lam4*ZH[gt1, 3]*ZH[gt2, 3]*ZH[gt3, 1]*ZH[gt4, 3] - (3*I)*lam4*ZH[gt1, 2]*ZH[gt2, 1]*ZH[gt3, 2]*ZH[gt4, 3] - (3*I)*lam4*ZH[gt1, 1]*ZH[gt2, 2]*ZH[gt3, 2]*ZH[gt4, 3] - (2*I)*lam1*ZH[gt1, 3]*ZH[gt2, 2]*ZH[gt3, 2]*ZH[gt4, 3] - (2*I)*lam3*ZH[gt1, 3]*ZH[gt2, 2]*ZH[gt3, 2]*ZH[gt4, 3] - (2*I)*lam1*ZH[gt1, 2]*ZH[gt2, 3]*ZH[gt3, 2]*ZH[gt4, 3] - (2*I)*lam3*ZH[gt1, 2]*ZH[gt2, 3]*ZH[gt3, 2]*ZH[gt4, 3] - I*lam5*ZH[gt1, 1]*ZH[gt2, 1]*ZH[gt3, 3]*ZH[gt4, 3] - I*lam6*ZH[gt1, 1]*ZH[gt2, 1]*ZH[gt3, 3]*ZH[gt4, 3] - (2*I)*lam7*ZH[gt1, 1]*ZH[gt2, 1]*ZH[gt3, 3]*ZH[gt4, 3] + (3*I)*lam4*ZH[gt1, 3]*ZH[gt2, 1]*ZH[gt3, 3]*ZH[gt4, 3] - (2*I)*lam1*ZH[gt1, 2]*ZH[gt2, 2]*ZH[gt3, 3]*ZH[gt4, 3] - (2*I)*lam3*ZH[gt1, 2]*ZH[gt2, 2]*ZH[gt3, 3]*ZH[gt4, 3] + (3*I)*lam4*ZH[gt1, 1]*ZH[gt2, 3]*ZH[gt3, 3]*ZH[gt4, 3] - (6*I)*lam1*ZH[gt1, 3]*ZH[gt2, 3]*ZH[gt3, 3]*ZH[gt4, 3] - (6*I)*lam3*ZH[gt1, 3]*ZH[gt2, 3]*ZH[gt3, 3]*ZH[gt4, 3]}},
 C[S[1, {gt1}], S[1, {gt2}], S[3, {gt3}], -S[3, {gt4}]] == {{(-2*I)*lam8*ZH[gt1, 1]*ZH[gt2, 1]*ZP[gt3, 1]*ZP[gt4, 1] - I*lam5*ZH[gt1, 2]*ZH[gt2, 2]*ZP[gt3, 1]*ZP[gt4, 1] - I*lam5*ZH[gt1, 3]*ZH[gt2, 3]*ZP[gt3, 1]*ZP[gt4, 1] - (I/2)*lam6*ZH[gt1, 2]*ZH[gt2, 1]*ZP[gt3, 2]*ZP[gt4, 1] - I*lam7*ZH[gt1, 2]*ZH[gt2, 1]*ZP[gt3, 2]*ZP[gt4, 1] - (I/2)*lam6*ZH[gt1, 1]*ZH[gt2, 2]*ZP[gt3, 2]*ZP[gt4, 1] - I*lam7*ZH[gt1, 1]*ZH[gt2, 2]*ZP[gt3, 2]*ZP[gt4, 1] - I*lam4*ZH[gt1, 3]*ZH[gt2, 2]*ZP[gt3, 2]*ZP[gt4, 1] - I*lam4*ZH[gt1, 2]*ZH[gt2, 3]*ZP[gt3, 2]*ZP[gt4, 1] - (I/2)*lam6*ZH[gt1, 3]*ZH[gt2, 1]*ZP[gt3, 3]*ZP[gt4, 1] - I*lam7*ZH[gt1, 3]*ZH[gt2, 1]*ZP[gt3, 3]*ZP[gt4, 1] - I*lam4*ZH[gt1, 2]*ZH[gt2, 2]*ZP[gt3, 3]*ZP[gt4, 1] - (I/2)*lam6*ZH[gt1, 1]*ZH[gt2, 3]*ZP[gt3, 3]*ZP[gt4, 1] - I*lam7*ZH[gt1, 1]*ZH[gt2, 3]*ZP[gt3, 3]*ZP[gt4, 1] + I*lam4*ZH[gt1, 3]*ZH[gt2, 3]*ZP[gt3, 3]*ZP[gt4, 1] - (I/2)*lam6*ZH[gt1, 2]*ZH[gt2, 1]*ZP[gt3, 1]*ZP[gt4, 2] - I*lam7*ZH[gt1, 2]*ZH[gt2, 1]*ZP[gt3, 1]*ZP[gt4, 2] - (I/2)*lam6*ZH[gt1, 1]*ZH[gt2, 2]*ZP[gt3, 1]*ZP[gt4, 2] - I*lam7*ZH[gt1, 1]*ZH[gt2, 2]*ZP[gt3, 1]*ZP[gt4, 2] - I*lam4*ZH[gt1, 3]*ZH[gt2, 2]*ZP[gt3, 1]*ZP[gt4, 2] - I*lam4*ZH[gt1, 2]*ZH[gt2, 3]*ZP[gt3, 1]*ZP[gt4, 2] - I*lam5*ZH[gt1, 1]*ZH[gt2, 1]*ZP[gt3, 2]*ZP[gt4, 2] - I*lam4*ZH[gt1, 3]*ZH[gt2, 1]*ZP[gt3, 2]*ZP[gt4, 2] - (2*I)*lam1*ZH[gt1, 2]*ZH[gt2, 2]*ZP[gt3, 2]*ZP[gt4, 2] - (2*I)*lam3*ZH[gt1, 2]*ZH[gt2, 2]*ZP[gt3, 2]*ZP[gt4, 2] - I*lam4*ZH[gt1, 1]*ZH[gt2, 3]*ZP[gt3, 2]*ZP[gt4, 2] - (2*I)*lam1*ZH[gt1, 3]*ZH[gt2, 3]*ZP[gt3, 2]*ZP[gt4, 2] + (2*I)*lam3*ZH[gt1, 3]*ZH[gt2, 3]*ZP[gt3, 2]*ZP[gt4, 2] - I*lam4*ZH[gt1, 2]*ZH[gt2, 1]*ZP[gt3, 3]*ZP[gt4, 2] - I*lam4*ZH[gt1, 1]*ZH[gt2, 2]*ZP[gt3, 3]*ZP[gt4, 2] - (2*I)*lam3*ZH[gt1, 3]*ZH[gt2, 2]*ZP[gt3, 3]*ZP[gt4, 2] - (2*I)*lam3*ZH[gt1, 2]*ZH[gt2, 3]*ZP[gt3, 3]*ZP[gt4, 2] - (I/2)*lam6*ZH[gt1, 3]*ZH[gt2, 1]*ZP[gt3, 1]*ZP[gt4, 3] - I*lam7*ZH[gt1, 3]*ZH[gt2, 1]*ZP[gt3, 1]*ZP[gt4, 3] - I*lam4*ZH[gt1, 2]*ZH[gt2, 2]*ZP[gt3, 1]*ZP[gt4, 3] - (I/2)*lam6*ZH[gt1, 1]*ZH[gt2, 3]*ZP[gt3, 1]*ZP[gt4, 3] - I*lam7*ZH[gt1, 1]*ZH[gt2, 3]*ZP[gt3, 1]*ZP[gt4, 3] + I*lam4*ZH[gt1, 3]*ZH[gt2, 3]*ZP[gt3, 1]*ZP[gt4, 3] - I*lam4*ZH[gt1, 2]*ZH[gt2, 1]*ZP[gt3, 2]*ZP[gt4, 3] - I*lam4*ZH[gt1, 1]*ZH[gt2, 2]*ZP[gt3, 2]*ZP[gt4, 3] - (2*I)*lam3*ZH[gt1, 3]*ZH[gt2, 2]*ZP[gt3, 2]*ZP[gt4, 3] - (2*I)*lam3*ZH[gt1, 2]*ZH[gt2, 3]*ZP[gt3, 2]*ZP[gt4, 3] - I*lam5*ZH[gt1, 1]*ZH[gt2, 1]*ZP[gt3, 3]*ZP[gt4, 3] + I*lam4*ZH[gt1, 3]*ZH[gt2, 1]*ZP[gt3, 3]*ZP[gt4, 3] - (2*I)*lam1*ZH[gt1, 2]*ZH[gt2, 2]*ZP[gt3, 3]*ZP[gt4, 3] + (2*I)*lam3*ZH[gt1, 2]*ZH[gt2, 2]*ZP[gt3, 3]*ZP[gt4, 3] + I*lam4*ZH[gt1, 1]*ZH[gt2, 3]*ZP[gt3, 3]*ZP[gt4, 3] - (2*I)*lam1*ZH[gt1, 3]*ZH[gt2, 3]*ZP[gt3, 3]*ZP[gt4, 3] - (2*I)*lam3*ZH[gt1, 3]*ZH[gt2, 3]*ZP[gt3, 3]*ZP[gt4, 3]}},
 C[S[1, {gt1}], S[1, {gt2}], S[10], S[10]] == {{(-I)*lam14*ZH[gt1, 1]*ZH[gt2, 1] - I*lam10*ZH[gt1, 2]*ZH[gt2, 2] - I*lam11*ZH[gt1, 2]*ZH[gt2, 2] + (2*I)*lam12*ZH[gt1, 2]*ZH[gt2, 2] - I*lam10*ZH[gt1, 3]*ZH[gt2, 3] - I*lam11*ZH[gt1, 3]*ZH[gt2, 3] + (2*I)*lam12*ZH[gt1, 3]*ZH[gt2, 3]}},
 C[S[1, {gt1}], S[3, {gt2}], S[10], -S[6]] == {{(lam14*ZH[gt1, 1]*ZP[gt2, 1])/2 + (lam11*ZH[gt1, 2]*ZP[gt2, 2])/2 - lam12*ZH[gt1, 2]*ZP[gt2, 2] + (lam11*ZH[gt1, 3]*ZP[gt2, 3])/2 - lam12*ZH[gt1, 3]*ZP[gt2, 3]}},
 C[S[3, {gt1}], S[3, {gt2}], -S[6], -S[6]] == {{(-4*I)*lam12*ZP[gt1, 2]*ZP[gt2, 2] - (4*I)*lam12*ZP[gt1, 3]*ZP[gt2, 3]}},
 C[S[3, {gt1}], S[3, {gt2}], -S[3, {gt3}], -S[3, {gt4}]] == {{(-4*I)*lam8*ZP[gt1, 1]*ZP[gt2, 1]*ZP[gt3, 1]*ZP[gt4, 1] - (4*I)*lam7*ZP[gt1, 2]*ZP[gt2, 2]*ZP[gt3, 1]*ZP[gt4, 1] - (4*I)*lam7*ZP[gt1, 3]*ZP[gt2, 3]*ZP[gt3, 1]*ZP[gt4, 1] - I*lam5*ZP[gt1, 2]*ZP[gt2, 1]*ZP[gt3, 2]*ZP[gt4, 1] - I*lam6*ZP[gt1, 2]*ZP[gt2, 1]*ZP[gt3, 2]*ZP[gt4, 1] - I*lam5*ZP[gt1, 1]*ZP[gt2, 2]*ZP[gt3, 2]*ZP[gt4, 1] - I*lam6*ZP[gt1, 1]*ZP[gt2, 2]*ZP[gt3, 2]*ZP[gt4, 1] - (2*I)*lam4*ZP[gt1, 3]*ZP[gt2, 2]*ZP[gt3, 2]*ZP[gt4, 1] - (2*I)*lam4*ZP[gt1, 2]*ZP[gt2, 3]*ZP[gt3, 2]*ZP[gt4, 1] - I*lam5*ZP[gt1, 3]*ZP[gt2, 1]*ZP[gt3, 3]*ZP[gt4, 1] - I*lam6*ZP[gt1, 3]*ZP[gt2, 1]*ZP[gt3, 3]*ZP[gt4, 1] - (2*I)*lam4*ZP[gt1, 2]*ZP[gt2, 2]*ZP[gt3, 3]*ZP[gt4, 1] - I*lam5*ZP[gt1, 1]*ZP[gt2, 3]*ZP[gt3, 3]*ZP[gt4, 1] - I*lam6*ZP[gt1, 1]*ZP[gt2, 3]*ZP[gt3, 3]*ZP[gt4, 1] + (2*I)*lam4*ZP[gt1, 3]*ZP[gt2, 3]*ZP[gt3, 3]*ZP[gt4, 1] - I*lam5*ZP[gt1, 2]*ZP[gt2, 1]*ZP[gt3, 1]*ZP[gt4, 2] - I*lam6*ZP[gt1, 2]*ZP[gt2, 1]*ZP[gt3, 1]*ZP[gt4, 2] - I*lam5*ZP[gt1, 1]*ZP[gt2, 2]*ZP[gt3, 1]*ZP[gt4, 2] - I*lam6*ZP[gt1, 1]*ZP[gt2, 2]*ZP[gt3, 1]*ZP[gt4, 2] - (2*I)*lam4*ZP[gt1, 3]*ZP[gt2, 2]*ZP[gt3, 1]*ZP[gt4, 2] - (2*I)*lam4*ZP[gt1, 2]*ZP[gt2, 3]*ZP[gt3, 1]*ZP[gt4, 2] - (4*I)*lam7*ZP[gt1, 1]*ZP[gt2, 1]*ZP[gt3, 2]*ZP[gt4, 2] - (2*I)*lam4*ZP[gt1, 3]*ZP[gt2, 1]*ZP[gt3, 2]*ZP[gt4, 2] - (4*I)*lam1*ZP[gt1, 2]*ZP[gt2, 2]*ZP[gt3, 2]*ZP[gt4, 2] - (4*I)*lam3*ZP[gt1, 2]*ZP[gt2, 2]*ZP[gt3, 2]*ZP[gt4, 2] - (2*I)*lam4*ZP[gt1, 1]*ZP[gt2, 3]*ZP[gt3, 2]*ZP[gt4, 2] - (4*I)*lam2*ZP[gt1, 3]*ZP[gt2, 3]*ZP[gt3, 2]*ZP[gt4, 2] - (4*I)*lam3*ZP[gt1, 3]*ZP[gt2, 3]*ZP[gt3, 2]*ZP[gt4, 2] - (2*I)*lam4*ZP[gt1, 2]*ZP[gt2, 1]*ZP[gt3, 3]*ZP[gt4, 2] - (2*I)*lam4*ZP[gt1, 1]*ZP[gt2, 2]*ZP[gt3, 3]*ZP[gt4, 2] - (2*I)*lam1*ZP[gt1, 3]*ZP[gt2, 2]*ZP[gt3, 3]*ZP[gt4, 2] + (2*I)*lam2*ZP[gt1, 3]*ZP[gt2, 2]*ZP[gt3, 3]*ZP[gt4, 2] - (2*I)*lam1*ZP[gt1, 2]*ZP[gt2, 3]*ZP[gt3, 3]*ZP[gt4, 2] + (2*I)*lam2*ZP[gt1, 2]*ZP[gt2, 3]*ZP[gt3, 3]*ZP[gt4, 2] - I*lam5*ZP[gt1, 3]*ZP[gt2, 1]*ZP[gt3, 1]*ZP[gt4, 3] - I*lam6*ZP[gt1, 3]*ZP[gt2, 1]*ZP[gt3, 1]*ZP[gt4, 3] - (2*I)*lam4*ZP[gt1, 2]*ZP[gt2, 2]*ZP[gt3, 1]*ZP[gt4, 3] - I*lam5*ZP[gt1, 1]*ZP[gt2, 3]*ZP[gt3, 1]*ZP[gt4, 3] - I*lam6*ZP[gt1, 1]*ZP[gt2, 3]*ZP[gt3, 1]*ZP[gt4, 3] + (2*I)*lam4*ZP[gt1, 3]*ZP[gt2, 3]*ZP[gt3, 1]*ZP[gt4, 3] - (2*I)*lam4*ZP[gt1, 2]*ZP[gt2, 1]*ZP[gt3, 2]*ZP[gt4, 3] - (2*I)*lam4*ZP[gt1, 1]*ZP[gt2, 2]*ZP[gt3, 2]*ZP[gt4, 3] - (2*I)*lam1*ZP[gt1, 3]*ZP[gt2, 2]*ZP[gt3, 2]*ZP[gt4, 3] + (2*I)*lam2*ZP[gt1, 3]*ZP[gt2, 2]*ZP[gt3, 2]*ZP[gt4, 3] - (2*I)*lam1*ZP[gt1, 2]*ZP[gt2, 3]*ZP[gt3, 2]*ZP[gt4, 3] + (2*I)*lam2*ZP[gt1, 2]*ZP[gt2, 3]*ZP[gt3, 2]*ZP[gt4, 3] - (4*I)*lam7*ZP[gt1, 1]*ZP[gt2, 1]*ZP[gt3, 3]*ZP[gt4, 3] + (2*I)*lam4*ZP[gt1, 3]*ZP[gt2, 1]*ZP[gt3, 3]*ZP[gt4, 3] - (4*I)*lam2*ZP[gt1, 2]*ZP[gt2, 2]*ZP[gt3, 3]*ZP[gt4, 3] - (4*I)*lam3*ZP[gt1, 2]*ZP[gt2, 2]*ZP[gt3, 3]*ZP[gt4, 3] + (2*I)*lam4*ZP[gt1, 1]*ZP[gt2, 3]*ZP[gt3, 3]*ZP[gt4, 3] - (4*I)*lam1*ZP[gt1, 3]*ZP[gt2, 3]*ZP[gt3, 3]*ZP[gt4, 3] - (4*I)*lam3*ZP[gt1, 3]*ZP[gt2, 3]*ZP[gt3, 3]*ZP[gt4, 3]}},
 C[S[3, {gt1}], S[10], S[10], -S[3, {gt4}]] == {{(-I)*lam10*ZP[gt1, 2]*ZP[gt4, 2] - I*lam10*ZP[gt1, 3]*ZP[gt4, 3]}},
 C[S[10], S[10], S[10], S[10]] == {{(-6*I)*lam13}},
 C[V[5, {ct1}], V[5, {ct2}], V[5, {ct3}]] == {{g3*fSU3[ct1, ct2, ct3]}},
 C[-V[3], V[1], V[3]] == {{(-I)*g2*STW}},
 C[-V[3], V[3], V[2]] == {{I*CTW*g2}},
 C[S[2, {gt3}], -U[3], U[3]] == {{(g2^2*v1*GaugeXi[Wp]*ZA[gt3, 1])/4 + (Sqrt[3]*g2^2*v2*GaugeXi[Wp]*ZA[gt3, 2])/4 + (g2^2*v2*GaugeXi[Wp]*ZA[gt3, 3])/4}},
 C[S[2, {gt3}], -U[4], U[4]] == {{-(g2^2*v1*GaugeXi[Wp]*ZA[gt3, 1])/4 - (Sqrt[3]*g2^2*v2*GaugeXi[Wp]*ZA[gt3, 2])/4 - (g2^2*v2*GaugeXi[Wp]*ZA[gt3, 3])/4}},
 C[S[1, {gt3}], -U[2], U[1]] == {{(I/4)*CTW^2*g1*g2*v1*GaugeXi[Z]*ZH[gt3, 1] + (I/4)*CTW*g1^2*STW*v1*GaugeXi[Z]*ZH[gt3, 1] - (I/4)*CTW*g2^2*STW*v1*GaugeXi[Z]*ZH[gt3, 1] - (I/4)*g1*g2*STW^2*v1*GaugeXi[Z]*ZH[gt3, 1] + (I/4)*Sqrt[3]*CTW^2*g1*g2*v2*GaugeXi[Z]*ZH[gt3, 2] + (I/4)*Sqrt[3]*CTW*g1^2*STW*v2*GaugeXi[Z]*ZH[gt3, 2] - (I/4)*Sqrt[3]*CTW*g2^2*STW*v2*GaugeXi[Z]*ZH[gt3, 2] - (I/4)*Sqrt[3]*g1*g2*STW^2*v2*GaugeXi[Z]*ZH[gt3, 2] + (I/4)*CTW^2*g1*g2*v2*GaugeXi[Z]*ZH[gt3, 3] + (I/4)*CTW*g1^2*STW*v2*GaugeXi[Z]*ZH[gt3, 3] - (I/4)*CTW*g2^2*STW*v2*GaugeXi[Z]*ZH[gt3, 3] - (I/4)*g1*g2*STW^2*v2*GaugeXi[Z]*ZH[gt3, 3]}},
 C[S[3, {gt3}], -U[3], U[1]] == {{(-I/4)*CTW*g1*g2*v1*GaugeXi[Wp]*ZP[gt3, 1] - (I/4)*g2^2*STW*v1*GaugeXi[Wp]*ZP[gt3, 1] - (I/4)*Sqrt[3]*CTW*g1*g2*v2*GaugeXi[Wp]*ZP[gt3, 2] - (I/4)*Sqrt[3]*g2^2*STW*v2*GaugeXi[Wp]*ZP[gt3, 2] - (I/4)*CTW*g1*g2*v2*GaugeXi[Wp]*ZP[gt3, 3] - (I/4)*g2^2*STW*v2*GaugeXi[Wp]*ZP[gt3, 3]}},
 C[-S[3, {gt3}], -U[4], U[1]] == {{(-I/4)*CTW*g1*g2*v1*GaugeXi[Wp]*ZP[gt3, 1] - (I/4)*g2^2*STW*v1*GaugeXi[Wp]*ZP[gt3, 1] - (I/4)*Sqrt[3]*CTW*g1*g2*v2*GaugeXi[Wp]*ZP[gt3, 2] - (I/4)*Sqrt[3]*g2^2*STW*v2*GaugeXi[Wp]*ZP[gt3, 2] - (I/4)*CTW*g1*g2*v2*GaugeXi[Wp]*ZP[gt3, 3] - (I/4)*g2^2*STW*v2*GaugeXi[Wp]*ZP[gt3, 3]}},
 C[S[1, {gt3}], -U[3], U[3]] == {{(-I/4)*g2^2*v1*GaugeXi[Wp]*ZH[gt3, 1] - (I/4)*Sqrt[3]*g2^2*v2*GaugeXi[Wp]*ZH[gt3, 2] - (I/4)*g2^2*v2*GaugeXi[Wp]*ZH[gt3, 3]}},
 C[-S[3, {gt3}], -U[2], U[3]] == {{(I/4)*CTW*g2^2*v1*GaugeXi[Z]*ZP[gt3, 1] + (I/4)*g1*g2*STW*v1*GaugeXi[Z]*ZP[gt3, 1] + (I/4)*Sqrt[3]*CTW*g2^2*v2*GaugeXi[Z]*ZP[gt3, 2] + (I/4)*Sqrt[3]*g1*g2*STW*v2*GaugeXi[Z]*ZP[gt3, 2] + (I/4)*CTW*g2^2*v2*GaugeXi[Z]*ZP[gt3, 3] + (I/4)*g1*g2*STW*v2*GaugeXi[Z]*ZP[gt3, 3]}},
 C[S[1, {gt3}], -U[4], U[4]] == {{(-I/4)*g2^2*v1*GaugeXi[Wp]*ZH[gt3, 1] - (I/4)*Sqrt[3]*g2^2*v2*GaugeXi[Wp]*ZH[gt3, 2] - (I/4)*g2^2*v2*GaugeXi[Wp]*ZH[gt3, 3]}},
 C[S[3, {gt3}], -U[2], U[4]] == {{(I/4)*CTW*g2^2*v1*GaugeXi[Z]*ZP[gt3, 1] + (I/4)*g1*g2*STW*v1*GaugeXi[Z]*ZP[gt3, 1] + (I/4)*Sqrt[3]*CTW*g2^2*v2*GaugeXi[Z]*ZP[gt3, 2] + (I/4)*Sqrt[3]*g1*g2*STW*v2*GaugeXi[Z]*ZP[gt3, 2] + (I/4)*CTW*g2^2*v2*GaugeXi[Z]*ZP[gt3, 3] + (I/4)*g1*g2*STW*v2*GaugeXi[Z]*ZP[gt3, 3]}},
 C[S[1, {gt3}], -U[2], U[2]] == {{(-I/8)*g1^2*v1*GaugeXi[Z]*ZH[gt3, 1] + (I/8)*CTW^2*g1^2*v1*GaugeXi[Z]*ZH[gt3, 1] - (I/8)*g2^2*v1*GaugeXi[Z]*ZH[gt3, 1] - (I/8)*CTW^2*g2^2*v1*GaugeXi[Z]*ZH[gt3, 1] - (I/2)*CTW*g1*g2*STW*v1*GaugeXi[Z]*ZH[gt3, 1] - (I/8)*g1^2*STW^2*v1*GaugeXi[Z]*ZH[gt3, 1] + (I/8)*g2^2*STW^2*v1*GaugeXi[Z]*ZH[gt3, 1] - (I/8)*Sqrt[3]*g1^2*v2*GaugeXi[Z]*ZH[gt3, 2] + (I/8)*Sqrt[3]*CTW^2*g1^2*v2*GaugeXi[Z]*ZH[gt3, 2] - (I/8)*Sqrt[3]*g2^2*v2*GaugeXi[Z]*ZH[gt3, 2] - (I/8)*Sqrt[3]*CTW^2*g2^2*v2*GaugeXi[Z]*ZH[gt3, 2] - (I/2)*Sqrt[3]*CTW*g1*g2*STW*v2*GaugeXi[Z]*ZH[gt3, 2] - (I/8)*Sqrt[3]*g1^2*STW^2*v2*GaugeXi[Z]*ZH[gt3, 2] + (I/8)*Sqrt[3]*g2^2*STW^2*v2*GaugeXi[Z]*ZH[gt3, 2] - (I/8)*g1^2*v2*GaugeXi[Z]*ZH[gt3, 3] + (I/8)*CTW^2*g1^2*v2*GaugeXi[Z]*ZH[gt3, 3] - (I/8)*g2^2*v2*GaugeXi[Z]*ZH[gt3, 3] - (I/8)*CTW^2*g2^2*v2*GaugeXi[Z]*ZH[gt3, 3] - (I/2)*CTW*g1*g2*STW*v2*GaugeXi[Z]*ZH[gt3, 3] - (I/8)*g1^2*STW^2*v2*GaugeXi[Z]*ZH[gt3, 3] + (I/8)*g2^2*STW^2*v2*GaugeXi[Z]*ZH[gt3, 3]}},
 C[S[3, {gt3}], -U[3], U[2]] == {{(-I/4)*CTW*g2^2*v1*GaugeXi[Wp]*ZP[gt3, 1] + (I/4)*g1*g2*STW*v1*GaugeXi[Wp]*ZP[gt3, 1] - (I/4)*Sqrt[3]*CTW*g2^2*v2*GaugeXi[Wp]*ZP[gt3, 2] + (I/4)*Sqrt[3]*g1*g2*STW*v2*GaugeXi[Wp]*ZP[gt3, 2] - (I/4)*CTW*g2^2*v2*GaugeXi[Wp]*ZP[gt3, 3] + (I/4)*g1*g2*STW*v2*GaugeXi[Wp]*ZP[gt3, 3]}},
 C[-S[3, {gt3}], -U[4], U[2]] == {{(-I/4)*CTW*g2^2*v1*GaugeXi[Wp]*ZP[gt3, 1] + (I/4)*g1*g2*STW*v1*GaugeXi[Wp]*ZP[gt3, 1] - (I/4)*Sqrt[3]*CTW*g2^2*v2*GaugeXi[Wp]*ZP[gt3, 2] + (I/4)*Sqrt[3]*g1*g2*STW*v2*GaugeXi[Wp]*ZP[gt3, 2] - (I/4)*CTW*g2^2*v2*GaugeXi[Wp]*ZP[gt3, 3] + (I/4)*g1*g2*STW*v2*GaugeXi[Wp]*ZP[gt3, 3]}},
 C[-U[5, {ct1}], U[5, {ct2}], V[5, {ct3}]] == {{g3*fSU3[ct1, ct2, ct3]}, {0}},
 C[-U[3], U[1], V[3]] == {{(-I)*g2*STW}, {0}},
 C[-U[4], U[1], -V[3]] == {{I*g2*STW}, {0}},
 C[-U[3], U[3], V[1]] == {{I*g2*STW}, {0}},
 C[-U[3], U[3], V[2]] == {{I*CTW*g2}, {0}},
 C[-U[1], U[3], -V[3]] == {{(-I)*g2*STW}, {0}},
 C[-U[2], U[3], -V[3]] == {{(-I)*CTW*g2}, {0}},
 C[-U[4], U[4], V[1]] == {{(-I)*g2*STW}, {0}},
 C[-U[1], U[4], V[3]] == {{I*g2*STW}, {0}},
 C[-U[2], U[4], V[3]] == {{I*CTW*g2}, {0}},
 C[-U[4], U[4], V[2]] == {{(-I)*CTW*g2}, {0}},
 C[-U[3], U[2], V[3]] == {{(-I)*CTW*g2}, {0}},
 C[-U[4], U[2], -V[3]] == {{I*CTW*g2}, {0}}
 }

 
Conjugate[g1] ^= g1; 
Conjugate[g2] ^= g2; 
Conjugate[g3] ^= g3; 
Conjugate[mu1] ^= mu1; 
Conjugate[mu2] ^= mu2; 
Conjugate[mu0] ^= mu0; 
Conjugate[lam1] ^= lam1; 
Conjugate[lam3] ^= lam3; 
Conjugate[lam4] ^= lam4; 
Conjugate[lam2] ^= lam2; 
Conjugate[lam12] ^= lam12; 
Conjugate[lam7] ^= lam7; 
Conjugate[lam11] ^= lam11; 
Conjugate[lam10] ^= lam10; 
Conjugate[lam13] ^= lam13; 
Conjugate[lam6] ^= lam6; 
Conjugate[lam14] ^= lam14; 
Conjugate[lam5] ^= lam5; 
Conjugate[lam8] ^= lam8; 
Conjugate[v1] ^= v1; 
Conjugate[v2] ^= v2; 
Conjugate[ZZ[a___]] ^= ZZ[a]; 
Conjugate[ZH[a___]] ^= ZH[a]; 
Conjugate[ZA[a___]] ^= ZA[a]; 
Conjugate[ZP[a___]] ^= ZP[a]; 
Conjugate[aEWinv] ^= aEWinv; 
Conjugate[v] ^= v; 
Conjugate[Gf] ^= Gf; 
