
# Define fortran compiler and options for diver
FF=mpif90
# GNU
FOPT=-ffixed-line-length-none -DMPI -cpp #-Wall -fcheck=all
MODULE=J

# Define C/C++ compilers and options
CC=mpicc
COPT=-DMPI
CPPOPT=
SO_LINK_FLAGS=
# GNU
MIXOPT_C=
MIXOPT_CPP=

# Define some more internal variables
CLEAN = _clean
AR = ar r
SHAR = ld -shared
LIBNAME = diver
PREFIX = diver
DIVER_ROOT = ${HOME}/diver
SOURCE = ${DIVER_ROOT}/src
INC = ${DIVER_ROOT}/include
BUILD = ${DIVER_ROOT}/build
LIB = ${DIVER_ROOT}/lib

# Set internal compile commands
DIVER_FF=$(FF)
DIVER_FOPT=$(FOPT) -O3 -fPIC -I$(INC) -$(MODULE) $(BUILD)
DIVER_CC=$(CC)
DIVER_COPT=$(COPT) -O3 -fPIC -I$(INC)
DIVER_CPPOPT=$(CPPOPT) -lstdc++
DIVER_MIXOPT_C=$(MIXOPT_C)
DIVER_MIXOPT_CPP=$(MIXOPT_CPP) -lstdc++
DIVER_SO_LINK_FLAGS=$(SO_LINK_FLAGS) -shared

prefix=${HOME}/heavyS3
INC2 = ${prefix}/src/include 
CFLAGS=
#CFLAGS= -ggdb

LIBS = -L$(LIB) -l$(LIBNAME) -lmpi -lm

VPATH = ${prefix}:${prefix}/src:${prefix}/src/nr:${prefix}/src/include:${prefix}/src/pcg


NAME = clstrdm
SOURCEFILES = clstrdm nrutil xsect unitary constraints pcg_basic jacobn balanc elmhes hqr lubksb ludcmp odeSDA rkckSDA \
                rkqsSDA stiffSDA jacobi eigsrt
OBJ = $(SOURCEFILES:%=%.o)
HEADERFILES = $(INC)/diver.h


all: clstrdm

%.o: %.c
	$(DIVER_CC) -c $(DIVER_COPT) -std=c99 -I$(INC2) -I$(INC)  $<
 
clstrdm: $(OBJ) $(HEADERS)
	$(DIVER_FF) $(DIVER_MIXOPT_C) $(OBJ) $(LIBS) -o $(NAME)
#	rm -f *.o 


.PHONY : clean
clean:
	rm -f *.o  *.mod $(NAME) 


