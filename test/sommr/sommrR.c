#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif
/* Driver for routine odeint */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define NRANSI
#include "nrutil.h"
#include "sda.h"

#define Neqs 18

double dxsav,*xp,**yp;  /* defining declarations */
const double units = 1E-3;   // units = 1.0  means  GeV;
int kmax,kount,kountRK,kountStiff;

int nrhs;   /* counts function evaluations */


int main(int argc, char** argv)
{
	int i,jj,nbad,nok;
	double eps=1.0e-4,h1=0.1,hmin=0.0,*ystart;
	double x1=0.2/units, x2=0.0000000000001/units;  //  <<--------
    //double x1=0.2/units, x2=0.00000000001/units;
    double coini;

	ystart=dvector(1,Neqs);
	xp=dvector(1,2000);
	yp=dmatrix(1,Neqs+5,1,2000);

	
	nrhs=0;
	kmax=500;
	dxsav=(x2-x1)/100.0;


	/*  ---------------------------------------------------------  */
	/*  ---------------------------------------------------------  */
	/*  ---------------------------------------------------------  */

    double tanth, theta, mu0, mu1, vs, v1, v2;
    double vev=246.0*units;
    double v=246.0*units;
    double lam1, lam2, lam3, lam4, lam5, lam6, lam7, lam8, lam9, lam10, lam11, lam12, lam13, lam14, mu2;
    double Man, Map, Maplus;  /*  Masses: a = Inert: n=neutral, p=neutral pseudoscalar, plus = charged   */
    double Mh, MH, MH3;       /*  Masses scalars: h = SM Higgs (in the decoupling limit): H=heavy Higgs, H3=xtra scalar   */
    double MA, MHplus;        /*  Masses pseudo scalar: A = THDM pseudoscalar : Hplus=THDM charged scalar   */
    double M2p, M2plus;       /*  Masses pseudo scalar: h2p = non-THDM pseudoscalar : H2plus= non-THDM charged scalar   */
    double alfa;              /*  mixing neutral scalar angle (THDM-analogue)  */
    double gapA, gapH;

    double Man2, Map2, Maplus2;  /*  Masses: a = Inert: n=neutral, p=neutral pseudoscalar, plus = charged   */
    double Mh2, MH2, MH32;       /*  Masses scalars: h = SM Higgs (in the decoupling limit): H=heavy Higgs, H3=xtra scalar   */
    double MA2, MHplus2;        /*  Masses pseudo scalar: A = THDM pseudoscalar : Hplus=THDM charged scalar   */
    double M2p2, M2plus2;       /*  Masses pseudo scalar: h2p = non-THDM pseudoscalar : H2plus= non-THDM charged scalar   */

    double halfsqrt3 = 0.5 * sqrt(3.0);
	double mZ  = (9.11887E+01)*units;      // 4     9.11887000E+01  # m_Z(pole)
	double mZ2 = mZ*mZ;
	double mW  = (80.379)*units;
	double mW2 = mW*mW;


	/*  ------------------------------------------------------------------------ --  */
	/*  ---------------------------------------------------------------------------  */
	/*  --    we take mu2 close to Man^2(very close to Map^2 and Maplus^2)      ---  */
	/*  --   (Man^2 - mu2 = 10,000 GeV^2) then we take lam13 small = 0.001      ---  */
	/*  --  to satisfy inequalities that ensure no tunneling to v_a \neq 0      ---  */
	/*  --  also Map and Maplus will be fixed by Map-Man=GapA and gapA free par ---  */
	/*  ---------------------------------------------------------------------------  */
	/*  ---------------------------------------------------------------------------  */

	/*
	 *  Best Fit Point coordinates
	*/

    Man    = 3.1397179600000000;  /*  in TeV, TODO not compatible if "units" is modified  */
    Man2   = Man*Man;

  /*  
   *  This 3 we keep fixed (or in terms of Man for the case of mu2)
   *  for a total of 12 free parameters
   * 
  */
    mu2    = Man2 - 10000.0*units*units;
    Mh     = 125.0*units;
    lam13  = 0.001;

    gapA   = pow(10.0, -6.7139079300000004); // (Map - Man)/Man     = gapA;
    gapH   = pow(10.0, -5.0584337599999998); // (Maplus - Man)/ Man = gapH;
    Map    = Man + gapA * Man;
    Maplus = Man + gapH * Man;

    MH     = 0.38355354500000000;  
    MH3    = 0.36805380199999999;  
    MA     = 0.50890192499999998;  
    MHplus = 0.27889866600000002;  
    M2p    = 0.34407693700000003;  
    M2plus = 0.33287005800000002;  
    tanth  = pow(10.0, 0.37351649100000001); 
    alfa   = -0.39435029700000002; 
    lam14  = 1.8675120599999999; 

  /*  
   *  The squares of the parameters, etc
   * 
  */
    Map2    = Map*Map;
    Maplus2 = Maplus*Maplus;
    Mh2     = Mh*Mh;
    MH2     = MH*MH;
    MH32    = MH3*MH3;
    MA2     = MA*MA;
    MHplus2 = MHplus*MHplus;
    M2p2    = M2p*M2p;
    M2plus2 = M2plus*M2plus;
    theta   = atan(tanth);






    double **ZH;
    ZH = (double **) malloc(3 * sizeof(double*)) ;   
    for(i = 0; i<3; i++) {
       ZH[i] = (double *) malloc(3 * sizeof(double));
    }

    double **ZA;
    ZA = (double **) malloc(3 * sizeof(double*)) ;   
    for(i = 0; i<3; i++) {
       ZA[i] = (double *) malloc(3 * sizeof(double));
    }

    double vrel=0.002;
    
    ZH[0][0]=cos(alfa);
    ZH[0][1]=halfsqrt3*sin(alfa);
    ZH[0][2]=0.5*sin(alfa);
    ZH[1][0]=0.0;
    ZH[1][1]=0.5;
    ZH[1][2]=-halfsqrt3;
    ZH[2][0]=-sin(alfa);
    ZH[2][1]=halfsqrt3*cos(alfa);
    ZH[2][2]=0.5*cos(alfa);

    ZA[0][0]=cos(theta)*sin(theta);
    ZA[0][1]=halfsqrt3*sin(theta)*sin(theta);
    ZA[0][2]=0.5*sin(theta)*sin(theta);
    ZA[1][0]=0.0;
    ZA[1][1]=0.5;
    ZA[1][2]=-halfsqrt3;
    ZA[2][0]=-cos(theta)*sin(theta);
    ZA[2][1]=halfsqrt3*cos(theta)*cos(theta);
    ZA[2][2]=0.5*cos(theta)*cos(theta);


    lam1 = (18*MHplus2 + (9*Mh2 + 9*MH2 + 18*M2plus2 - 2*MH32 - 18*MHplus2 + 9*(Mh2 - MH2)*cos(2*alfa))*pow(1/sin(theta),2))/(36.*pow(v,2));
    lam2 = (-MA2 + MHplus2 + (-M2p2 + M2plus2 + MA2 - MHplus2)*pow(1/sin(theta),2))/(2.*pow(v,2));
    lam3 = ((-18*M2plus2 + 8*MH32 + 9*MHplus2 + 9*MHplus2*cos(2*theta))*pow(1/sin(theta),2))/(36.*pow(v,2));
    lam4 = (-2*MH32*1/cos(theta)*1/sin(theta))/(9.*pow(v,2));
    lam5 = (36*MHplus2 + 2*MH32*pow(1/cos(theta),2) + 9*(-Mh2 + MH2)*1/cos(theta)*1/sin(theta)*sin(2*alfa))/(18.*pow(v,2));
    lam6 = (9*MA2 - 18*MHplus2 + MH32*pow(1/cos(theta),2))/(9.*pow(v,2));
    lam7 = (-9*MA2 + MH32*pow(1/cos(theta),2))/(18.*pow(v,2));
    lam8 = (pow(1/cos(theta),2)*(9*(Mh2 + MH2) + 9*(-Mh2 + MH2)*cos(2*alfa) - 2*MH32*pow(tan(theta),2)))/(36.*pow(v,2));
    lam9 = 0.0;

    vs = v*cos(theta);
    v1 = vs;
    v2 = (v*sin(theta))/2.;
    mu0 = (-4*lam4*pow(v2,3) - 2*lam5*pow(v2,2)*vs - 2*lam6*pow(v2,2)*vs - 4*lam7*pow(v2,2)*vs - lam8*pow(vs,3))/vs;
    mu1 = (-8*lam1*pow(v2,2) - 8*lam3*pow(v2,2) - 6*lam4*v2*vs - lam5*pow(vs,2) - lam6*pow(vs,2) - 2*lam7*pow(vs,2))/2.;

    lam10 = (2*(Maplus2 - mu2)*pow(1/sin(theta),2))/pow(v,2);
    lam11 = -((-(Man2*pow(1/sin(theta),2)) - Map2*pow(1/sin(theta),2) + 2*Maplus2*pow(1/sin(theta),2) + lam14*pow(v,2)*pow(1/tan(theta),2))/pow(v,2));
    lam12 = ((Man2 - Map2)*pow(1/sin(theta),2))/(2.*pow(v,2));

    double lambdas[15];

    lambdas[1]=lam1;
    lambdas[2]=lam2;
    lambdas[3]=lam3;
    lambdas[4]=lam4;
    lambdas[5]=lam5;
    lambdas[6]=lam6;
    lambdas[7]=lam7;
    lambdas[8]=lam8;
    lambdas[9]=lam9;
    lambdas[10]=lam10;
    lambdas[11]=lam11;
    lambdas[12]=lam12;
    lambdas[13]=lam13;
    lambdas[14]=lam14;

    double Mdm[4];

    Mdm[0]=0.0;    /* flag for handling errors instead of nrerror  */
    Mdm[1]=Man;
    Mdm[2]=Map;
    Mdm[3]=Maplus;

    double Mlight[10];

    Mlight[1]=MH;
    Mlight[2]=MH3;
    Mlight[3]=Mh;
    Mlight[4]=mZ;
    Mlight[5]=M2p;
    Mlight[6]=MA;
    Mlight[7]=mW;
    Mlight[8]=M2plus;
    Mlight[9]=MHplus;

    ystart[1]=0.0;
    ystart[2]=0.0;
    ystart[3]=0.0;
    ystart[4]=0.0;
    ystart[5]=0.0;
    ystart[6]=0.0;
    ystart[7]=0.0;
    ystart[8]=0.0;
    ystart[9]=0.0;

    ystart[10]=Man*vrel*0.5;
    ystart[11]=0.0;
    ystart[12]=0.0;
    ystart[13]=0.0;
    ystart[14]=0.0;
    ystart[15]=0.0;
    ystart[16]=0.0;
    ystart[17]=0.0;
    ystart[18]=0.0;

    coini = 1.0-(4.0/(Man*vrel*vrel)*VNRij(5, x1, lambdas, ZH, ZA, Mdm, Mlight, theta));
    if(coini<0.0){
        ystart[5] =-sqrt(fabs(coini))*Man*vrel*0.5;
    }else{
        ystart[14]= sqrt(fabs(coini))*Man*vrel*0.5;
    }

    coini = 1.0-(4.0/(Man*vrel*vrel)*VNRij(9, x1, lambdas, ZH, ZA, Mdm, Mlight, theta));
    if(coini<0.0){
        ystart[9] =-sqrt(fabs(coini))*Man*vrel*0.5;
    }else{
        ystart[18]= sqrt(fabs(coini))*Man*vrel*0.5;
    }

	/*  ---------------------------------------------------------  */
	/*  ---------------------------------------------------------  */
	/*  ---------------------------------------------------------  */

    /*
    printf("\n pots \n");
    for(i=1;i<=Neqs;i++) printf("Vij(%d) = %f\n",i,
          VNRij(i, x1, lambdas, ZH, ZA, Mdm, Mlight, theta));
    
    printf("\n diffpots \n");
    for(i=1;i<=Neqs;i++) printf("diffVij(%d) = %f\n",i,
          diffVNRij(i, x1, lambdas, ZH, ZA, Mdm, Mlight, theta));
    
    printf("\n coini \n");
    for(i=1;i<=Neqs;i++) printf("ystart(%d) = %f\n",i,
          ystart[i]);
    
    printf("\n odeSDA \n");
    */

	odeSDA(ystart,Neqs,x1,x2,eps,h1,hmin,&nok,&nbad,derivs,rkqsSDA,stiffSDA,
            lambdas, ZH, ZA, Mdm, Mlight, theta);
	printf("\n%s %13s %3d\n","successful steps:"," ",nok);
	printf("%s %20s %3d\n","bad steps:"," ",nbad);
	printf("%s %9s %3d\n","function evaluations:"," ",nrhs);
	printf("\n%s %3d\n","stored intermediate values:    ",kount);
	printf("\n%s %3d\n","RK evaluations:       ",kountRK);
	printf("\n%s %3d\n","Stiff evaluations:    ",kountStiff);
	printf("\n%8s %18s %15s\n","x","integral","bessj(3,x)");
	//for (i=1;i<=kount;i++)
    i=kount;
		//printf("%10.4f %16.6f %16.6f %16.6f %16.6f \n",
        printf("%10.4f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f \n",
			xp[i],yp[1][i],yp[10][i],
                  yp[2][i],yp[11][i],
                  yp[3][i],yp[12][i],
                  yp[4][i],yp[13][i],
                  yp[5][i],yp[14][i],
                  yp[6][i],yp[15][i],
                  yp[7][i],yp[16][i],
                  yp[8][i],yp[17][i],
                  yp[9][i],yp[18][i]);




    double **hres=dmatrix(1,3,1,3);
    //wr=dvector(1,3);
    double *d1jSomm=dvector(1,3);
    double **vectmat=dmatrix(1,3,1,3);
    double *eigen=dvector(1,3);
    int nrot, kk;
    FILE * fp;

    fp = fopen ("sommr.txt","w");
    if (fp == NULL) {
                          nrerror("lsp file");
                          exit(1);
                        }
    /* ---------------------------------------------------------------------  */
    /* ---------------------------------------------------------------------  */
    /*  ---  make computations in the range of masses for a given point  ---  */
    /* ---------------------------------------------------------------------  */
    /* ---------------------------------------------------------------------  */

    /* ---------------------------------------------------------------------  */
    /* ---------------------------------------------------------------------  */
    /*  ---  gaps must be greater than vrel^2 * Man = Man * 10^-6        ---  */
    /* --------------------    i.e. gaps > 1 MeV   will do   ---------------  */
    /* ---------------------------------------------------------------------  */

    for(jj=0;jj<=200000;jj++)
    //for(jj=0;jj<=1;jj++)
    {

    nrhs=0;
    Man    = 1000.0*units + jj*0.1*units;
    Map    = Man + gapA * Man;
    Maplus = Man + gapH * Man;
    Man2   = Man*Man;
    mu2    = Man2 - 10000.0*units*units;
    Map2    = Map*Map;
    Maplus2 = Maplus*Maplus;

    lam10 = (2*(Maplus2 - mu2)*pow(1/sin(theta),2))/pow(v,2);
    lam11 = -((-(Man2*pow(1/sin(theta),2)) - Map2*pow(1/sin(theta),2) + 2*Maplus2*pow(1/sin(theta),2) + lam14*pow(v,2)*pow(1/tan(theta),2))/pow(v,2));
    lam12 = ((Man2 - Map2)*pow(1/sin(theta),2))/(2.*pow(v,2));

    lambdas[10]=lam10;
    lambdas[11]=lam11;
    lambdas[12]=lam12;

    Mdm[0]=0.0;    /* flag for handling errors instead of nrerror  */
    Mdm[1]=Man;
    Mdm[2]=Map;
    Mdm[3]=Maplus;


    ystart[1]=0.0;
    ystart[2]=0.0;
    ystart[3]=0.0;
    ystart[4]=0.0;
    ystart[5]=0.0;
    ystart[6]=0.0;
    ystart[7]=0.0;
    ystart[8]=0.0;
    ystart[9]=0.0;

    ystart[10]=Man*vrel*0.5;
    ystart[11]=0.0;
    ystart[12]=0.0;
    ystart[13]=0.0;
    ystart[14]=0.0;
    ystart[15]=0.0;
    ystart[16]=0.0;
    ystart[17]=0.0;
    ystart[18]=0.0;

    coini = 1.0-(4.0/(Man*vrel*vrel)*VNRij(5, x1, lambdas, ZH, ZA, Mdm, Mlight, theta));
    if(coini<0.0){
        ystart[5] =-sqrt(fabs(coini))*Man*vrel*0.5;
    }else{
        ystart[14]= sqrt(fabs(coini))*Man*vrel*0.5;
    }

    coini = 1.0-(4.0/(Man*vrel*vrel)*VNRij(9, x1, lambdas, ZH, ZA, Mdm, Mlight, theta));
    if(coini<0.0){
        ystart[9] =-sqrt(fabs(coini))*Man*vrel*0.5;
    }else{
        ystart[18]= sqrt(fabs(coini))*Man*vrel*0.5;
    }

    odeSDA(ystart,Neqs,x1,x2,eps,h1,hmin,&nok,&nbad,derivs,rkqsSDA,stiffSDA,
            lambdas, ZH, ZA, Mdm, Mlight, theta);
    if(Mdm[0]>0) continue;
    /*
    printf("\n%s %13s %3d\n","successful steps:"," ",nok);
    printf("%s %20s %3d\n","bad steps:"," ",nbad);
    printf("%s %9s %3d\n","function evaluations:"," ",nrhs);
    printf("%s %3d\n","stored intermediate values:    ",kount);
    printf("%s %3d\n","RK evaluations:       ",kountRK);
    printf("%s %3d\n","Stiff evaluations:    ",kountStiff);
    printf("%8s %18s %15s\n","x","integral","bessj(3,x)");
    for (i=1;i<=kount;i++) 
    */
    i=kount;
        /* printf("%10.4f %16.6f %16.6f %16.6f %16.6f \n", */
        /*
        printf("%10.4f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f \n",
            xp[i],yp[1][i],yp[10][i],
                  yp[2][i],yp[11][i],
                  yp[3][i],yp[12][i],
                  yp[4][i],yp[13][i],
                  yp[5][i],yp[14][i],
                  yp[6][i],yp[15][i],
                  yp[7][i],yp[16][i],
                  yp[8][i],yp[17][i],
                  yp[9][i],yp[18][i]);
        */

    /* construct matrix h - h^dagger at x=0  */

    hres[1][1]=2*yp[10][i] / (Mdm[1]*vrel);
    hres[1][2]=2*yp[11][i] / (Mdm[1]*vrel);
    hres[1][3]=2*yp[12][i] / (Mdm[1]*vrel);
    hres[2][1]=hres[1][2];
    hres[2][2]=2*yp[14][i] / (Mdm[1]*vrel);
    hres[2][3]=2*yp[15][i] / (Mdm[1]*vrel);
    hres[3][1]=hres[1][3];
    hres[3][2]=hres[2][3];
    hres[3][3]=2*yp[18][i] / (Mdm[1]*vrel);

    /*
    printf("\n");
    for(kk=1;kk<=3;kk++) printf("h(%d,...) = %f %f %f\n", kk, 
                                   hres[kk][1], hres[kk][2], hres[kk][3]);
    */
    /*  get the eigenvector with non-zero eigenvalue  */

    jacobi(hres,3,eigen,vectmat,&nrot);
    if(nrot==1) continue;
    eigsrt(eigen,vectmat,3);

    /*
    printf("\n");
    for(kk=1;kk<=3;kk++) printf("eigenvalue[%d]= %f\n", kk, eigen[kk]);
    printf("\n");
    for(kk=1;kk<=3;kk++) printf("d_%d = %f %f %f\n", kk, 
                                   vectmat[kk][1], vectmat[kk][2], vectmat[kk][3]);

    printf("\n");
    printf("MassDM = %f\n", Mdm[1]);
    */

    for(kk=1;kk<=3;kk++){
        d1jSomm[kk]=sqrt(eigen[1])*vectmat[kk][1];
    }

    fprintf(fp,"%d %f %f %f %f\n", jj, Mdm[1], d1jSomm[1], d1jSomm[2], d1jSomm[3]);

    printf("jj = %d Mdm = %f d11= %f d12 = %f d13 = %f\n", 
                 jj, Mdm[1], d1jSomm[1], d1jSomm[2], d1jSomm[3]);

    }  //done for jj
    /*   done   */

    fclose(fp);

    free_dvector(eigen,1,3);
    free_dmatrix(vectmat,1,3,1,3);
    free_dvector(d1jSomm,1,3);
    //free_dvector(wr,1,3);
    free_dmatrix(hres,1,3,1,3);
	free_dmatrix(yp,1,Neqs+5,1,2000);
	free_dvector(xp,1,2000);
	free_dvector(ystart,1,Neqs);

    for(i = 0; i<3; i++) free(ZH[i]);
    free(ZH);
    for(i = 0; i<3; i++) free(ZA[i]);
    free(ZA);

	return 0;
}
#undef NRANSI
