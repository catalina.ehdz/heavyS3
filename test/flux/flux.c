

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define NRANSI
#include "nrutil.h"
#include "sda.h"
#include "xsect.h"

#define Neqs 18

const double units = 1E-3;   /* units = 1.0  means  GeV */

  /* global variables to store intermediate results of odeSDA, not used here 
   * but I don't want to modify odeSDA
  */
double dxsav,*xp,**yp;
int kmax,kount,kountRK,kountStiff;
int nrhs;   /* counts function evaluations */


int main(int argc, char** argv)
{


  if (argc != 2 ) {
    fprintf(stderr, "\nUsage: flux [output-file] \n\n");
    return 0;
  }


  double myDMmass;


	int i,jj,nbad,nok;
	double eps=1.0e-4,h1=0.1,hmin=0.0,*ystart;

    /* ---------------------------------------- */
    /* Start calculating the Sommerfeld factors */
    /* ---------------------------------------- */

    /*  We need to find the solution to the Schrodinger like eq
     *  for the Sommerfeld factors with initial conditions at x =  + \infty
     *  all the way down to 0, for practical purposes 200TeV \approx \infty
    */
	double x1=0.2/units, x2=0.0000000000001/units;  
    double coini;

    /* contains the initial value of the functions,
     * at the end will contain their value at x = 0
    */
    ystart=dvector(1,Neqs);

    /* never mind these */
    nrhs=0;
    kmax=0;  /* 0 means no saving of intermediate results in odeSDA */
    dxsav=(x2-x1)/100.0;
	xp=dvector(1,2);
	yp=dmatrix(1,Neqs,1,2);

	


	/*  ---------------------------------------------------------  */
	/*  ---------------------------------------------------------  */
	/*  ---------------------------------------------------------  */

    /*  
     *  Parameters of the model
     * 
    */


    double tanth, theta, mu0, mu1, vs, v1, v2;
    double vev=246.0*units;
    double v=246.0*units;
    double lam1, lam2, lam3, lam4, lam5, lam6, lam7, lam8, lam9, lam10, lam11, 
           lam12, lam13, lam14, mu2;
    double Man, Map, Maplus;  /*  Masses: a = Inert: n=neutral, p=neutral pseudoscalar, plus = charged   */
    double Mh, MH, MH3;       /*  Masses scalars: h = SM Higgs (in the decoupling limit): H=heavy Higgs, H3=xtra scalar   */
    double MA, MHplus;        /*  Masses pseudo scalar: A = THDM pseudoscalar : Hplus=THDM charged scalar   */
    double M2p, M2plus;       /*  Masses pseudo scalar: h2p = non-THDM pseudoscalar : H2plus= non-THDM charged scalar   */
    double alfa;              /*  mixing neutral scalar angle (THDM-analogue)  */
    double gapA, gapH;

    double Man2, Map2, Maplus2;  /*  Masses: a = Inert: n=neutral, p=neutral pseudoscalar, plus = charged   */
    double Mh2, MH2, MH32;       /*  Masses scalars: h = SM Higgs (in the decoupling limit): H=heavy Higgs, H3=xtra scalar   */
    double MA2, MHplus2;        /*  Masses pseudo scalar: A = THDM pseudoscalar : Hplus=THDM charged scalar   */
    double M2p2, M2plus2;       /*  Masses pseudo scalar: h2p = non-THDM pseudoscalar : H2plus= non-THDM charged scalar   */

    double halfsqrt3 = 0.5 * sqrt(3.0);
	double mZ  = (9.11887E+01)*units;      /* 4     9.11887000E+01  # m_Z(pole)  */
	double mZ2 = mZ*mZ;
	double mW  = (80.379)*units;
	double mW2 = mW*mW;


	/*  ------------------------------------------------------------------------ --  */
	/*  ---------------------------------------------------------------------------  */
	/*  --    we take mu2 close to Man^2(very close to Map^2 and Maplus^2)      ---  */
	/*  --   (Man^2 - mu2 = 10,000 GeV^2) then we take lam13 small = 0.001      ---  */
	/*  --  to satisfy inequalities that ensure no tunneling to v_a \neq 0      ---  */
	/*  --  also Map and Maplus will be fixed by Map-Man=gapA and gapA free par ---  */
	/*  ---------------------------------------------------------------------------  */
	/*  ---------------------------------------------------------------------------  */

  /*
   *  Best Fit Point coordinates
  */

    Man    = 3.1397179600000000;  /*  in TeV, TODO not compatible if "units" is modified  */
    Man2   = Man*Man;

  /*  
   *  This 3 we keep fixed (or in terms of Man for the case of mu2)
   *  for a total of 12 free parameters
   * 
  */
    mu2    = Man2 - 10000.0*units*units;
    Mh     = 125.0*units;
    lam13  = 0.001;

    gapA   = pow(10.0, -6.7139079300000004); // (Map - Man)/Man     = gapA;
    gapH   = pow(10.0, -5.0584337599999998); // (Maplus - Man)/ Man = gapH;
    Map    = Man + gapA * Man;
    Maplus = Man + gapH * Man;

    MH     = 0.38355354500000000;  
    MH3    = 0.36805380199999999;  
    MA     = 0.50890192499999998;  
    MHplus = 0.27889866600000002;  
    M2p    = 0.34407693700000003;  
    M2plus = 0.33287005800000002;  
    tanth  = pow(10.0, 0.37351649100000001); 
    alfa   = -0.39435029700000002; 
    lam14  = 1.8675120599999999; 

  /*  
   *  The squares of the parameters, etc
   * 
  */
    Map2    = Map*Map;
    Maplus2 = Maplus*Maplus;
    Mh2     = Mh*Mh;
    MH2     = MH*MH;
    MH32    = MH3*MH3;
    MA2     = MA*MA;
    MHplus2 = MHplus*MHplus;
    M2p2    = M2p*M2p;
    M2plus2 = M2plus*M2plus;
    theta   = atan(tanth);



    double **ZH;
    ZH = (double **) malloc(3 * sizeof(double*)) ;   
    for(i = 0; i<3; i++) {
       ZH[i] = (double *) malloc(3 * sizeof(double));
    }

    double **ZA;
    ZA = (double **) malloc(3 * sizeof(double*)) ;   
    for(i = 0; i<3; i++) {
       ZA[i] = (double *) malloc(3 * sizeof(double));
    }

    double vrel=0.002;
    
    ZH[0][0]=cos(alfa);
    ZH[0][1]=halfsqrt3*sin(alfa);
    ZH[0][2]=0.5*sin(alfa);
    ZH[1][0]=0.0;
    ZH[1][1]=0.5;
    ZH[1][2]=-halfsqrt3;
    ZH[2][0]=-sin(alfa);
    ZH[2][1]=halfsqrt3*cos(alfa);
    ZH[2][2]=0.5*cos(alfa);

    ZA[0][0]=cos(theta)*sin(theta);
    ZA[0][1]=halfsqrt3*sin(theta)*sin(theta);
    ZA[0][2]=0.5*sin(theta)*sin(theta);
    ZA[1][0]=0.0;
    ZA[1][1]=0.5;
    ZA[1][2]=-halfsqrt3;
    ZA[2][0]=-cos(theta)*sin(theta);
    ZA[2][1]=halfsqrt3*cos(theta)*cos(theta);
    ZA[2][2]=0.5*cos(theta)*cos(theta);

    /*  
     *  END Parameters of the model
     * 
    */

    /*  ---------------------------------------------------------  */
    /*  ---------------------------------------------------------  */
    /*  ---------------------------------------------------------  */


    /*  
     *  Next we invert to obtain the quartic couplings etc
     * 
    */

    lam1 = (18*MHplus2 + (9*Mh2 + 9*MH2 + 18*M2plus2 - 
            2*MH32 - 18*MHplus2 + 
            9*(Mh2 - MH2)*cos(2*alfa))*pow(1/sin(theta),2))/(36.*pow(v,2));
    lam2 = (-MA2 + MHplus2 + (-M2p2 + M2plus2 + 
             MA2 - MHplus2)*pow(1/sin(theta),2))/(2.*pow(v,2));
    lam3 = ((-18*M2plus2 + 8*MH32 + 9*MHplus2 + 
               9*MHplus2*cos(2*theta))*pow(1/sin(theta),2))/(36.*pow(v,2));
    lam4 = (-2*MH32*1/cos(theta)*1/sin(theta))/(9.*pow(v,2));
    lam5 = (36*MHplus2 + 2*MH32*pow(1/cos(theta),2) + 
             9*(-Mh2 + MH2)*1/cos(theta)*1/sin(theta)*sin(2*alfa))/(18.*pow(v,2));
    lam6 = (9*MA2 - 18*MHplus2 + MH32*pow(1/cos(theta),2))/(9.*pow(v,2));
    lam7 = (-9*MA2 + MH32*pow(1/cos(theta),2))/(18.*pow(v,2));
    lam8 = (pow(1/cos(theta),2)*(9*(Mh2 + MH2) + 
            9*(-Mh2 + MH2)*cos(2*alfa) - 2*MH32*pow(tan(theta),2)))/(36.*pow(v,2));
    lam9 = 0.0;

    vs = v*cos(theta);
    v1 = vs;
    v2 = (v*sin(theta))/2.;
    mu0 = (-4*lam4*pow(v2,3) - 2*lam5*pow(v2,2)*vs - 
            2*lam6*pow(v2,2)*vs - 4*lam7*pow(v2,2)*vs - lam8*pow(vs,3))/vs;
    mu1 = (-8*lam1*pow(v2,2) - 8*lam3*pow(v2,2) - 6*lam4*v2*vs - 
            lam5*pow(vs,2) - lam6*pow(vs,2) - 2*lam7*pow(vs,2))/2.;

    lam10 = (2*(Maplus2 - mu2)*pow(1/sin(theta),2))/pow(v,2);
    lam11 = -((-(Man2*pow(1/sin(theta),2)) - Map2*pow(1/sin(theta),2) + 
                2*Maplus2*pow(1/sin(theta),2) + lam14*pow(v,2)*pow(1/tan(theta),2))/pow(v,2));
    lam12 = ((Man2 - Map2)*pow(1/sin(theta),2))/(2.*pow(v,2));


    /*  
     *  We put them in an array ...
     *  TODO: obviously this part and the previous one can be better optimized
    */


    double lambdas[15];

    lambdas[1]=lam1;
    lambdas[2]=lam2;
    lambdas[3]=lam3;
    lambdas[4]=lam4;
    lambdas[5]=lam5;
    lambdas[6]=lam6;
    lambdas[7]=lam7;
    lambdas[8]=lam8;
    lambdas[9]=lam9;
    lambdas[10]=lam10;
    lambdas[11]=lam11;
    lambdas[12]=lam12;
    lambdas[13]=lam13;
    lambdas[14]=lam14;

    double Mdm[4];

    Mdm[0]=0.0;    /* flag for handling errors instead of nrerror ( yes, a flag of type double :P )  */
    Mdm[1]=Man;
    Mdm[2]=Map;
    Mdm[3]=Maplus;

    double Mlight[10];

    Mlight[1]=MH;
    Mlight[2]=MH3;
    Mlight[3]=Mh;
    Mlight[4]=mZ;
    Mlight[5]=M2p;
    Mlight[6]=MA;
    Mlight[7]=mW;
    Mlight[8]=M2plus;
    Mlight[9]=MHplus;

    /*  
     *  Now define the initial conditions at x = \infty
     *  
    */

    ystart[1]=0.0;
    ystart[2]=0.0;
    ystart[3]=0.0;
    ystart[4]=0.0;
    ystart[5]=0.0;
    ystart[6]=0.0;
    ystart[7]=0.0;
    ystart[8]=0.0;
    ystart[9]=0.0;

    ystart[10]=Man*vrel*0.5;
    ystart[11]=0.0;
    ystart[12]=0.0;
    ystart[13]=0.0;
    ystart[14]=0.0;
    ystart[15]=0.0;
    ystart[16]=0.0;
    ystart[17]=0.0;
    ystart[18]=0.0;

    /*  
     *  These ystart below depend on the values of the potential and the mass
     *  Here are set accordingly
    */

    coini = 1.0-(4.0/(Man*vrel*vrel)*VNRij(5, x1, lambdas, ZH, ZA, Mdm, Mlight, theta));
    if(coini<0.0){
        ystart[5] =-sqrt(fabs(coini))*Man*vrel*0.5;
    }else{
        ystart[14]= sqrt(fabs(coini))*Man*vrel*0.5;
    }

    coini = 1.0-(4.0/(Man*vrel*vrel)*VNRij(9, x1, lambdas, ZH, ZA, Mdm, Mlight, theta));
    if(coini<0.0){
        ystart[9] =-sqrt(fabs(coini))*Man*vrel*0.5;
    }else{
        ystart[18]= sqrt(fabs(coini))*Man*vrel*0.5;
    }

	/*  ---------------------------------------------------------  */
	/*  ---------------------------------------------------------  */
	/*  ---------------------------------------------------------  */

    /*  
     *  Solve the system of equations to obtain the sought matrices at x = 0
     *  
    */

	odeSDA(ystart,Neqs,x1,x2,eps,h1,hmin,&nok,&nbad,derivs,rkqsSDA,stiffSDA,
            lambdas, ZH, ZA, Mdm, Mlight, theta);
    if(Mdm[0]>0) {
        printf("\n\n --->>> Error in odeSDA, implement a proper error handling if you want to know what was the error cause\n\n");
        exit(1);
    }

    double **hres=dmatrix(1,3,1,3);
    double *d1jSomm=dvector(1,3);
    double **vectmat=dmatrix(1,3,1,3);
    double *eigen=dvector(1,3);



    /*  
     *  Construct matrix h - h^dagger at x=0
     *  
    */
    

    hres[1][1]=2*ystart[10] / (Mdm[1]*vrel);
    hres[1][2]=2*ystart[11] / (Mdm[1]*vrel);
    hres[1][3]=2*ystart[12] / (Mdm[1]*vrel);
    hres[2][1]=hres[1][2];
    hres[2][2]=2*ystart[14] / (Mdm[1]*vrel);
    hres[2][3]=2*ystart[15] / (Mdm[1]*vrel);
    hres[3][1]=hres[1][3];
    hres[3][2]=hres[2][3];
    hres[3][3]=2*ystart[18] / (Mdm[1]*vrel);

    /*  Get the eigenvector with non-zero eigenvalue  */
    /*  It contains the Sommerfeld factors            */

    jacobi(hres,3,eigen,vectmat,&i);
    if(i==1) {
        printf("\n\n --->>> Error in jacobi, implement a proper error handling if you want to know what was the error cause\n\n");
        exit(1);
    }

    eigsrt(eigen,vectmat,3);
    if(eigen[1] < 0.1) {
        printf("\n\n --->>> Warning: Module of vector d of Sommerfeld factors less than 0.1, are you sure this is the right eigenvector?\n\n");
        printf("eigen[1] = %lf/n/n", eigen[1]);
    }



    /*  
     *  The vector components of d are the Sommerfeld factors
     *  
    */
    for(jj=1;jj<=3;jj++){
        d1jSomm[jj]=sqrt(eigen[1])*vectmat[jj][1];
    }



    /* ------------------------------------------------------------- */
    /* Next step: calculate the enhanced annihilation cross section  */
    /* ------------------------------------------------------------- */

    /*  
     *  The vector components of d are (in our basis) the first row of the matrix Dij
     *  
    */

    /*  
     *  The present day s-wave ann x-sect of the DM particle to final state "f" is given by
     *
     *  \sigma v (DM DM --> f) = (1/2)*( D . Gamma^f . D^\dagger )_{1 1}
     *
     *  where Gamma^f is the matrix of absorptive terms to final state "f"
    */

    /* --------------------------------------------------------------------- */
    /* Next step: obtain the differetntial ann x-sect using the given yields */
    /* --------------------------------------------------------------------- */

    /*  
     *  The total differential cross section into gammas is given by
     *  
     *  d(\sigma v) / dEgamma = \sum_f \sigma v (DM DM --> f)  \times  (dN/dEgamma)^f
     *  
     *  For the case of continuous yields (f = EW or Higgs boson pair as final state)
     *  we use the parametrization:
     *  
     *  (dN/dEgamma)^f = (0.73/M_DM) * x^(1.5) * exp(-7.8 * x)
     *
     *  with x = Egamma / M_DM
    */

    /*  
     *  For the gamma gamma or the gamma Z final states the yield is a Dirac delta
     *  centered respectively at
     *
     *  \mu_line = M_DM   or   M_DM - (M_Z)^2 / (4*M_DM)
     *
     *  We model the delta as a gaussian centered at the correspondig energy
     *  and of width equal to (conservately) 15% the energy of the line, this
     *  since a delta would be a "monochromatic line" which in the context of
     *  ID experiments refers to spectral features with energy width much smaller 
     *  than the energy resolution of the detector, tipically 15% is achieved e.g.
     *  in HESS and therefore such value would be conservative for the CTA.
     *
     *  Thus, with gaussian width = 0.15 M_DM:
     *
     *  (dN/dEgamma)^{gamma gamma} = 2 * \delta(E_gamma - M_DM)
     *                             = 2 * (2.65962 / M_DM) * exp( - 22.2222 * ( x - M_DM )^2 / M_DM^2 )
     *
     *  and similar for gamma Z
    */

    /* ------------------------------------------------------------------------- */
    /* Finally: compute the differential gamma ray flux for every given J-factor */
    /* ------------------------------------------------------------------------- */

    double diffSigmaVdE = 0.0;

    FILE *fp1, *fp2;

    fp1 = fopen (argv[1],"w");
    if (fp1 == NULL) {
                          nrerror("flux file");
                          exit(1);
                        }

    fp2 = fopen ("Null_Result_Coma.txt","w");
    if (fp2 == NULL) {
                          nrerror("flux file");
                          exit(1);
                        }


    double fixedLogDeltaE    = 0.115129;
    double iniDeltaE         = 0.001;           /* TeV  */
    double logDeltaE         = log(iniDeltaE);
    double energyTev         = iniDeltaE;

    double jpsi[]={pow(10.0,19.52)};   /* Coma Berenices  log10(J/GeV^2 cm^−5) = 19.52 */


    jpsi[0] = jpsi[0] * 1E-6; /* TeV^2 cm^−5 */

    double ycontinue   = yield_continue(Mdm[1], energyTev);
    double ygammagamma = 2 * yield_delta(Mdm[1], energyTev);
    double ygammaZ     = 2 * yield_delta(Mdm[1]-mZ2/(4*Mdm[1]), energyTev);
    int channels=8;


    /*  
     *  channels
     *  
     *  0 --> gamma gamma
     *  1 --> gamma Z
     *  2 --> Z Z
     *  3 --> W W
     *  4 --> H H
     *  5 --> H3 H3
     *  6 --> h h
     *  7 --> H h
     *  
    */

    double sVsomm[8];    /*  hardcoded :P  */

    for (i = 0; i < channels; i++)
    {
      sVsomm[i] = sigmaVsomm(lambdas, d1jSomm, Mdm[1], Mlight, theta, alfa, i);
    }

    for (i = 2; i < channels; i++)
    {
        diffSigmaVdE = diffSigmaVdE + ycontinue * sVsomm[i];
    }

    diffSigmaVdE = diffSigmaVdE + ygammagamma * sVsomm[0];
    diffSigmaVdE = diffSigmaVdE + ygammaZ * sVsomm[1];

    double conversion_factor = 116427E-28;  /* c \times (hbar c)^2 TeV^2 cm^3 s^-1 */

    diffSigmaVdE = diffSigmaVdE * conversion_factor; /* cm^3 s^-1 TeV^-1 */

    double fluxgammadiff = diffSigmaVdE * jpsi[0] / (8*M_PI*Mdm[1]*Mdm[1]); /* ph cm^-2 s^-1 TeV^-1 */

    /*  ctools requires the flux file in MeVs  */
    fluxgammadiff = fluxgammadiff * 1E-6; /* ph cm^-2 s^-1 MeV^-1 */

    fprintf(fp1,"%lE %lE\n", energyTev * 1E6, fluxgammadiff);

    /*  Include flux reduced to almost nothing to be used for the null result sim  */
    fprintf(fp2,"%lE %lE\n", energyTev * 1E6, fluxgammadiff * 1E-20);

    for (jj = 1; jj < 100; jj++)
    {
        logDeltaE      = logDeltaE + fixedLogDeltaE;
        energyTev      = exp(logDeltaE);
        ycontinue      = yield_continue(Mdm[1], energyTev);
        ygammagamma    = 2 * yield_delta(Mdm[1], energyTev);
        ygammaZ        = 2 * yield_delta(Mdm[1]-mZ2/(4*Mdm[1]), energyTev);
        diffSigmaVdE   = 0.0;
        for (i = 2; i < channels; i++)
        {
            diffSigmaVdE = diffSigmaVdE + ycontinue * sVsomm[i];
        }
    
        diffSigmaVdE = diffSigmaVdE + ygammagamma * sVsomm[0];
        diffSigmaVdE = diffSigmaVdE + ygammaZ * sVsomm[1];


        diffSigmaVdE = diffSigmaVdE * conversion_factor; /* cm^3 s^-1 TeV^-1 */

        fluxgammadiff = diffSigmaVdE * jpsi[0] / (8*M_PI*Mdm[1]*Mdm[1]); /* ph cm^-2 s^-1 TeV^-1 */

        /*  ctools requires the flux file in MeVs  */
        fluxgammadiff = fluxgammadiff * 1E-6; /* ph cm^-2 s^-1 MeV^-1 */
    
        fprintf(fp1,"%lE %lE\n", energyTev * 1E6, fluxgammadiff);

        /*  Include flux reduced to almost nothing to be used for the null result sim  */
        fprintf(fp2,"%lE %lE\n", energyTev * 1E6, fluxgammadiff * 1E-20);

         if( energyTev > Mdm[1] && fluxgammadiff < 1e-50 ) break;
    }


    /*   done   */

    fclose(fp1);
    fclose(fp2);

    free_dvector(eigen,1,3);
    free_dmatrix(vectmat,1,3,1,3);
    free_dvector(d1jSomm,1,3);
    free_dmatrix(hres,1,3,1,3);
	  free_dmatrix(yp,1,Neqs+5,1,2000);
	  free_dvector(xp,1,2000);
	  free_dvector(ystart,1,Neqs);

    for(i = 0; i<3; i++) free(ZH[i]);
    free(ZH);
    for(i = 0; i<3; i++) free(ZA[i]);
    free(ZA);

	return 0;
}
#undef NRANSI
