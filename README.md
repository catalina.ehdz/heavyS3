# Heavy S3

This code was developed with the specific aim of calculating the results
presented in the research paper:

```
Prospects of Indirect Detection for the Heavy S3 Dark Doublet
C. Espinoza and M. Mondragon
Submitted
```


## Intent

The release of this code is intended solely for purposes of reproducibility
of the results, as such I don't think it will be useful for other people except
perhaps for others working in this specific model.

### Prerequisites

Working installations of the following tools are necessary

```
ctools
Diver
HiggsBounds
Micromegas
Spheno
```

Model files for Spheno and Micromegas are included, alternatively you can use

`SARAH`

with the included SARAH model files to generate them.

_(Detail instructions for configuring the whole set up can be requested from the author)_


## Acknowledgments

This work relies heavily in the following public tools which do the _hard_ work,
many thanks to all of their developers!

* [CalcHEP](http://theory.sinp.msu.ru/~pukhov/calchep.html) - A package for calculation of Feynman diagrams and integration over multi-particle phase space.
* [ctools](http://cta.irap.omp.eu/ctools/) - Cherenkov Telescope Array Science Analysis Software.
* [Diver](https://diver.hepforge.org/) - A fast parameter sampler and optimiser based on differential evolution.
* [FeynArts](http://www.feynarts.de/) - A Mathematica package for the generation and visualization of Feynman diagrams and amplitudes.
* [FeynCalc](https://feyncalc.github.io/) - Tools and Tables for Quantum Field Theory Calculations.
* [FormCalc](http://www.feynarts.de/formcalc/) - A Mathematica package for the calculation of tree-level and one-loop Feynman diagrams.
* [HiggsBounds](https://gitlab.com/higgsbounds/higgsbounds) - Testing BSM Higgs sectors against limits from LEP, Tevatron and LHC Higgs searches.
* [Micromegas](https://lapth.cnrs.fr/micromegas/) - A code for the calculation of Dark Matter Properties.
* [Pippi](http://github.com/patscott/pippi) - Painless parsing, post-processing and plotting of posterior and likelihood samples.
* [SARAH](https://sarah.hepforge.org/) - A Mathematica package for building and analyzing SUSY and non-SUSY models.
* [Spheno](http://projects.hepforge.org/spheno/) - S(upersymmetric) Pheno(menology).


## License

Except where explicitly stated otherwise in the code, this project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details
