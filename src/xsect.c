#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "xsect.h"


extern const double units;


    /*  
     *  Note that these routines return values in natural units.
     *
     *  The total differential cross section into gammas is given by
     *  
     *  d(\sigma v) / dEgamma = \sum_f \sigma v (DM DM --> f)  \times  (dN/dEgamma)^f
     *  
     *  For the case of continuous yields (f = EW or Higgs boson pair as final state)
     *  we use the parametrization:
     *  
     *  (dN/dEgamma)^f = (0.73/M_DM) * x^(1.5) * exp(-7.8 * x)
     *
     *  with x = Egamma / M_DM
    */


    /*  
     *  For the gamma gamma or the gamma Z final states the yield is a Dirac delta
     *  centered respectively at
     *
     *  \mu_line = M_DM   or   M_DM - (M_Z)^2 / (4*M_DM)
     *
     *  We model the delta as a gaussian centered at the correspondig energy
     *  and of width equal to (conservately) 15% the energy of the line, this
     *  since a delta would be a "monochromatic line" which in the context of
     *  ID experiments refers to spectral features with energy width much smaller 
     *  than the energy resolution of the detector, tipically 15% is achieved e.g.
     *  in HESS and therefore such value would be conservative for the CTA.
     *
     *  Thus, with gaussian width = 0.15 M_DM:
     *
     *  (dN/dEgamma)^{gamma gamma} = 2 * \delta(E_gamma - M_DM)
     *                        = 2 * (2.65962 / M_DM) * exp( - 22.2222 * ( x - M_DM )^2 / M_DM^2 )
     *
     *  and similar for gamma Z
    */


double yield_continue(double mdm, double energy){

	double x = energy/mdm;
	double res = (0.73/mdm) * pow(x,1.5) * exp(-7.8 * x);

	return res;
}

double yield_delta(double mdm, double energy){

	double res = (2.65962 / mdm) * 
	             exp( - 22.2222 * ( energy - mdm )*( energy - mdm ) / mdm/mdm );

	return res;
}

double Sec(double x){

	double res = 1. / cos(x);
	return res;
}


    /*  
     *  The present day s-wave ann x-sect of the DM particle to final state "f" is given by
     *
     *  \sigma v (DM DM --> f) = (1/2)*( D . Gamma^f . D^\dagger )_{1 1}
     *
     *  where Gamma^f is the matrix of absorptive terms to final state "f"
    */


    /*
     * \sigma v (DM DM --> f) = (1/2)*( D . Gamma^f . D^\dagger )_{1 1}
	 *
	 * In our case:
	 *
	 *    D . Gamma^f . D =
	 *    d11 (d11 G11+d12 G12+d13 G13)+
	 *    d12 (d11 G21+d12 G22+d13 G23)+
	 *    d13 (d11 G31+d12 G32+d13 G33);
	 *
	 * We hardcode the Gs from the mathematica notebooks calculations.
    */


double sigmaVsomm(double lambdas[], double d1jSomm[], double Man, 
	              double Mlight[], double theta, double alfa, int i){

	double v=246.0*units;
	double v1 = v*cos(theta);
    double v2 = (v*sin(theta))/2.;
	double sinW2 = 1. - Mlight[7]*Mlight[7]/Mlight[4]/Mlight[4];   //sinW2 = 1. - mW2/mZ2; 
	double sw = sqrt(sinW2);
	double cw = Mlight[7]/Mlight[4];
	double Alpha_MZ  = 1./(1.27932E+02);  //   # alpha_em^-1(MZ)^MSbar
	double g1    = sqrt(4*M_PI*Alpha_MZ/(1-sinW2)); 
	double g2    = sqrt(4*M_PI*Alpha_MZ/ sinW2); 

	double e  = g2*sw;
	double TW = atan(sw/cw);
	double Pi = M_PI;


	/* 
	 * ugly code but I'm lazy,
	 * not to mention the bureaucratic deadlines to publish 
	 * otherwise your job is gone: 
	 *  
	 */

    double lam1  = lambdas[1];
    double lam2  = lambdas[2];
    double lam3  = lambdas[3];
    double lam4  = lambdas[4];
    double lam5  = lambdas[5];
    double lam6  = lambdas[6];
    double lam7  = lambdas[7];
    double lam8  = lambdas[8];
    double lam9  = lambdas[9];
    double lam10 = lambdas[10];
    double lam11 = lambdas[11];
    double lam12 = lambdas[12];
    double lam13 = lambdas[13];
    double lam14 = lambdas[14];

	double G11=0;
	double G12=0;
	double G13=0;
	double G21=0;
	double G22=0;
	double G23=0;
	double G31=0;
	double G32=0;
	double G33=0;

	double res;

	switch(i){

		case 0:
			/* final state gamma gamma */
			G33 = pow(e,4)/(4.*(Man*Man)*M_PI);
			break;

		case 1:
			/* final state gamma Z */
			G33 = (pow(e,4)*pow((1/tan(2*TW)),2))/(4.*(Man*Man)*M_PI);
			break;

		case 2:
			/* final state Z Z */
			G11 = (4*pow(lam10 + lam11 + 2*lam12 + lam14 - (lam10 + lam11 + 2*lam12 - lam14)*
				   cos(2*theta),2) + 8*pow(g2,4)*pow(Sec(TW),4))/(1024.*(Man*Man)*M_PI);
			G12 = (3*pow(lam10 + lam11,2) - 
				   12*pow(lam12,2) + 2*(lam10 + lam11)*lam14 + 3*pow(lam14,2) - 
				   4*(pow(lam10 + lam11,2) - 4*pow(lam12,2) - 
				   pow(lam14,2))*cos(2*theta) + 
				   (lam10 + lam11 - 2*lam12 - lam14)*(lam10 + lam11 + 
				   	2*lam12 - lam14)*cos(4*theta) + 
				   4*pow(g2,4)*pow(Sec(TW),4))/(512.*pow(Man,2)*Pi);
			G13 = (pow(g2,4)*pow(cos(2*TW),2)*pow(Sec(TW),4) + lam10*(lam10 + lam11 + 
				   2*lam12 + lam14 - (lam10 + lam11 + 2*lam12 - lam14)*cos(2*theta))*
			       pow(sin(theta),2))/(64.*sqrt(2)*pow(Man,2)*Pi);
			G22 = (4*pow(lam10 + lam11 - 2*lam12 + lam14 - (lam10 + lam11 - 2*lam12 - lam14)*
				   cos(2*theta),2) + 8*pow(g2,4)*pow(Sec(TW),4))/(1024.*(Man*Man)*M_PI);
			G23 = (pow(g2,4)*pow(cos(2*TW),2)*pow(Sec(TW),4) + 
				   lam10*(lam10 + lam11 - 2*lam12 + lam14 - 
				   (lam10 + lam11 - 2*lam12 - lam14)*cos(2*theta))*
				   pow(sin(theta),2))/(64.*sqrt(2)*pow(Man,2)*Pi);
			G33 = (pow(g2,4)*pow(cos(2*TW),4)*pow(Sec(TW),4) + 
				   2*(lam10*lam10)*pow(sin(theta),4))/(64.*(Man*Man)*M_PI);
			G21 = G12;
			G31 = G13;
			G32 = G23;
			break;

		case 3:
			/* final state W W */
			G11 = (4*pow(g2,4) + 3*pow(lam10 + lam11 + 2*lam12,2) + 
				   2*(lam10 + lam11 + 2*lam12)*lam14 + 3*(lam14*lam14) - 
				   4*(lam10 + lam11 + 2*lam12 - lam14)*(lam10 + lam11 + 2*lam12 + 
				   	lam14)*cos(2*theta) + pow(lam10 + lam11 + 2*lam12 - 
				   	lam14,2)*cos(4*theta))/(512.*(Man*Man)*M_PI);
			G12 = (4*pow(g2,4) + 3*pow(lam10 + lam11,2) - 12*pow(lam12,2) + 
				   2*(lam10 + lam11)*lam14 + 3*pow(lam14,2) - 4*(pow(lam10 + lam11,2) - 
				   4*pow(lam12,2) - pow(lam14,2))*cos(2*theta) + 
				   (lam10 + lam11 - 2*lam12 - lam14)*(lam10 + lam11 + 2*lam12 - 
				   	lam14)*cos(4*theta))/(512.*pow(Man,2)*Pi);
			G13 = (4*pow(g2,4) + lam10*(3*(lam10 + lam11 + 2*lam12) + lam14) - 
				   4*lam10*(lam10 + lam11 + 2*lam12)*cos(2*theta) + 
				   lam10*(lam10 + lam11 + 2*lam12 - 
				   lam14)*cos(4*theta))/(256.*sqrt(2)*pow(Man,2)*Pi);
			G22 = (4*pow(g2,4) + 3*pow(lam10 + lam11 - 2*lam12,2) + 
				   2*(lam10 + lam11 - 2*lam12)*lam14 + 3*(lam14*lam14) - 
				   4*(lam10 + lam11 - 2*lam12 - lam14)*(lam10 + lam11 - 
				   2*lam12 + lam14)*cos(2*theta) + pow(lam10 + lam11 - 
				   2*lam12 - lam14,2)*cos(4*theta))/(512.*(Man*Man)*M_PI);
			G23 = (4*pow(g2,4) + lam10*(3*(lam10 + lam11 - 2*lam12) + lam14) - 
				   4*lam10*(lam10 + lam11 - 2*lam12)*cos(2*theta) + lam10*(lam10 + lam11 - 
				   2*lam12 - lam14)*cos(4*theta))/(256.*sqrt(2)*pow(Man,2)*Pi);
			G33 = (4*pow(g2,4) + 3*(lam10*lam10) + lam10*lam10*(-4*cos(2*theta) + 
				   cos(4*theta)))/(256.*(Man*Man)*M_PI);
			G21 = G12;
			G31 = G13;
			G32 = G23;
			break;

		case 4:
			/* final state H H  */
			G11 = pow(lam14*pow(cos(alfa),2) + (lam10 + lam11 + 
				  2*lam12)*pow(sin(alfa),2),2)/(64.*(Man*Man)*M_PI);
			G12 = (9*(lam14*pow(cos(alfa),2) + (lam10 + lam11 - 
				   2*lam12)*pow(sin(alfa),2))*(lam14*pow(cos(alfa),2) + 
			      (lam10 + lam11 + 2*lam12)*pow(sin(alfa),2)))/(64.*pow(Man,2)*Pi);
			G13 = (9*lam10*pow(sin(alfa),2)*(lam14*pow(cos(alfa),2) + (lam10 + 
				   lam11 + 2*lam12)*pow(sin(alfa),2)))/(32.*sqrt(2)*pow(Man,2)*Pi);
			G22 = pow(lam14*pow(cos(alfa),2) + (lam10 + lam11 - 
				  2*lam12)*pow(sin(alfa),2),2)/(64.*(Man*Man)*M_PI);
			G23 = (9*lam10*pow(sin(alfa),2)*(lam14*pow(cos(alfa),2) + 
				  (lam10 + lam11 - 2*lam12)*pow(sin(alfa),2)))/(32.*sqrt(2)*pow(Man,2)*Pi);
			G33 = (lam10*lam10*pow(sin(alfa),4))/(32.*(Man*Man)*M_PI);
			G21 = G12;
			G31 = G13;
			G32 = G23;
			break;

		case 5:
			/* final state H3 H3  */
			G11 = pow(lam10 + lam11 + 2*lam12,2)/(64.*(Man*Man)*M_PI);
			G12 = (9*(lam10 + lam11 - 2*lam12)*(lam10 + lam11 + 2*lam12))/(64.*pow(Man,2)*Pi);
			G13 = (9*lam10*(lam10 + lam11 + 2*lam12))/(32.*sqrt(2)*pow(Man,2)*Pi);
			G22 = pow(lam10 + lam11 - 2*lam12,2)/(64.*(Man*Man)*M_PI);
			G23 = (9*lam10*(lam10 + lam11 - 2*lam12))/(32.*sqrt(2)*pow(Man,2)*Pi);
			G33 = (lam10*lam10)/(32.*(Man*Man)*M_PI);
			G21 = G12;
			G31 = G13;
			G32 = G23;
			break;

		case 6:
			/* final state h h */
			G11 = pow((lam10 + lam11 + 2*lam12)*pow(cos(alfa),2) + 
				  lam14*pow(sin(alfa),2),2)/(64.*(Man*Man)*Pi);
			G12 = (9*((lam10 + lam11 - 2*lam12)*pow(cos(alfa),2) + 
				  lam14*pow(sin(alfa),2))*((lam10 + lam11 + 2*lam12)*pow(cos(alfa),2) + 
				  lam14*pow(sin(alfa),2)))/(64.*pow(Man,2)*Pi);
			G13 = (9*lam10*pow(cos(alfa),2)*((lam10 + lam11 + 2*lam12)*pow(cos(alfa),2) + 
				  lam14*pow(sin(alfa),2)))/(32.*sqrt(2)*pow(Man,2)*Pi);
			G22 = pow((lam10 + lam11 - 2*lam12)*pow(cos(alfa),2) + 
				  lam14*pow(sin(alfa),2),2)/(64.*(Man*Man)*M_PI);
			G23 = (9*lam10*pow(cos(alfa),2)*((lam10 + lam11 - 2*lam12)*pow(cos(alfa),2) + 
				  lam14*pow(sin(alfa),2)))/(32.*sqrt(2)*pow(Man,2)*Pi);
			G33 = (lam10*lam10*pow(cos(alfa),4))/(32.*(Man*Man)*M_PI);
			G21 = G12;
			G31 = G13;
			G32 = G23;
			break;

		case 7:
			/* final state H h */
			G11 = (pow(lam10 + lam11 + 2*lam12 - 
				   lam14,2)*pow(cos(alfa),2)*pow(sin(alfa),2))/(64.*(Man*Man)*Pi);
			G12 = (9*(lam10 + lam11 - 2*lam12 - lam14)*(lam10 + lam11 + 2*lam12 - 
				  lam14)*pow(cos(alfa),2)*pow(sin(alfa),2))/(64.*pow(Man,2)*Pi);
			G13 = (9*lam10*(lam10 + lam11 + 2*lam12 - 
				  lam14)*pow(cos(alfa),2)*pow(sin(alfa),2))/(32.*sqrt(2)*pow(Man,2)*Pi);
			G22 = (pow(lam10 + lam11 - 2*lam12 - 
				  lam14,2)*pow(cos(alfa),2)*pow(sin(alfa),2))/(64.*(Man*Man)*Pi);
			G23 = (9*lam10*(lam10 + lam11 - 2*lam12 - 
				  lam14)*pow(cos(alfa),2)*pow(sin(alfa),2))/(32.*sqrt(2)*pow(Man,2)*Pi);
			G33 = (lam10*lam10*pow(cos(alfa),2)*pow(sin(alfa),2))/(32.*(Man*Man)*Pi);
			G21 = G12;
			G31 = G13;
			G32 = G23;
			break;

        default :
         printf("\n\nError: Invalid channel number: %d\n\n",i);
         exit(1);
	}

	res = d1jSomm[1] * (d1jSomm[1] * G11 + d1jSomm[2] * G12 + d1jSomm[3] * G13)+
		  d1jSomm[2] * (d1jSomm[1] * G21 + d1jSomm[2] * G22 + d1jSomm[3] * G23)+
		  d1jSomm[3] * (d1jSomm[1] * G31 + d1jSomm[2] * G32 + d1jSomm[3] * G33);

	res = res/2.0;

	return res;

}