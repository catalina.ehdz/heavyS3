#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif

/*  ---  ...........................................................  ---*/
/*  ---  Equations for the calculation of the Sommerfeld corrections  ---*/
/*  ---        for the Dark S3 doublet ---  arxiv:xxxx.xxxx           ---*/
/*  ---  ...........................................................  ---*/

#include <stdio.h>
#include <math.h>

extern int nrhs; 
extern const double units;

/*  ---  ...........................................................  ---*/
/*  ---                  h_{ij}(r) = O_{ij}(r) + I P_{ij}(r)           ---*/
/*  ---          O_{11} = O_1 , O_{12} = O_2 , O_{13} = O_3           ---*/
/*  ---          O_{21} = O_4 , O_{22} = O_5 , O_{23} = O_6           ---*/
/*  ---          O_{31} = O_7 , O_{32} = O_8 , O_{33} = O_9           ---*/
/*  ---  ...........................................................  ---*/
/*  ---          P_{11} = P_1 , P_{12} = P_2 , P_{13} = P_3           ---*/
/*  ---          P_{21} = P_4 , P_{22} = P_5 , P_{23} = P_6           ---*/
/*  ---          P_{31} = P_7 , P_{32} = P_8 , P_{33} = P_9           ---*/
/*  ---  ...........................................................  ---*/
/*  ---                                                               ---*/
/*  ---          y[1] = O_1 , y[2] = O_2 , y[3] = O_3                 ---*/
/*  ---          y[4] = O_4 , y[5] = O_5 , y[6] = O_6                 ---*/
/*  ---          y[7] = O_7 , y[8] = O_8 , y[9] = O_9                 ---*/
/*  ---  ...........................................................  ---*/
/*  ---          y[10] = P_1 , y[11] = P_2 , y[12] = P_3              ---*/
/*  ---          y[13] = P_4 , y[14] = P_5 , y[15] = P_6              ---*/
/*  ---          y[16] = P_7 , y[17] = P_8 , y[18] = P_9              ---*/
/*  ---  ...........................................................  ---*/
/*  ---                        y[19] = x                              ---*/
/*  ---                     Oik[\infty] = 0                           ---*/
/*  ---                     Pik[\infty] = (M v / 2) delta_ij          ---*/
/*  ---  ...........................................................  ---*/
/*  ---  ...........................................................  ---*/
/*  ---  (d/dx) Oij = - Oik Okj + Pik Pkj                             ---*/
/*  ---                         - M( 0.25 M v^2 delta_ij - V_ij )     ---*/
/*  ---  ...........................................................  ---*/
/*  ---  (d/dx) Pij = - Oik Pkj - Pik Okj                             ---*/
/*  ---  ...........................................................  ---*/
/*  ---  ...........................................................  ---*/
/*  ---       lambdas[1] = lam1 , ... , lambdas[14] = lam14           ---*/
/*  ---       Mdm[1] = Man , Mdm[2] = Map , Mdm[3] = Maplus           ---*/
/*  ---  ...........................................................  ---*/
/*  ---  ...........................................................  ---*/
/*  
    Mlight[1]=MH;
    Mlight[2]=MH3;
    Mlight[3]=Mh;
    Mlight[4]=mZ;       =mG0
    Mlight[5]=M2p;      =MA3
    Mlight[6]=MA;   
    Mlight[7]=mW;       =mGw
    Mlight[8]=M2plus;   =MH3pm
    Mlight[9]=MHplus;   =MHpm
*/

double VNRij(int j, double x, double lambdas[], double **ZH, double **ZA, double Mdm[], double Mlight[],
	         double theta)
{

	if(j>9 || j<1) return 0.0;

	int i;
	double Vpot=0.0;
	double couphahahk[4];
	double coupAaAahk[4];
	double couphaAaAk[4];
	double couphaAaZ;
	double couphaHapmHw[4];
	double couphaHapmW;
	double coupAaHapmHw[4];
	double coupAaHapmW;
	double coupHapmHapmhk[4];
	double coupHapmHapmZ;
	double coupHapmHapmg;
	double v=246.0*units;
	double v1 = v*cos(theta);
    double v2 = (v*sin(theta))/2.;
	double sinW2 = 1. - Mlight[7]*Mlight[7]/Mlight[4]/Mlight[4];   //sinW2 = 1. - mW2/mZ2; 
	double sw = sqrt(sinW2);
	double cw = Mlight[7]/Mlight[4];
	double Alpha_MZ  = 1./(1.27932E+02);  //   # alpha_em^-1(MZ)^MSbar
	double g1    = sqrt(4*M_PI*Alpha_MZ/(1-sinW2)); 
	double g2    = sqrt(4*M_PI*Alpha_MZ/ sinW2); 

	switch(j)
	{
		case 1:
			/* (1,1) */
			for(i=1;i<=3;i++) couphahahk[i]=(v1*lambdas[14]*(ZH[i-1][0])
				+ v2*(lambdas[10]+lambdas[11]+lambdas[12])*(sqrt(3.0)*(ZH[i-1][1])+ZH[i-1][2]));
			for(i=1;i<=3;i++) Vpot=Vpot+couphahahk[i]*couphahahk[i]*exp(-Mlight[i]*x);
			Vpot=Vpot*(-1.0)/(16*M_PI*Mdm[1]*Mdm[1]*x);
			break;

		case 2:
			/* (1,2) */
			for(i=1;i<=3;i++) couphaAaAk[i]=2*lambdas[12]*v2*(sqrt(3.0)*(ZA[i-1][1])+ZA[i-1][2]);
			couphaAaZ = 0.5*(g1*sw+g2*cw);
			for(i=1;i<=3;i++) Vpot=Vpot+couphaAaAk[i]*couphaAaAk[i]*exp(-Mlight[i+3]*x);
			Vpot=Vpot*(-1.0)/(16*M_PI*Mdm[1]*Mdm[1]*x);
			Vpot=Vpot+(-g2*g2)/(16*M_PI*cw*cw*x) * exp(-Mlight[4]*x);
			break;

		case 3:
			/* (1,3) */
			for(i=1;i<=3;i++) couphaHapmHw[i]=0.5*(lambdas[14]*v1*(ZA[i-1][0]) + 
				              (2*lambdas[12]+lambdas[11])*v2*(sqrt(3.0)*(ZA[i-1][1])+ZA[i-1][2]));
			couphaHapmW = 0.5*g2;
			for(i=1;i<=3;i++) Vpot=Vpot+couphaHapmHw[i]*couphaHapmHw[i]*exp(-Mlight[i+6]*x);
			Vpot=Vpot*(-1.0)/(16*M_PI*Mdm[1]*Mdm[1]*x);
			Vpot=Vpot+(-g2*g2)/(16*M_PI*x) * exp(-Mlight[7]*x);
			Vpot=Vpot*sqrt(2.0); /*  ---  2-body basis factor  ---  */
			break;

		case 4:
			/* (2,1) */
			for(i=1;i<=3;i++) couphaAaAk[i]=2*lambdas[12]*v2*(sqrt(3.0)*(ZA[i-1][1])+ZA[i-1][2]);
			couphaAaZ = 0.5*(g1*sw+g2*cw);
			for(i=1;i<=3;i++) Vpot=Vpot+couphaAaAk[i]*couphaAaAk[i]*exp(-Mlight[i+3]*x);
			Vpot=Vpot*(-1.0)/(16*M_PI*Mdm[1]*Mdm[1]*x);
			Vpot=Vpot+(-g2*g2)/(16*M_PI*cw*cw*x) * exp(-Mlight[4]*x);
			break;

		case 5:
			/* (2,2) */
			for(i=1;i<=3;i++) coupAaAahk[i]=(v1*lambdas[14]*(ZH[i-1][0])
				+ v2*(lambdas[10]+lambdas[11]-2.0*lambdas[12])*(sqrt(3.0)*(ZH[i-1][1])+ZH[i-1][2]));
			for(i=1;i<=3;i++) Vpot=Vpot+coupAaAahk[i]*coupAaAahk[i]*exp(-Mlight[i]*x);
			Vpot=Vpot*(-1.0)/(16*M_PI*Mdm[1]*Mdm[1]*x);
			Vpot=Vpot+2*(Mdm[2]-Mdm[1]);       /*  ---   mass diff term see Ibarra  ---  */
			break;

		case 6:
			/* (2,3) */
			for(i=1;i<=3;i++) coupAaHapmHw[i]=0.5*(lambdas[14]*v1*(ZA[i-1][0]) + 
				                (lambdas[11]-2*lambdas[12])*v2*(sqrt(3.0)*(ZA[i-1][1])+ZA[i-1][2]));
			coupAaHapmW=0.5*g2;
			for(i=1;i<=3;i++) Vpot=Vpot+coupAaHapmHw[i]*coupAaHapmHw[i]*exp(-Mlight[i+6]*x);
			Vpot=Vpot*(-1.0)/(16*M_PI*Mdm[1]*Mdm[1]*x);
			Vpot=Vpot+(-g2*g2)/(16*M_PI*x) * exp(-Mlight[7]*x);
			Vpot=Vpot*sqrt(2.0); /*  ---  2-body basis factor  ---  */
			break;

		case 7:
			/* (3,1) */
			for(i=1;i<=3;i++) couphaHapmHw[i]=0.5*(lambdas[14]*v1*(ZA[i-1][0]) + 
				              (2*lambdas[12]+lambdas[11])*v2*(sqrt(3.0)*(ZA[i-1][1])+ZA[i-1][2]));
			couphaHapmW = 0.5*g2;
			for(i=1;i<=3;i++) Vpot=Vpot+couphaHapmHw[i]*couphaHapmHw[i]*exp(-Mlight[i+6]*x);
			Vpot=Vpot*(-1.0)/(16*M_PI*Mdm[1]*Mdm[1]*x);
			Vpot=Vpot+(-g2*g2)/(16*M_PI*x) * exp(-Mlight[7]*x);
			Vpot=Vpot*sqrt(2.0); /*  ---  2-body basis factor  ---  */
			break;

		case 8:
			/* (3,2) */
			for(i=1;i<=3;i++) coupAaHapmHw[i]=0.5*(lambdas[14]*v1*(ZA[i-1][0]) + 
				                (lambdas[11]-2*lambdas[12])*v2*(sqrt(3.0)*(ZA[i-1][1])+ZA[i-1][2]));
			coupAaHapmW=0.5*g2;
			for(i=1;i<=3;i++) Vpot=Vpot+coupAaHapmHw[i]*coupAaHapmHw[i]*exp(-Mlight[i+6]*x);
			Vpot=Vpot*(-1.0)/(16*M_PI*Mdm[1]*Mdm[1]*x);
			Vpot=Vpot+(-g2*g2)/(16*M_PI*x) * exp(-Mlight[7]*x);
			Vpot=Vpot*sqrt(2.0); /*  ---  2-body basis factor  ---  */
			break;

		case 9:
			/* (3,3) */
			for(i=1;i<=3;i++) coupHapmHapmhk[i]=lambdas[10]*v2*(sqrt(3.0)*(ZH[i-1][1])+ZH[i-1][2]);
			for(i=1;i<=3;i++) Vpot=Vpot+coupHapmHapmhk[i]*coupHapmHapmhk[i]*exp(-Mlight[i]*x);
			Vpot=Vpot*(-1.0)/(16*M_PI*Mdm[1]*Mdm[1]*x);
			Vpot=Vpot+(-g2*g2)/(4*M_PI*x)*
			            (sinW2+(1-2*sinW2)*(1-2*sinW2)/(4*cw*cw)*exp(-Mlight[4]*x));
			Vpot=Vpot+2*(Mdm[3]-Mdm[1]);       /*  ---   mass diff term see Ibarra  ---  */
			break;
	}
	return Vpot;
}

double diffVNRij(int j, double x, double lambdas[], double **ZH, double **ZA, double Mdm[], double Mlight[],
	         double theta)
{

	if(j>9 || j<1) return 0.0;

	int i;
	double Vpot=0.0;
	double Vpot1=0.0;
	double Vpot2=0.0;
	double couphahahk[4];
	double coupAaAahk[4];
	double couphaAaAk[4];
	double couphaAaZ;
	double couphaHapmHw[4];
	double couphaHapmW;
	double coupAaHapmHw[4];
	double coupAaHapmW;
	double coupHapmHapmhk[4];
	double coupHapmHapmZ;
	double coupHapmHapmg;
	double v=246.0*units;
	double v1 = v*cos(theta);
    double v2 = (v*sin(theta))/2.;
	double sinW2 = 1. - Mlight[7]*Mlight[7]/Mlight[4]/Mlight[4];   //sinW2 = 1. - mW2/mZ2; 
	double sw = sqrt(sinW2);
	double cw = Mlight[7]/Mlight[4];
	double Alpha_MZ  = 1./(1.27932E+02);  //   # alpha_em^-1(MZ)^MSbar
	double g1    = sqrt(4*M_PI*Alpha_MZ/(1-sinW2)); 
	double g2    = sqrt(4*M_PI*Alpha_MZ/ sinW2); 

	switch(j)
	{
		case 1:
			/* (1,1) */
			for(i=1;i<=3;i++) couphahahk[i]=(v1*lambdas[14]*(ZH[i-1][0])
				+ v2*(lambdas[10]+lambdas[11]+lambdas[12])*(sqrt(3.0)*(ZH[i-1][1])+ZH[i-1][2]));
			for(i=1;i<=3;i++) Vpot1=Vpot1+couphahahk[i]*couphahahk[i]*exp(-Mlight[i]*x);
			Vpot1=Vpot1*(1.0)/(16*M_PI*Mdm[1]*Mdm[1]*x*x);
			for(i=1;i<=3;i++) Vpot2=
				                Vpot2+couphahahk[i]*couphahahk[i]*(-Mlight[i])*exp(-Mlight[i]*x);
			Vpot2=Vpot2*(-1.0)/(16*M_PI*Mdm[1]*Mdm[1]*x);
			Vpot=Vpot1+Vpot2;
			break;

		case 2:
			/* (1,2) */
			for(i=1;i<=3;i++) couphaAaAk[i]=2*lambdas[12]*v2*(sqrt(3.0)*(ZA[i-1][1])+ZA[i-1][2]);
			couphaAaZ = 0.5*(g1*sw+g2*cw);
			for(i=1;i<=3;i++) Vpot1=Vpot1+couphaAaAk[i]*couphaAaAk[i]*exp(-Mlight[i+3]*x);
			Vpot1=Vpot1*(1.0)/(16*M_PI*Mdm[1]*Mdm[1]*x*x);
			for(i=1;i<=3;i++) Vpot2=
				          Vpot2+couphaAaAk[i]*couphaAaAk[i]*(-Mlight[i+3])*exp(-Mlight[i+3]*x);
			Vpot2=Vpot2*(-1.0)/(16*M_PI*Mdm[1]*Mdm[1]*x);
			Vpot=Vpot1+Vpot2+(g2*g2)/(16*M_PI*cw*cw*x*x) * exp(-Mlight[4]*x) +
			                (-g2*g2)/(16*M_PI*cw*cw*x) * (-Mlight[4]) * exp(-Mlight[4]*x);
			break;

		case 3:
			/* (1,3) */
			for(i=1;i<=3;i++) couphaHapmHw[i]=0.5*(lambdas[14]*v1*(ZA[i-1][0]) + 
				              (2*lambdas[12]+lambdas[11])*v2*(sqrt(3.0)*(ZA[i-1][1])+ZA[i-1][2]));
			couphaHapmW = 0.5*g2;
			for(i=1;i<=3;i++) Vpot1=Vpot1+couphaHapmHw[i]*couphaHapmHw[i]*exp(-Mlight[i+6]*x);
			Vpot1=Vpot1*(1.0)/(16*M_PI*Mdm[1]*Mdm[1]*x*x);
			for(i=1;i<=3;i++) Vpot2=Vpot2+couphaHapmHw[i]*couphaHapmHw[i]*(-Mlight[i+6])*exp(-Mlight[i+6]*x);
			Vpot2=Vpot2*(-1.0)/(16*M_PI*Mdm[1]*Mdm[1]*x);
			Vpot= Vpot1+Vpot2+(g2*g2)/(16*M_PI*x*x) * exp(-Mlight[7]*x) +
			                 (-g2*g2)/(16*M_PI*x) * (-Mlight[7]) * exp(-Mlight[7]*x);
			Vpot=Vpot*sqrt(2.0); /*  ---  2-body basis factor  ---  */
			break;

		case 4:
			/* (2,1) */
			for(i=1;i<=3;i++) couphaAaAk[i]=2*lambdas[12]*v2*(sqrt(3.0)*(ZA[i-1][1])+ZA[i-1][2]);
			couphaAaZ = 0.5*(g1*sw+g2*cw);
			for(i=1;i<=3;i++) Vpot1=Vpot1+couphaAaAk[i]*couphaAaAk[i]*exp(-Mlight[i+3]*x);
			Vpot1=Vpot1*(1.0)/(16*M_PI*Mdm[1]*Mdm[1]*x*x);
			for(i=1;i<=3;i++) Vpot2=
				          Vpot2+couphaAaAk[i]*couphaAaAk[i]*(-Mlight[i+3])*exp(-Mlight[i+3]*x);
			Vpot2=Vpot2*(-1.0)/(16*M_PI*Mdm[1]*Mdm[1]*x);
			Vpot=Vpot1+Vpot2+(g2*g2)/(16*M_PI*cw*cw*x*x) * exp(-Mlight[4]*x) +
			                (-g2*g2)/(16*M_PI*cw*cw*x) * (-Mlight[4]) * exp(-Mlight[4]*x);
			break;

		case 5:
			/* (2,2) */
			for(i=1;i<=3;i++) coupAaAahk[i]=(v1*lambdas[14]*(ZH[i-1][0])
				+ v2*(lambdas[10]+lambdas[11]-2.0*lambdas[12])*(sqrt(3.0)*ZH[i-1][1]+ZH[i-1][2]));
			for(i=1;i<=3;i++) Vpot1=Vpot1+coupAaAahk[i]*coupAaAahk[i]*exp(-Mlight[i]*x);
			Vpot1=Vpot1*(1.0)/(16*M_PI*Mdm[1]*Mdm[1]*x*x);
			for(i=1;i<=3;i++) Vpot2=Vpot2+coupAaAahk[i]*coupAaAahk[i]*(-Mlight[i])*exp(-Mlight[i]*x);
			Vpot2=Vpot2*(-1.0)/(16*M_PI*Mdm[1]*Mdm[1]*x);
			Vpot=Vpot1+Vpot2;
			break;

		case 6:
			/* (2,3) */
			for(i=1;i<=3;i++) coupAaHapmHw[i]=0.5*(lambdas[14]*v1*ZA[i-1][0] + 
				                (lambdas[11]-2*lambdas[12])*v2*(sqrt(3.0)*ZA[i-1][1]+ZA[i-1][2]));
			coupAaHapmW=0.5*g2;
			for(i=1;i<=3;i++) Vpot1=Vpot1+coupAaHapmHw[i]*coupAaHapmHw[i]*exp(-Mlight[i+6]*x);
			Vpot1=Vpot1*(1.0)/(16*M_PI*Mdm[1]*Mdm[1]*x*x);
			for(i=1;i<=3;i++) Vpot2=Vpot2+coupAaHapmHw[i]*coupAaHapmHw[i]*(-Mlight[i+6])*exp(-Mlight[i+6]*x);
			Vpot2=Vpot2*(-1.0)/(16*M_PI*Mdm[1]*Mdm[1]*x);
			Vpot=Vpot1+Vpot2+(g2*g2)/(16*M_PI*x*x) * exp(-Mlight[7]*x)
			               +(-g2*g2)/(16*M_PI*x) * (-Mlight[7]) * exp(-Mlight[7]*x);
			Vpot=Vpot*sqrt(2.0); /*  ---  2-body basis factor  ---  */
			break;

		case 7:
			/* (3,1) */
			for(i=1;i<=3;i++) couphaHapmHw[i]=0.5*(lambdas[14]*v1*ZA[i-1][0] + 
				              (2*lambdas[12]+lambdas[11])*v2*(sqrt(3.0)*ZA[i-1][1]+ZA[i-1][2]));
			couphaHapmW = 0.5*g2;
			for(i=1;i<=3;i++) Vpot1=Vpot1+couphaHapmHw[i]*couphaHapmHw[i]*exp(-Mlight[i+6]*x);
			Vpot1=Vpot1*(1.0)/(16*M_PI*Mdm[1]*Mdm[1]*x*x);
			for(i=1;i<=3;i++) Vpot2=Vpot2+couphaHapmHw[i]*couphaHapmHw[i]*(-Mlight[i+6])*exp(-Mlight[i+6]*x);
			Vpot2=Vpot2*(-1.0)/(16*M_PI*Mdm[1]*Mdm[1]*x);
			Vpot= Vpot1+Vpot2+(g2*g2)/(16*M_PI*x*x) * exp(-Mlight[7]*x) +
			                 (-g2*g2)/(16*M_PI*x) * (-Mlight[7]) * exp(-Mlight[7]*x);
			Vpot=Vpot*sqrt(2.0); /*  ---  2-body basis factor  ---  */
			break;

		case 8:
			/* (2,3) */
			for(i=1;i<=3;i++) coupAaHapmHw[i]=0.5*(lambdas[14]*v1*ZA[i-1][0] + 
				                (lambdas[11]-2*lambdas[12])*v2*(sqrt(3.0)*ZA[i-1][1]+ZA[i-1][2]));
			coupAaHapmW=0.5*g2;
			for(i=1;i<=3;i++) Vpot1=Vpot1+coupAaHapmHw[i]*coupAaHapmHw[i]*exp(-Mlight[i+6]*x);
			Vpot1=Vpot1*(1.0)/(16*M_PI*Mdm[1]*Mdm[1]*x*x);
			for(i=1;i<=3;i++) Vpot2=Vpot2+coupAaHapmHw[i]*coupAaHapmHw[i]*(-Mlight[i+6])*exp(-Mlight[i+6]*x);
			Vpot2=Vpot2*(-1.0)/(16*M_PI*Mdm[1]*Mdm[1]*x);
			Vpot=Vpot1+Vpot2+(g2*g2)/(16*M_PI*x*x) * exp(-Mlight[7]*x)
			               +(-g2*g2)/(16*M_PI*x) * (-Mlight[7]) * exp(-Mlight[7]*x);
			Vpot=Vpot*sqrt(2.0); /*  ---  2-body basis factor  ---  */
			break;

		case 9:
			/* (3,3) */
			for(i=1;i<=3;i++) coupHapmHapmhk[i]=lambdas[10]*v2*(sqrt(3.0)*ZH[i-1][1]+ZH[i-1][2]);
			for(i=1;i<=3;i++) Vpot1=Vpot1+coupHapmHapmhk[i]*coupHapmHapmhk[i]*exp(-Mlight[i]*x);
			Vpot1=Vpot1*(1.0)/(16*M_PI*Mdm[1]*Mdm[1]*x*x);
			for(i=1;i<=3;i++) Vpot2=Vpot2+coupHapmHapmhk[i]*coupHapmHapmhk[i]*(-Mlight[i])*exp(-Mlight[i]*x);
			Vpot2=Vpot2*(-1.0)/(16*M_PI*Mdm[1]*Mdm[1]*x);
			Vpot=Vpot1+Vpot2;
			Vpot1=(g2*g2)/(4*M_PI*x*x)*
			            (sinW2+(1-2*sinW2)*(1-2*sinW2)/(4*cw*cw)*exp(-Mlight[4]*x));
			Vpot2=(-g2*g2)/(4*M_PI*x)*
			            ((1-2*sinW2)*(1-2*sinW2)/(4*cw*cw)*(-Mlight[4])*exp(-Mlight[4]*x));
			Vpot=Vpot + Vpot1+Vpot2;
			break;

	}
	return Vpot;
}

void derivs(double x, double y[], double dydx[], double lambdas[], double **ZH, double **ZA, 
	          double Mdm[], double Mlight[], double theta)
{
	double vrel=0.002;
	nrhs++;
	dydx[1]  = - y[1]*y[1]   - y[2]*y[4]   - y[3]*y[7]
	           + y[10]*y[10] + y[11]*y[13] + y[12]*y[16]
	           - Mdm[1]*( 0.25*Mdm[1]*vrel*vrel - VNRij(1, x, lambdas, ZH, ZA, Mdm, Mlight, theta) );

	dydx[2]  = - y[1]*y[2]   - y[2]*y[5]   - y[3]*y[8]
	           + y[10]*y[11] + y[11]*y[14] + y[12]*y[17]
	           - Mdm[1]*( - VNRij(2, x, lambdas, ZH, ZA, Mdm, Mlight, theta) );

	dydx[3]  = - y[1]*y[3]   - y[2]*y[6]   - y[3]*y[9]
	           + y[10]*y[12] + y[11]*y[15] + y[12]*y[18]
	           - Mdm[1]*( - VNRij(3, x, lambdas, ZH, ZA, Mdm, Mlight, theta) );

	dydx[4]  = - y[4]*y[1]   - y[5]*y[4]   - y[6]*y[7]
	           + y[13]*y[10] + y[14]*y[13] + y[15]*y[16]
	           - Mdm[1]*( - VNRij(4, x, lambdas, ZH, ZA, Mdm, Mlight, theta) );

	dydx[5]  = - y[4]*y[2]   - y[5]*y[5]   - y[6]*y[8]
	           + y[13]*y[11] + y[14]*y[14] + y[15]*y[17]
	           - Mdm[1]*( 0.25*Mdm[1]*vrel*vrel - VNRij(5, x, lambdas, ZH, ZA, Mdm, Mlight, theta) );

	dydx[6]  = - y[4]*y[3]   - y[5]*y[6]   - y[6]*y[9]
	           + y[13]*y[12] + y[14]*y[15] + y[15]*y[18]
	           - Mdm[1]*( - VNRij(6, x, lambdas, ZH, ZA, Mdm, Mlight, theta) );

	dydx[7]  = - y[7]*y[1]   - y[8]*y[4]   - y[9]*y[7]
	           + y[16]*y[10] + y[17]*y[13] + y[18]*y[16]
	           - Mdm[1]*( - VNRij(7, x, lambdas, ZH, ZA, Mdm, Mlight, theta) );

	dydx[8]  = - y[7]*y[2]   - y[8]*y[5]   - y[9]*y[8]
	           + y[16]*y[11] + y[17]*y[14] + y[18]*y[17]
	           - Mdm[1]*( - VNRij(8, x, lambdas, ZH, ZA, Mdm, Mlight, theta) );

	dydx[9]  = - y[7]*y[3]   - y[8]*y[6]   - y[9]*y[9]
	           + y[16]*y[12] + y[17]*y[15] + y[18]*y[18]
	           - Mdm[1]*( 0.25*Mdm[1]*vrel*vrel - VNRij(9, x, lambdas, ZH, ZA, Mdm, Mlight, theta) );

	dydx[10] = - y[1]*y[10]  - y[2]*y[13]  - y[3]*y[16]
	           - y[10]*y[1]  - y[11]*y[4]  - y[12]*y[7];

	dydx[11] = - y[1]*y[11]  - y[2]*y[14]  - y[3]*y[17]
	           - y[10]*y[2]  - y[11]*y[5]  - y[12]*y[8];

	dydx[12] = - y[1]*y[12]  - y[2]*y[15]  - y[3]*y[18]
	           - y[10]*y[3]  - y[11]*y[6]  - y[12]*y[9];

	dydx[13] = - y[4]*y[10]  - y[5]*y[13]  - y[6]*y[16]
	           - y[13]*y[1]  - y[14]*y[4]  - y[15]*y[7];

	dydx[14] = - y[4]*y[11]  - y[5]*y[14]  - y[6]*y[17]
	           - y[13]*y[2]  - y[14]*y[5]  - y[15]*y[8];

	dydx[15] = - y[4]*y[12]  - y[5]*y[15]  - y[6]*y[18]
	           - y[13]*y[3]  - y[14]*y[6]  - y[15]*y[9];

	dydx[16] = - y[7]*y[10]  - y[8]*y[13]  - y[9]*y[16]
	           - y[16]*y[1]  - y[17]*y[4]  - y[18]*y[7];

	dydx[17] = - y[7]*y[11]  - y[8]*y[14]  - y[9]*y[17]
	           - y[16]*y[2]  - y[17]*y[5]  - y[18]*y[8];

	dydx[18] = - y[7]*y[12]  - y[8]*y[15]  - y[9]*y[18]
	           - y[16]*y[3]  - y[17]*y[6]  - y[18]*y[9];
}


void jacobn(double x, double y[], double dfdx[], double **dfdy, int n, 
	          double lambdas[], double **ZH, double **ZA, double Mdm[], double Mlight[], double theta)
{
	int i;

	for (i=1;i<=n;i++) dfdx[i]=Mdm[1]*diffVNRij(i, x, lambdas, ZH, ZA, Mdm, Mlight, theta);

dfdy[1][1] = -2*y[1];
dfdy[1][2] = -y[4];
dfdy[1][3] = -y[7];
dfdy[1][4] = -y[2];
dfdy[1][5] = 0;
dfdy[1][6] = 0;
dfdy[1][7] = -y[3];
dfdy[1][8] = 0;
dfdy[1][9] = 0;
dfdy[1][10] = 2*y[10];
dfdy[1][11] = y[13];
dfdy[1][12] = y[16];
dfdy[1][13] = y[11];
dfdy[1][14] = 0;
dfdy[1][15] = 0;
dfdy[1][16] = y[12];
dfdy[1][17] = 0;
dfdy[1][18] = 0;
dfdy[2][1] = -y[2];
dfdy[2][2] = -y[1] - y[5];
dfdy[2][3] = -y[8];
dfdy[2][4] = 0;
dfdy[2][5] = -y[2];
dfdy[2][6] = 0;
dfdy[2][7] = 0;
dfdy[2][8] = -y[3];
dfdy[2][9] = 0;
dfdy[2][10] = y[11];
dfdy[2][11] = y[10] + y[14];
dfdy[2][12] = y[17];
dfdy[2][13] = 0;
dfdy[2][14] = y[11];
dfdy[2][15] = 0;
dfdy[2][16] = 0;
dfdy[2][17] = y[12];
dfdy[2][18] = 0;
dfdy[3][1] = -y[3];
dfdy[3][2] = -y[6];
dfdy[3][3] = -y[1] - y[9];
dfdy[3][4] = 0;
dfdy[3][5] = 0;
dfdy[3][6] = -y[2];
dfdy[3][7] = 0;
dfdy[3][8] = 0;
dfdy[3][9] = -y[3];
dfdy[3][10] = y[12];
dfdy[3][11] = y[15];
dfdy[3][12] = y[10] + y[18];
dfdy[3][13] = 0;
dfdy[3][14] = 0;
dfdy[3][15] = y[11];
dfdy[3][16] = 0;
dfdy[3][17] = 0;
dfdy[3][18] = y[12];
dfdy[4][1] = -y[4];
dfdy[4][2] = 0;
dfdy[4][3] = 0;
dfdy[4][4] = -y[1] - y[5];
dfdy[4][5] = -y[4];
dfdy[4][6] = -y[7];
dfdy[4][7] = -y[6];
dfdy[4][8] = 0;
dfdy[4][9] = 0;
dfdy[4][10] = y[13];
dfdy[4][11] = 0;
dfdy[4][12] = 0;
dfdy[4][13] = y[10] + y[14];
dfdy[4][14] = y[13];
dfdy[4][15] = y[16];
dfdy[4][16] = y[15];
dfdy[4][17] = 0;
dfdy[4][18] = 0;
dfdy[5][1] = 0;
dfdy[5][2] = -y[4];
dfdy[5][3] = 0;
dfdy[5][4] = -y[2];
dfdy[5][5] = -2*y[5];
dfdy[5][6] = -y[8];
dfdy[5][7] = 0;
dfdy[5][8] = -y[6];
dfdy[5][9] = 0;
dfdy[5][10] = 0;
dfdy[5][11] = y[13];
dfdy[5][12] = 0;
dfdy[5][13] = y[11];
dfdy[5][14] = 2*y[14];
dfdy[5][15] = y[17];
dfdy[5][16] = 0;
dfdy[5][17] = y[15];
dfdy[5][18] = 0;
dfdy[6][1] = 0;
dfdy[6][2] = 0;
dfdy[6][3] = -y[4];
dfdy[6][4] = -y[3];
dfdy[6][5] = -y[6];
dfdy[6][6] = -y[5] - y[9];
dfdy[6][7] = 0;
dfdy[6][8] = 0;
dfdy[6][9] = -y[6];
dfdy[6][10] = 0;
dfdy[6][11] = 0;
dfdy[6][12] = y[13];
dfdy[6][13] = y[12];
dfdy[6][14] = y[15];
dfdy[6][15] = y[14] + y[18];
dfdy[6][16] = 0;
dfdy[6][17] = 0;
dfdy[6][18] = y[15];
dfdy[7][1] = -y[7];
dfdy[7][2] = 0;
dfdy[7][3] = 0;
dfdy[7][4] = -y[8];
dfdy[7][5] = 0;
dfdy[7][6] = 0;
dfdy[7][7] = -y[1] - y[9];
dfdy[7][8] = -y[4];
dfdy[7][9] = -y[7];
dfdy[7][10] = y[16];
dfdy[7][11] = 0;
dfdy[7][12] = 0;
dfdy[7][13] = y[17];
dfdy[7][14] = 0;
dfdy[7][15] = 0;
dfdy[7][16] = y[10] + y[18];
dfdy[7][17] = y[13];
dfdy[7][18] = y[16];
dfdy[8][1] = 0;
dfdy[8][2] = -y[7];
dfdy[8][3] = 0;
dfdy[8][4] = 0;
dfdy[8][5] = -y[8];
dfdy[8][6] = 0;
dfdy[8][7] = -y[2];
dfdy[8][8] = -y[5] - y[9];
dfdy[8][9] = -y[8];
dfdy[8][10] = 0;
dfdy[8][11] = y[16];
dfdy[8][12] = 0;
dfdy[8][13] = 0;
dfdy[8][14] = y[17];
dfdy[8][15] = 0;
dfdy[8][16] = y[11];
dfdy[8][17] = y[14] + y[18];
dfdy[8][18] = y[17];
dfdy[9][1] = 0;
dfdy[9][2] = 0;
dfdy[9][3] = -y[7];
dfdy[9][4] = 0;
dfdy[9][5] = 0;
dfdy[9][6] = -y[8];
dfdy[9][7] = -y[3];
dfdy[9][8] = -y[6];
dfdy[9][9] = -2*y[9];
dfdy[9][10] = 0;
dfdy[9][11] = 0;
dfdy[9][12] = y[16];
dfdy[9][13] = 0;
dfdy[9][14] = 0;
dfdy[9][15] = y[17];
dfdy[9][16] = y[12];
dfdy[9][17] = y[15];
dfdy[9][18] = 2*y[18];
dfdy[10][1] = -2*y[10];
dfdy[10][2] = -y[13];
dfdy[10][3] = -y[16];
dfdy[10][4] = -y[11];
dfdy[10][5] = 0;
dfdy[10][6] = 0;
dfdy[10][7] = -y[12];
dfdy[10][8] = 0;
dfdy[10][9] = 0;
dfdy[10][10] = -2*y[1];
dfdy[10][11] = -y[4];
dfdy[10][12] = -y[7];
dfdy[10][13] = -y[2];
dfdy[10][14] = 0;
dfdy[10][15] = 0;
dfdy[10][16] = -y[3];
dfdy[10][17] = 0;
dfdy[10][18] = 0;
dfdy[11][1] = -y[11];
dfdy[11][2] = -y[10] - y[14];
dfdy[11][3] = -y[17];
dfdy[11][4] = 0;
dfdy[11][5] = -y[11];
dfdy[11][6] = 0;
dfdy[11][7] = 0;
dfdy[11][8] = -y[12];
dfdy[11][9] = 0;
dfdy[11][10] = -y[2];
dfdy[11][11] = -y[1] - y[5];
dfdy[11][12] = -y[8];
dfdy[11][13] = 0;
dfdy[11][14] = -y[2];
dfdy[11][15] = 0;
dfdy[11][16] = 0;
dfdy[11][17] = -y[3];
dfdy[11][18] = 0;
dfdy[12][1] = -y[12];
dfdy[12][2] = -y[15];
dfdy[12][3] = -y[10] - y[18];
dfdy[12][4] = 0;
dfdy[12][5] = 0;
dfdy[12][6] = -y[11];
dfdy[12][7] = 0;
dfdy[12][8] = 0;
dfdy[12][9] = -y[12];
dfdy[12][10] = -y[3];
dfdy[12][11] = -y[6];
dfdy[12][12] = -y[1] - y[9];
dfdy[12][13] = 0;
dfdy[12][14] = 0;
dfdy[12][15] = -y[2];
dfdy[12][16] = 0;
dfdy[12][17] = 0;
dfdy[12][18] = -y[3];
dfdy[13][1] = -y[13];
dfdy[13][2] = 0;
dfdy[13][3] = 0;
dfdy[13][4] = -y[10] - y[14];
dfdy[13][5] = -y[13];
dfdy[13][6] = -y[16];
dfdy[13][7] = -y[15];
dfdy[13][8] = 0;
dfdy[13][9] = 0;
dfdy[13][10] = -y[4];
dfdy[13][11] = 0;
dfdy[13][12] = 0;
dfdy[13][13] = -y[1] - y[5];
dfdy[13][14] = -y[4];
dfdy[13][15] = -y[7];
dfdy[13][16] = -y[6];
dfdy[13][17] = 0;
dfdy[13][18] = 0;
dfdy[14][1] = 0;
dfdy[14][2] = -y[13];
dfdy[14][3] = 0;
dfdy[14][4] = -y[11];
dfdy[14][5] = -2*y[14];
dfdy[14][6] = -y[17];
dfdy[14][7] = 0;
dfdy[14][8] = -y[15];
dfdy[14][9] = 0;
dfdy[14][10] = 0;
dfdy[14][11] = -y[4];
dfdy[14][12] = 0;
dfdy[14][13] = -y[2];
dfdy[14][14] = -2*y[5];
dfdy[14][15] = -y[8];
dfdy[14][16] = 0;
dfdy[14][17] = -y[6];
dfdy[14][18] = 0;
dfdy[15][1] = 0;
dfdy[15][2] = 0;
dfdy[15][3] = -y[13];
dfdy[15][4] = -y[12];
dfdy[15][5] = -y[15];
dfdy[15][6] = -y[14] - y[18];
dfdy[15][7] = 0;
dfdy[15][8] = 0;
dfdy[15][9] = -y[15];
dfdy[15][10] = 0;
dfdy[15][11] = 0;
dfdy[15][12] = -y[4];
dfdy[15][13] = -y[3];
dfdy[15][14] = -y[6];
dfdy[15][15] = -y[5] - y[9];
dfdy[15][16] = 0;
dfdy[15][17] = 0;
dfdy[15][18] = -y[6];
dfdy[16][1] = -y[16];
dfdy[16][2] = 0;
dfdy[16][3] = 0;
dfdy[16][4] = -y[17];
dfdy[16][5] = 0;
dfdy[16][6] = 0;
dfdy[16][7] = -y[10] - y[18];
dfdy[16][8] = -y[13];
dfdy[16][9] = -y[16];
dfdy[16][10] = -y[7];
dfdy[16][11] = 0;
dfdy[16][12] = 0;
dfdy[16][13] = -y[8];
dfdy[16][14] = 0;
dfdy[16][15] = 0;
dfdy[16][16] = -y[1] - y[9];
dfdy[16][17] = -y[4];
dfdy[16][18] = -y[7];
dfdy[17][1] = 0;
dfdy[17][2] = -y[16];
dfdy[17][3] = 0;
dfdy[17][4] = 0;
dfdy[17][5] = -y[17];
dfdy[17][6] = 0;
dfdy[17][7] = -y[11];
dfdy[17][8] = -y[14] - y[18];
dfdy[17][9] = -y[17];
dfdy[17][10] = 0;
dfdy[17][11] = -y[7];
dfdy[17][12] = 0;
dfdy[17][13] = 0;
dfdy[17][14] = -y[8];
dfdy[17][15] = 0;
dfdy[17][16] = -y[2];
dfdy[17][17] = -y[5] - y[9];
dfdy[17][18] = -y[8];
dfdy[18][1] = 0;
dfdy[18][2] = 0;
dfdy[18][3] = -y[16];
dfdy[18][4] = 0;
dfdy[18][5] = 0;
dfdy[18][6] = -y[17];
dfdy[18][7] = -y[12];
dfdy[18][8] = -y[15];
dfdy[18][9] = -2*y[18];
dfdy[18][10] = 0;
dfdy[18][11] = 0;
dfdy[18][12] = -y[7];
dfdy[18][13] = 0;
dfdy[18][14] = 0;
dfdy[18][15] = -y[8];
dfdy[18][16] = -y[3];
dfdy[18][17] = -y[6];
dfdy[18][18] = -2*y[9];

}


