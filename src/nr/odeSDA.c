
#include <stdio.h>
#include <math.h>
#include <stdbool.h>
#define NRANSI
#include "nrutil.h"
#include "sda.h"
#define MAXSTP 1000000
#define TINY 1.0e-30

#define LSB -1.55787   /* Linear Stability Boundary for this RK method  */
#define SAFETY 0.9

extern int kmax,kount,kountRK,kountStiff;
extern double *xp,**yp,dxsav;

void odeSDA(double ystart[], int nvar, double x1, double x2, double eps, double h1,
	double hmin, int *nok, int *nbad,
	void (*derivs)(double, double [], double [], double [], double **, double **, 
	          double [], double [], double),
	void (*rkqsSDA)(double [], double [], int, double *, double, double, double [],
	double *, double *, void (*)(double, double [], double [], double [], double **, double **, 
	          double [], double [], double), 
	double *, double [], double **, double **, 
	          double [], double [], double),
	void (*stiffSDA)(double [], double [], int, double *, double, double, double [],
	double *, double *, void (*)(double, double [], double [], double [], double **, double **, 
	          double [], double [], double), 
	double *, double [], double **, double **, 
	          double [], double [], double), 
	double lambdas[], double **ZH, double **ZA, 
	          double Mdm[], double Mlight[], double theta)
{
	int nstp,i,kontador;
	double xsav,x,hnext,hdid,h;
	double *yscal,*y,*dydx;

	bool flagStiff;
	double lambda;

	yscal=dvector(1,nvar);
	y=dvector(1,nvar);
	dydx=dvector(1,nvar);
	x=x1;
	h=SIGN(h1,x2-x1);
	*nok = (*nbad) = kount = 0;
	for (i=1;i<=nvar;i++) y[i]=ystart[i];
	if (kmax > 0) xsav=x-dxsav*2.0;
	/*  set flag to RK initially  */
	/*flagStiff = 0;*/
	flagStiff = 1;
	kontador = 0;
	kountRK=0;
	kountStiff=0;
	lambda=0;
	/*  ---  */
	for (nstp=1;nstp<=MAXSTP;nstp++) {
		(*derivs)(x,y,dydx, lambdas, ZH, ZA, Mdm, Mlight, theta);
		for (i=1;i<=nvar;i++)
			yscal[i]=fabs(y[i])+fabs(dydx[i]*h)+TINY;
		if (kmax > 0 && kount < kmax-1 && fabs(x-xsav) > fabs(dxsav)) {
			xp[++kount]=x;
			for (i=1;i<=nvar;i++) yp[i][kount]=y[i];
			yp[i][kount]=h*lambda;
			yp[i+1][kount]=SAFETY*fabs(LSB);
			xsav=x;
		}
		if ((x+h-x2)*(x+h-x1) > 0.0) h=x2-x;
		/* Initially RK later switch between RK and stiff as appropiate  */
		if(flagStiff){
			kountStiff++;
			(*stiffSDA)(y,dydx,nvar,&x,h,eps,yscal,&hdid,&hnext,derivs,&lambda, 
				          lambdas, ZH, ZA, Mdm, Mlight, theta);
		} else{
			kountRK++;
			(*rkqsSDA)(y,dydx,nvar,&x,h,eps,yscal,&hdid,&hnext,derivs,&lambda,
				          lambdas, ZH, ZA, Mdm, Mlight, theta);
		}
		/*  ---  */
		if(Mdm[0]>0) return;
		/*  ---  */
		if (hdid == h) ++(*nok); else ++(*nbad);
		if ((x-x2)*(x2-x1) >= 0.0) {
			for (i=1;i<=nvar;i++) ystart[i]=y[i];
			if (kmax) {
				xp[++kount]=x;
				for (i=1;i<=nvar;i++) yp[i][kount]=y[i];
			}
			free_dvector(dydx,1,nvar);
			free_dvector(y,1,nvar);
			free_dvector(yscal,1,nvar);
			return;
		}
		if (fabs(hnext) <= hmin){
			Mdm[0]=1.0;
			return;
			/* nrerror("Step size too small in odeint"); */
		} 
		h=hnext;
		/*  decide if switching algoritms is necessary  */
		/*flagStiff = (fabs(h*lambda)<SAFETY*fabs(LSB)) ? 1:0;*/
		
		/*  ---  */
	}
	Mdm[0]=1.0;
	return;
	/* nrerror("Too many steps in routine odeint"); */
}
#undef MAXSTP
#undef TINY
#undef NRANSI
#undef SAFETY
#undef LSB

