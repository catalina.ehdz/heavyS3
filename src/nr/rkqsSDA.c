
#include <math.h>
#define NRANSI
#include "nrutil.h"
#include "sda.h"
#define SAFETY 0.9
#define PGROW -0.2
#define PSHRNK -0.25
#define ERRCON 1.89e-4

void rkqsSDA(double y[], double dydx[], int n, double *x, double htry, double eps,
	double yscal[], double *hdid, double *hnext,
	void (*derivs)(double, double [], double [], double [], double **, double **, 
	          double [], double [], double), 
	double *lambda, double lambdas[], double **ZH, double **ZA, 
	          double Mdm[], double Mlight[], double theta)
{
	void rkckSDA(double y[], double dydx[], int n, double x, double h,
		double yout[], double yerr[], 
		void (*derivs)(double, double [], double [], double [], double **, double **, 
	          double [], double [], double), 
		double *lambda, 
		double lambdas[], double **ZH, double **ZA, double Mdm[], double Mlight[], double theta);
	int i;
	double errmax,h,htemp,xnew,*yerr,*ytemp;

	yerr=dvector(1,n);
	ytemp=dvector(1,n);
	h=htry;
	for (;;) {
		rkckSDA(y,dydx,n,*x,h,ytemp,yerr,derivs,lambda, lambdas, ZH, ZA, Mdm, Mlight, theta);
		errmax=0.0;
		for (i=1;i<=n;i++) errmax=DMAX(errmax,fabs(yerr[i]/yscal[i]));
		errmax /= eps;
		if (errmax <= 1.0) break;
		htemp=SAFETY*h*pow(errmax,PSHRNK);
		h=(h >= 0.0 ? DMAX(htemp,0.1*h) : DMIN(htemp,0.1*h));
		xnew=(*x)+h;
		if (xnew == *x) nrerror("stepsize underflow in rkqs");
	}
	if (errmax > ERRCON) *hnext=SAFETY*h*pow(errmax,PGROW);
	else *hnext=5.0*h;
	*x += (*hdid=h);
	for (i=1;i<=n;i++) y[i]=ytemp[i];
	free_dvector(ytemp,1,n);
	free_dvector(yerr,1,n);
}
#undef SAFETY
#undef PGROW
#undef PSHRNK
#undef ERRCON
#undef NRANSI

