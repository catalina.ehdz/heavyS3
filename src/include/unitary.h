
#include <complex.h> 
/* ------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------- */
/* -- Module for computing Unitarity constraints for the Dark S3 Model    -- */
/* -- This code is a port to C of the SARAH-SPHENO generated Fortran code -- */
/* ------------------------------------------------------------------------- */
/* ------------------------------------------------------------------------- */
 
/* Start module DarkS3 */

       /* ----------------------------------------------------------------------------------------------------- */

double complex  Kaehler(double *mya, double *myb, double *myc);
 /* End Function Kaehler */ 


       /* ----------------------------------------------------------------------------------------------------- */


double   CheckTpole(double *mym1, double *mym2, double *mym3, double *mym4, double *mymP); 
  /* End Function CheckTpole */ 
  

       /* ----------------------------------------------------------------------------------------------------- */
 
double   CheckUpole(double *mym1, double *mym2, double *mym3, double *mym4, double *mymP); 
 /* End Function CheckUpole */ 
  

       /* ----------------------------------------------------------------------------------------------------- */
 
double complex  Schannel(double *mym1, double *mym2, double *mym3, double *mym4, double *mymP, double *mys, double complex c1, double complex c2);
 /* End Function Schannel */ 
  

       /* ----------------------------------------------------------------------------------------------------- */
 
double complex Uchannel(double *mym1r, double *mym2r, double *mym3r, double *mym4r, double *mymPr, double *mys, double complex c1, double complex c2);
 /* End Function Uchannel */ 
  

       /* ----------------------------------------------------------------------------------------------------- */
  
double complex Tchannel(double *mym1r, double *mym2r, double *mym3r, double *mym4r, double *mymPr, double *mys, double complex c1, double complex c2);
 /* End Function Tchannel */ 
  



void CouplingAhAhHhSMT(const int* mygt1,const int* mygt2,const int* mygt3,const double* mylam1, const double* mylam3, const double* mylam4, const double* mylam2, const double* mylam7, const double* mylam6, const double* mylam5, const double* mylam8, const double* myv1, const double* myv2, double **ZH, double **ZA, double complex* myres);
/* End Subroutine CouplingAhAhHhSMT */ 


       /* ----------------------------------------------------------------------------------------------------- */


void CouplingAhhhasigaT(const int* mygt1,const double* mylam12, const double* myv2, double **ZA, double complex* myres);
/* End Subroutine CouplingAhhhasigaT */ 


       /* ----------------------------------------------------------------------------------------------------- */


void CouplingAhHpcHpT(const int* mygt1,const int* mygt2,const int* mygt3,const double* mylam2, const double* mylam7, const double* mylam6, const double* myv1, const double* myv2, double **ZA, double **ZP, double complex* myres);
/* End Subroutine CouplingAhHpcHpT */ 


       /* ----------------------------------------------------------------------------------------------------- */


void CouplingHaphhacHpT(const int* mygt3,const double* mylam12, const double* mylam11, const double* mylam14, const double* myv1, const double* myv2, double **ZP, double complex* myres);
/* End Subroutine CouplingHaphhacHpT */ 


       /* ----------------------------------------------------------------------------------------------------- */


void CouplingHapHhSMcHapT(const int* mygt2,const double* mylam10, const double* myv2, double **ZH, double complex* myres);
/* End Subroutine CouplingHapHhSMcHapT */ 


       /* ----------------------------------------------------------------------------------------------------- */


void CouplingHapsigacHpT(const int* mygt3,const double* mylam12, const double* mylam11, const double* mylam14, const double* myv1, const double* myv2, double **ZP, double complex* myres);
/* End Subroutine CouplingHapsigacHpT */ 


       /* ----------------------------------------------------------------------------------------------------- */


void CouplinghhahhaHhSMT(const int* mygt3,const double* mylam12, const double* mylam11, const double* mylam10, const double* mylam14, const double* myv1, const double* myv2, double **ZH, double complex* myres);
/* End Subroutine CouplinghhahhaHhSMT */ 


       /* ----------------------------------------------------------------------------------------------------- */


void CouplinghhaHpcHapT(const int* mygt2,const double* mylam12, const double* mylam11, const double* mylam14, const double* myv1, const double* myv2, double **ZP, double complex* myres);
/* End Subroutine CouplinghhaHpcHapT */ 


       /* ----------------------------------------------------------------------------------------------------- */


void CouplingHhSMHhSMHhSMT(const int* mygt1,const int* mygt2,const int* mygt3,const double* mylam1, const double* mylam3, const double* mylam4, const double* mylam7, const double* mylam6, const double* mylam5, const double* mylam8, const double* myv1, const double* myv2, double **ZH, double complex* myres);
/* End Subroutine CouplingHhSMHhSMHhSMT */ 


       /* ----------------------------------------------------------------------------------------------------- */


void CouplingHhSMHpcHpT(const int* mygt1,const int* mygt2,const int* mygt3,const double* mylam1, const double* mylam3, const double* mylam4, const double* mylam7, const double* mylam6, const double* mylam5, const double* mylam8, const double* myv1, const double* myv2, double **ZH, double **ZP, double complex* myres);
/* End Subroutine CouplingHhSMHpcHpT */ 


       /* ----------------------------------------------------------------------------------------------------- */


void CouplingHhSMsigasigaT(const int* mygt1,const double* mylam12, const double* mylam11, const double* mylam10, const double* mylam14, const double* myv1, const double* myv2, double **ZH, double complex* myres);
/* End Subroutine CouplingHhSMsigasigaT */ 


       /* ----------------------------------------------------------------------------------------------------- */


void CouplingHpsigacHapT(const int* mygt1,const double* mylam12, const double* mylam11, const double* mylam14, const double* myv1, const double* myv2, double **ZP, double complex* myres);
/* End Subroutine CouplingHpsigacHapT */ 


       /* ----------------------------------------------------------------------------------------------------- */


void CouplingAhAhAhAhT(const int* mygt1,const int* mygt2,const int* mygt3,const int* mygt4,const double* mylam1, const double* mylam3, const double* mylam4, const double* mylam7, const double* mylam6, const double* mylam5, const double* mylam8, double **ZA, double complex* myres);
/* End Subroutine CouplingAhAhAhAhT */ 


       /* ----------------------------------------------------------------------------------------------------- */


void CouplingAhAhHapcHapT(const int* mygt1,const int* mygt2,const double* mylam10, double **ZA, double complex* myres);
/* End Subroutine CouplingAhAhHapcHapT */ 


       /* ----------------------------------------------------------------------------------------------------- */


void CouplingAhAhhhahhaT(const int* mygt1,const int* mygt2,const double* mylam12, const double* mylam11, const double* mylam10, const double* mylam14, double **ZA, double complex* myres);
/* End Subroutine CouplingAhAhhhahhaT */ 


       /* ----------------------------------------------------------------------------------------------------- */


void CouplingAhAhHhSMHhSMT(const int* mygt1,const int* mygt2,const int* mygt3,const int* mygt4,const double* mylam1, const double* mylam3, const double* mylam4, const double* mylam2, const double* mylam7, const double* mylam6, const double* mylam5, const double* mylam8, double **ZH, double **ZA, double complex* myres);
/* End Subroutine CouplingAhAhHhSMHhSMT */ 


       /* ----------------------------------------------------------------------------------------------------- */


void CouplingAhAhHpcHpT(const int* mygt1,const int* mygt2,const int* mygt3,const int* mygt4,const double* mylam1, const double* mylam3, const double* mylam4, const double* mylam7, const double* mylam6, const double* mylam5, const double* mylam8, double **ZA, double **ZP, double complex* myres);
/* End Subroutine CouplingAhAhHpcHpT */ 


       /* ----------------------------------------------------------------------------------------------------- */


void CouplingAhAhsigasigaT(const int* mygt1,const int* mygt2,const double* mylam12, const double* mylam11, const double* mylam10, const double* mylam14, double **ZA, double complex* myres);
/* End Subroutine CouplingAhAhsigasigaT */ 


       /* ----------------------------------------------------------------------------------------------------- */


void CouplingAhHaphhacHpT(const int* mygt1,const int* mygt4,const double* mylam12, const double* mylam11, const double* mylam14, double **ZA, double **ZP, double complex* myres);
/* End Subroutine CouplingAhHaphhacHpT */ 


       /* ----------------------------------------------------------------------------------------------------- */


void CouplingAhHapsigacHpT(const int* mygt1,const int* mygt4,const double* mylam12, const double* mylam11, const double* mylam14, double **ZA, double **ZP, double complex* myres);
/* End Subroutine CouplingAhHapsigacHpT */ 


       /* ----------------------------------------------------------------------------------------------------- */


void CouplingAhhhaHhSMsigaT(const int* mygt1,const int* mygt3,const double* mylam12, double **ZH, double **ZA, double complex* myres);
/* End Subroutine CouplingAhhhaHhSMsigaT */ 


       /* ----------------------------------------------------------------------------------------------------- */


void CouplingAhhhaHpcHapT(const int* mygt1,const int* mygt3,const double* mylam12, const double* mylam11, const double* mylam14, double **ZA, double **ZP, double complex* myres);
/* End Subroutine CouplingAhhhaHpcHapT */ 


       /* ----------------------------------------------------------------------------------------------------- */


void CouplingAhHhSMHpcHpT(const int* mygt1,const int* mygt2,const int* mygt3,const int* mygt4,const double* mylam2, const double* mylam7, const double* mylam6, double **ZH, double **ZA, double **ZP, double complex* myres);
/* End Subroutine CouplingAhHhSMHpcHpT */ 


       /* ----------------------------------------------------------------------------------------------------- */


void CouplingAhHpsigacHapT(const int* mygt1,const int* mygt2,const double* mylam12, const double* mylam11, const double* mylam14, double **ZA, double **ZP, double complex* myres);
/* End Subroutine CouplingAhHpsigacHapT */ 


       /* ----------------------------------------------------------------------------------------------------- */


void CouplingHapHapcHapcHapT(const double* mylam13, double complex* myres);
/* End Subroutine CouplingHapHapcHapcHapT */ 


       /* ----------------------------------------------------------------------------------------------------- */


void CouplingHapHapcHpcHpT(const int* mygt3,const int* mygt4,const double* mylam12, double **ZP, double complex* myres);
/* End Subroutine CouplingHapHapcHpcHpT */ 


       /* ----------------------------------------------------------------------------------------------------- */


void CouplingHaphhahhacHapT(const double* mylam13, double complex* myres);
/* End Subroutine CouplingHaphhahhacHapT */ 


       /* ----------------------------------------------------------------------------------------------------- */


void CouplingHaphhaHhSMcHpT(const int* mygt3,const int* mygt4,const double* mylam12, const double* mylam11, const double* mylam14, double **ZH, double **ZP, double complex* myres);
/* End Subroutine CouplingHaphhaHhSMcHpT */ 


       /* ----------------------------------------------------------------------------------------------------- */


void CouplingHapHhSMHhSMcHapT(const int* mygt2,const int* mygt3,const double* mylam10, double **ZH, double complex* myres);
/* End Subroutine CouplingHapHhSMHhSMcHapT */ 


       /* ----------------------------------------------------------------------------------------------------- */


void CouplingHapHhSMsigacHpT(const int* mygt2,const int* mygt4,const double* mylam12, const double* mylam11, const double* mylam14, double **ZH, double **ZP, double complex* myres);
/* End Subroutine CouplingHapHhSMsigacHpT */ 


       /* ----------------------------------------------------------------------------------------------------- */


void CouplingHapHpcHapcHpT(const int* mygt2,const int* mygt4,const double* mylam11, const double* mylam10, const double* mylam14, double **ZP, double complex* myres);
/* End Subroutine CouplingHapHpcHapcHpT */ 


       /* ----------------------------------------------------------------------------------------------------- */


void CouplingHapsigasigacHapT(const double* mylam13, double complex* myres);
/* End Subroutine CouplingHapsigasigacHapT */ 


       /* ----------------------------------------------------------------------------------------------------- */


void CouplinghhahhahhahhaT(const double* mylam13, double complex* myres);
/* End Subroutine CouplinghhahhahhahhaT */ 


       /* ----------------------------------------------------------------------------------------------------- */


void CouplinghhahhaHhSMHhSMT(const int* mygt3,const int* mygt4,const double* mylam12, const double* mylam11, const double* mylam10, const double* mylam14, double **ZH, double complex* myres);
/* End Subroutine CouplinghhahhaHhSMHhSMT */ 


       /* ----------------------------------------------------------------------------------------------------- */


void CouplinghhahhaHpcHpT(const int* mygt3,const int* mygt4,const double* mylam10, double **ZP, double complex* myres);
/* End Subroutine CouplinghhahhaHpcHpT */ 


       /* ----------------------------------------------------------------------------------------------------- */


void CouplinghhahhasigasigaT(const double* mylam13, double complex* myres);
/* End Subroutine CouplinghhahhasigasigaT */ 


       /* ----------------------------------------------------------------------------------------------------- */


void CouplinghhaHhSMHpcHapT(const int* mygt2,const int* mygt3,const double* mylam12, const double* mylam11, const double* mylam14, double **ZH, double **ZP, double complex* myres);
/* End Subroutine CouplinghhaHhSMHpcHapT */ 


       /* ----------------------------------------------------------------------------------------------------- */


void CouplingHhSMHhSMHhSMHhSMT(const int* mygt1,const int* mygt2,const int* mygt3,const int* mygt4,const double* mylam1, const double* mylam3, const double* mylam4, const double* mylam7, const double* mylam6, const double* mylam5, const double* mylam8, double **ZH, double complex* myres);
/* End Subroutine CouplingHhSMHhSMHhSMHhSMT */ 


       /* ----------------------------------------------------------------------------------------------------- */


void CouplingHhSMHhSMHpcHpT(const int* mygt1,const int* mygt2,const int* mygt3,const int* mygt4,const double* mylam1, const double* mylam3, const double* mylam4, const double* mylam7, const double* mylam6, const double* mylam5, const double* mylam8, double **ZH, double **ZP, double complex* myres);
/* End Subroutine CouplingHhSMHhSMHpcHpT */ 


       /* ----------------------------------------------------------------------------------------------------- */


void CouplingHhSMHhSMsigasigaT(const int* mygt1,const int* mygt2,const double* mylam12, const double* mylam11, const double* mylam10, const double* mylam14, double **ZH, double complex* myres);
/* End Subroutine CouplingHhSMHhSMsigasigaT */ 


       /* ----------------------------------------------------------------------------------------------------- */


void CouplingHhSMHpsigacHapT(const int* mygt1,const int* mygt2,const double* mylam12, const double* mylam11, const double* mylam14, double **ZH, double **ZP, double complex* myres);
/* End Subroutine CouplingHhSMHpsigacHapT */ 


       /* ----------------------------------------------------------------------------------------------------- */


void CouplingHpHpcHapcHapT(const int* mygt1,const int* mygt2,const double* mylam12, double **ZP, double complex* myres);
/* End Subroutine CouplingHpHpcHapcHapT */ 


       /* ----------------------------------------------------------------------------------------------------- */


void CouplingHpHpcHpcHpT(const int* mygt1,const int* mygt2,const int* mygt3,const int* mygt4,const double* mylam1, const double* mylam3, const double* mylam4, const double* mylam2, const double* mylam7, const double* mylam6, const double* mylam5, const double* mylam8, double **ZP, double complex* myres);
/* End Subroutine CouplingHpHpcHpcHpT */ 


       /* ----------------------------------------------------------------------------------------------------- */


void CouplingHpsigasigacHpT(const int* mygt1,const int* mygt4,const double* mylam10, double **ZP, double complex* myres);
/* End Subroutine CouplingHpsigasigacHpT */ 


       /* ----------------------------------------------------------------------------------------------------- */


void CouplingsigasigasigasigaT(const double* mylam13, double complex* myres);
/* End Subroutine CouplingsigasigasigasigaT */ 


       /* ----------------------------------------------------------------------------------------------------- */


       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_AhAh_AhAh(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplAhAhAhAh[4][4][4][4], double complex cplAhAhHhSM[4][4][4], double MHhSM[4], double MAh[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_AhAh_Ahhha(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MAh[4], double Mhha); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_AhAh_AhHhSM(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MAh[4], double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_AhAh_Ahsiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MAh[4], double Msiga); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_AhAh_HapHapc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplAhAhHapcHap[4][4], double complex cplAhAhHhSM[4][4][4], double complex cplHapHhSMcHap[4], double MHhSM[4], double MAh[4], double MHap); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_AhAh_HapHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MAh[4], double MHp[4], double MHap); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_AhAh_hhahha(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplAhAhhhahha[4][4], double complex cplAhAhHhSM[4][4][4], double complex cplhhahhaHhSM[4], double MHhSM[4], double complex cplAhhhasiga[4], double Msiga, double MAh[4], double Mhha); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_AhAh_hhaHhSM(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MAh[4], double MHhSM[4], double Mhha); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_AhAh_hhasiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MAh[4], double Msiga, double Mhha); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_AhAh_HhSMHhSM(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplAhAhHhSMHhSM[4][4][4][4], double complex cplAhAhHhSM[4][4][4], double complex cplHhSMHhSMHhSM[4][4][4], double MHhSM[4], double MAh[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_AhAh_HhSMsiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MAh[4], double Msiga, double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_AhAh_HpHapc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MAh[4], double MHap, double MHp[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_AhAh_HpHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplAhAhHpcHp[4][4][4][4], double complex cplAhAhHhSM[4][4][4], double complex cplHhSMHpcHp[4][4][4], double MHhSM[4], double complex cplAhHpcHp[4][4][4], double MHp[4], double MAh[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_AhAh_sigasiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplAhAhsigasiga[4][4], double complex cplAhAhHhSM[4][4][4], double complex cplHhSMsigasiga[4], double MHhSM[4], double complex cplAhhhasiga[4], double Mhha, double MAh[4], double Msiga); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_AhHap_AhHapc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplAhAhHapcHap[4][4], double complex cplAhAhHhSM[4][4][4], double complex cplHapHhSMcHap[4], double MHhSM[4], double MAh[4], double MHap); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_AhHap_AhHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MAh[4], double MHp[4], double MHap); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_AhHap_hhaHapc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MAh[4], double MHap, double Mhha); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_AhHap_hhaHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplAhHaphhacHp[4][4], double complex cplAhhhasiga[4], double complex cplHapsigacHp[4], double Msiga, double complex cplAhHpcHp[4][4][4], double complex cplHaphhacHp[4], double MHp[4], double MAh[4], double MHap, double Mhha); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_AhHap_HhSMHapc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MAh[4], double MHap, double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_AhHap_HhSMHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MAh[4], double MHp[4], double MHap, double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_AhHap_sigaHapc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MAh[4], double MHap, double Msiga); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_AhHap_sigaHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplAhHapsigacHp[4][4], double complex cplAhhhasiga[4], double complex cplHaphhacHp[4], double Mhha, double complex cplAhHpcHp[4][4][4], double complex cplHapsigacHp[4], double MHp[4], double MAh[4], double MHap, double Msiga); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_Ahhha_AhAh(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MAh[4], double Mhha); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_Ahhha_Ahhha(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplAhAhhhahha[4][4], double complex cplAhhhasiga[4], double Msiga, double complex cplAhAhHhSM[4][4][4], double complex cplhhahhaHhSM[4], double MHhSM[4], double MAh[4], double Mhha); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_Ahhha_AhHhSM(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MAh[4], double MHhSM[4], double Mhha); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_Ahhha_Ahsiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MAh[4], double Msiga, double Mhha); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_Ahhha_HapHapc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MAh[4], double MHap, double Mhha); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_Ahhha_HapHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplAhHaphhacHp[4][4], double complex cplAhhhasiga[4], double complex cplHapsigacHp[4], double Msiga, double complex cplAhHpcHp[4][4][4], double complex cplHaphhacHp[4], double MHp[4], double MAh[4], double Mhha, double MHap); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_Ahhha_hhahha(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MAh[4], double Mhha); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_Ahhha_hhaHhSM(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MAh[4], double MHhSM[4], double Mhha); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_Ahhha_hhasiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MAh[4], double Msiga, double Mhha); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_Ahhha_HhSMHhSM(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MAh[4], double MHhSM[4], double Mhha); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_Ahhha_HhSMsiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplAhhhaHhSMsiga[4][4], double complex cplAhhhasiga[4], double complex cplHhSMsigasiga[4], double Msiga, double complex cplAhAhHhSM[4][4][4], double MAh[4], double complex cplhhahhaHhSM[4], double Mhha, double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_Ahhha_HpHapc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplAhhhaHpcHap[4][4], double complex cplAhhhasiga[4], double complex cplHpsigacHap[4], double Msiga, double complex cplAhHpcHp[4][4][4], double complex cplhhaHpcHap[4], double MHp[4], double MAh[4], double MHap, double Mhha); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_Ahhha_HpHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MAh[4], double MHp[4], double Mhha); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_Ahhha_sigasiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MAh[4], double Msiga, double Mhha); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_AhHhSM_AhAh(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MAh[4], double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_AhHhSM_Ahhha(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MAh[4], double Mhha, double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_AhHhSM_AhHhSM(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplAhAhHhSMHhSM[4][4][4][4], double complex cplAhAhHhSM[4][4][4], double MAh[4], double complex cplHhSMHhSMHhSM[4][4][4], double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_AhHhSM_Ahsiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MAh[4], double Msiga, double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_AhHhSM_HapHapc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MAh[4], double MHap, double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_AhHhSM_HapHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MAh[4], double MHp[4], double MHhSM[4], double MHap); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_AhHhSM_hhahha(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MAh[4], double Mhha, double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_AhHhSM_hhaHhSM(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MAh[4], double MHhSM[4], double Mhha); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_AhHhSM_hhasiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplAhhhaHhSMsiga[4][4], double complex cplAhAhHhSM[4][4][4], double complex cplAhhhasiga[4], double MAh[4], double complex cplHhSMsigasiga[4], double Msiga, double complex cplhhahhaHhSM[4], double Mhha, double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_AhHhSM_HhSMHhSM(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MAh[4], double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_AhHhSM_HhSMsiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MAh[4], double Msiga, double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_AhHhSM_HpHapc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MAh[4], double MHap, double MHhSM[4], double MHp[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_AhHhSM_HpHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplAhHhSMHpcHp[4][4][4][4], double complex cplAhAhHhSM[4][4][4], double complex cplAhHpcHp[4][4][4], double MAh[4], double complex cplHhSMHpcHp[4][4][4], double MHp[4], double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_AhHhSM_sigasiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MAh[4], double Msiga, double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_AhHp_AhHapc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MAh[4], double MHap, double MHp[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_AhHp_AhHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplAhAhHpcHp[4][4][4][4], double complex cplAhHpcHp[4][4][4], double MHp[4], double complex cplAhAhHhSM[4][4][4], double complex cplHhSMHpcHp[4][4][4], double MHhSM[4], double MAh[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_AhHp_hhaHapc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplAhhhaHpcHap[4][4], double complex cplAhHpcHp[4][4][4], double complex cplhhaHpcHap[4], double MHp[4], double complex cplAhhhasiga[4], double complex cplHpsigacHap[4], double Msiga, double MAh[4], double MHap, double Mhha); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_AhHp_hhaHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MAh[4], double MHp[4], double Mhha); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_AhHp_HhSMHapc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MAh[4], double MHap, double MHp[4], double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_AhHp_HhSMHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplAhHhSMHpcHp[4][4][4][4], double complex cplAhHpcHp[4][4][4], double complex cplHhSMHpcHp[4][4][4], double MHp[4], double complex cplAhAhHhSM[4][4][4], double MAh[4], double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_AhHp_sigaHapc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplAhHpsigacHap[4][4], double complex cplAhHpcHp[4][4][4], double complex cplHpsigacHap[4], double MHp[4], double complex cplAhhhasiga[4], double complex cplhhaHpcHap[4], double Mhha, double MAh[4], double MHap, double Msiga); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_AhHp_sigaHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MAh[4], double MHp[4], double Msiga); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_Ahsiga_AhAh(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MAh[4], double Msiga); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_Ahsiga_Ahhha(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MAh[4], double Mhha, double Msiga); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_Ahsiga_AhHhSM(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MAh[4], double MHhSM[4], double Msiga); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_Ahsiga_Ahsiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplAhAhsigasiga[4][4], double complex cplAhhhasiga[4], double Mhha, double complex cplAhAhHhSM[4][4][4], double complex cplHhSMsigasiga[4], double MHhSM[4], double MAh[4], double Msiga); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_Ahsiga_HapHapc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MAh[4], double MHap, double Msiga); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_Ahsiga_HapHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplAhHapsigacHp[4][4], double complex cplAhhhasiga[4], double complex cplHaphhacHp[4], double Mhha, double complex cplAhHpcHp[4][4][4], double complex cplHapsigacHp[4], double MHp[4], double MAh[4], double Msiga, double MHap); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_Ahsiga_hhahha(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MAh[4], double Mhha, double Msiga); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_Ahsiga_hhaHhSM(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplAhhhaHhSMsiga[4][4], double complex cplAhhhasiga[4], double complex cplhhahhaHhSM[4], double Mhha, double complex cplHhSMsigasiga[4], double Msiga, double complex cplAhAhHhSM[4][4][4], double MAh[4], double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_Ahsiga_hhasiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MAh[4], double Msiga, double Mhha); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_Ahsiga_HhSMHhSM(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MAh[4], double MHhSM[4], double Msiga); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_Ahsiga_HhSMsiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MAh[4], double Msiga, double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_Ahsiga_HpHapc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplAhHpsigacHap[4][4], double complex cplAhhhasiga[4], double complex cplhhaHpcHap[4], double Mhha, double complex cplAhHpcHp[4][4][4], double complex cplHpsigacHap[4], double MHp[4], double MAh[4], double MHap, double Msiga); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_Ahsiga_HpHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MAh[4], double MHp[4], double Msiga); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_Ahsiga_sigasiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MAh[4], double Msiga); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_AhHapc_AhHap(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplAhAhHapcHap[4][4], double complex cplAhAhHhSM[4][4][4], double complex cplHapHhSMcHap[4], double MHhSM[4], double MAh[4], double MHap); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_AhHapc_AhHp(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MAh[4], double MHp[4], double MHap); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_AhHapc_Haphha(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MAh[4], double Mhha, double MHap); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_AhHapc_HapHhSM(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MAh[4], double MHhSM[4], double MHap); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_AhHapc_Hapsiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MAh[4], double Msiga, double MHap); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_AhHapc_hhaHp(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplAhhhaHpcHap[4][4], double complex cplAhhhasiga[4], double complex cplHpsigacHap[4], double Msiga, double complex cplAhHpcHp[4][4][4], double complex cplhhaHpcHap[4], double MHp[4], double MAh[4], double MHap, double Mhha); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_AhHapc_HhSMHp(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MAh[4], double MHp[4], double MHap, double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_AhHapc_Hpsiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplAhHpsigacHap[4][4], double complex cplAhHpcHp[4][4][4], double complex cplHpsigacHap[4], double MHp[4], double complex cplAhhhasiga[4], double complex cplhhaHpcHap[4], double Mhha, double MAh[4], double Msiga, double MHap); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_AhHpc_AhHap(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MAh[4], double MHap, double MHp[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_AhHpc_AhHp(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplAhAhHpcHp[4][4][4][4], double complex cplAhHpcHp[4][4][4], double MHp[4], double complex cplAhAhHhSM[4][4][4], double complex cplHhSMHpcHp[4][4][4], double MHhSM[4], double MAh[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_AhHpc_Haphha(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplAhHaphhacHp[4][4], double complex cplAhHpcHp[4][4][4], double complex cplHaphhacHp[4], double MHp[4], double complex cplAhhhasiga[4], double complex cplHapsigacHp[4], double Msiga, double MAh[4], double Mhha, double MHap); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_AhHpc_HapHhSM(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MAh[4], double MHhSM[4], double MHp[4], double MHap); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_AhHpc_Hapsiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplAhHapsigacHp[4][4], double complex cplAhHpcHp[4][4][4], double complex cplHapsigacHp[4], double MHp[4], double complex cplAhhhasiga[4], double complex cplHaphhacHp[4], double Mhha, double MAh[4], double Msiga, double MHap); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_AhHpc_hhaHp(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MAh[4], double MHp[4], double Mhha); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_AhHpc_HhSMHp(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplAhHhSMHpcHp[4][4][4][4], double complex cplAhHpcHp[4][4][4], double complex cplHhSMHpcHp[4][4][4], double MHp[4], double complex cplAhAhHhSM[4][4][4], double MAh[4], double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_AhHpc_Hpsiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MAh[4], double Msiga, double MHp[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HapHap_HapcHapc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHapHapcHapcHap, double complex cplHapHhSMcHap[4], double MHhSM[4], double MHap); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HapHap_HapcHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHap, double MHp[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HapHap_HpcHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHapHapcHpcHp[4][4], double complex cplHaphhacHp[4], double Mhha, double complex cplHapsigacHp[4], double Msiga, double MHap, double MHp[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_Haphha_AhHapc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHap, double Mhha, double MAh[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_Haphha_AhHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplAhHaphhacHp[4][4], double complex cplHaphhacHp[4], double complex cplAhHpcHp[4][4][4], double MHp[4], double complex cplHapsigacHp[4], double complex cplAhhhasiga[4], double Msiga, double MHap, double Mhha, double MAh[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_Haphha_hhaHapc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHaphhahhacHap, double complex cplHaphhacHp[4], double complex cplhhaHpcHap[4], double MHp[4], double complex cplHapHhSMcHap[4], double complex cplhhahhaHhSM[4], double MHhSM[4], double MHap, double Mhha); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_Haphha_hhaHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHap, double MHp[4], double Mhha); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_Haphha_HhSMHapc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHap, double Mhha, double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_Haphha_HhSMHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHaphhaHhSMcHp[4][4], double complex cplHaphhacHp[4], double complex cplHhSMHpcHp[4][4][4], double MHp[4], double complex cplHapHhSMcHap[4], double MHap, double complex cplhhahhaHhSM[4], double Mhha, double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_Haphha_sigaHapc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHaphhacHp[4], double complex cplHpsigacHap[4], double MHp[4], double complex cplHapsigacHp[4], double complex cplhhaHpcHap[4], double MHap, double Mhha, double Msiga); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_Haphha_sigaHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHap, double MHp[4], double Mhha, double Msiga); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HapHhSM_AhHapc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHap, double MHhSM[4], double MAh[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HapHhSM_AhHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHap, double MHp[4], double MHhSM[4], double MAh[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HapHhSM_hhaHapc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHap, double MHhSM[4], double Mhha); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HapHhSM_hhaHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHaphhaHhSMcHp[4][4], double complex cplHapHhSMcHap[4], double complex cplHaphhacHp[4], double MHap, double complex cplHhSMHpcHp[4][4][4], double MHp[4], double complex cplhhahhaHhSM[4], double Mhha, double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HapHhSM_HhSMHapc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHapHhSMHhSMcHap[4][4], double complex cplHapHhSMcHap[4], double MHap, double complex cplHhSMHhSMHhSM[4][4][4], double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HapHhSM_HhSMHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHap, double MHp[4], double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HapHhSM_sigaHapc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHap, double MHhSM[4], double Msiga); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HapHhSM_sigaHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHapHhSMsigacHp[4][4], double complex cplHapHhSMcHap[4], double complex cplHapsigacHp[4], double MHap, double complex cplHhSMHpcHp[4][4][4], double MHp[4], double complex cplHhSMsigasiga[4], double Msiga, double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HapHp_HapcHapc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHap, double MHp[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HapHp_HapcHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHapHpcHapcHp[4][4], double complex cplHapHhSMcHap[4], double complex cplHhSMHpcHp[4][4][4], double MHhSM[4], double complex cplHaphhacHp[4], double complex cplhhaHpcHap[4], double Mhha, double complex cplHapsigacHp[4], double complex cplHpsigacHap[4], double Msiga, double MHap, double MHp[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HapHp_HpcHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHap, double MHp[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_Hapsiga_AhHapc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHap, double Msiga, double MAh[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_Hapsiga_AhHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplAhHapsigacHp[4][4], double complex cplHapsigacHp[4], double complex cplAhHpcHp[4][4][4], double MHp[4], double complex cplHaphhacHp[4], double complex cplAhhhasiga[4], double Mhha, double MHap, double Msiga, double MAh[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_Hapsiga_hhaHapc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHapsigacHp[4], double complex cplhhaHpcHap[4], double MHp[4], double complex cplHaphhacHp[4], double complex cplHpsigacHap[4], double MHap, double Msiga, double Mhha); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_Hapsiga_hhaHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHap, double MHp[4], double Msiga, double Mhha); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_Hapsiga_HhSMHapc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHap, double Msiga, double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_Hapsiga_HhSMHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHapHhSMsigacHp[4][4], double complex cplHapsigacHp[4], double complex cplHhSMHpcHp[4][4][4], double MHp[4], double complex cplHapHhSMcHap[4], double MHap, double complex cplHhSMsigasiga[4], double Msiga, double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_Hapsiga_sigaHapc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHapsigasigacHap, double complex cplHapsigacHp[4], double complex cplHpsigacHap[4], double MHp[4], double complex cplHapHhSMcHap[4], double complex cplHhSMsigasiga[4], double MHhSM[4], double MHap, double Msiga); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_Hapsiga_sigaHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHap, double MHp[4], double Msiga); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HapHapc_AhAh(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplAhAhHapcHap[4][4], double complex cplHapHhSMcHap[4], double complex cplAhAhHhSM[4][4][4], double MHhSM[4], double MHap, double MAh[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HapHapc_Ahhha(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHap, double Mhha, double MAh[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HapHapc_AhHhSM(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHap, double MHhSM[4], double MAh[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HapHapc_Ahsiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHap, double Msiga, double MAh[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HapHapc_HapHapc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHapHapcHapcHap, double complex cplHapHhSMcHap[4], double MHhSM[4], double MHap); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HapHapc_HapHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHap, double MHp[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HapHapc_hhahha(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHaphhahhacHap, double complex cplHapHhSMcHap[4], double complex cplhhahhaHhSM[4], double MHhSM[4], double complex cplHaphhacHp[4], double complex cplhhaHpcHap[4], double MHp[4], double MHap, double Mhha); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HapHapc_hhaHhSM(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHap, double MHhSM[4], double Mhha); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HapHapc_hhasiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHaphhacHp[4], double complex cplHpsigacHap[4], double MHp[4], double complex cplHapsigacHp[4], double complex cplhhaHpcHap[4], double MHap, double Msiga, double Mhha); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HapHapc_HhSMHhSM(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHapHhSMHhSMcHap[4][4], double complex cplHapHhSMcHap[4], double complex cplHhSMHhSMHhSM[4][4][4], double MHhSM[4], double MHap); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HapHapc_HhSMsiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHap, double Msiga, double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HapHapc_HpHapc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHap, double MHp[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HapHapc_HpHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHapHpcHapcHp[4][4], double complex cplHapHhSMcHap[4], double complex cplHhSMHpcHp[4][4][4], double MHhSM[4], double complex cplHaphhacHp[4], double complex cplhhaHpcHap[4], double Mhha, double complex cplHapsigacHp[4], double complex cplHpsigacHap[4], double Msiga, double MHap, double MHp[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HapHapc_sigasiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHapsigasigacHap, double complex cplHapHhSMcHap[4], double complex cplHhSMsigasiga[4], double MHhSM[4], double complex cplHapsigacHp[4], double complex cplHpsigacHap[4], double MHp[4], double MHap, double Msiga); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HapHpc_AhAh(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHap, double MAh[4], double MHp[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HapHpc_Ahhha(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplAhHaphhacHp[4][4], double complex cplHapsigacHp[4], double complex cplAhhhasiga[4], double Msiga, double complex cplHaphhacHp[4], double complex cplAhHpcHp[4][4][4], double MHp[4], double MHap, double Mhha, double MAh[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HapHpc_AhHhSM(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHap, double MHhSM[4], double MHp[4], double MAh[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HapHpc_Ahsiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplAhHapsigacHp[4][4], double complex cplHaphhacHp[4], double complex cplAhhhasiga[4], double Mhha, double complex cplHapsigacHp[4], double complex cplAhHpcHp[4][4][4], double MHp[4], double MHap, double Msiga, double MAh[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HapHpc_HapHapc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHap, double MHp[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HapHpc_HapHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHapHapcHpcHp[4][4], double complex cplHaphhacHp[4], double Mhha, double complex cplHapsigacHp[4], double Msiga, double MHap, double MHp[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HapHpc_hhahha(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHap, double Mhha, double MHp[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HapHpc_hhaHhSM(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHaphhaHhSMcHp[4][4], double complex cplHaphhacHp[4], double complex cplhhahhaHhSM[4], double Mhha, double complex cplHhSMHpcHp[4][4][4], double MHp[4], double complex cplHapHhSMcHap[4], double MHap, double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HapHpc_hhasiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHap, double Msiga, double MHp[4], double Mhha); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HapHpc_HhSMHhSM(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHap, double MHhSM[4], double MHp[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HapHpc_HhSMsiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHapHhSMsigacHp[4][4], double complex cplHapsigacHp[4], double complex cplHhSMsigasiga[4], double Msiga, double complex cplHapHhSMcHap[4], double MHap, double complex cplHhSMHpcHp[4][4][4], double MHp[4], double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HapHpc_HpHapc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHapHpcHapcHp[4][4], double complex cplHaphhacHp[4], double complex cplhhaHpcHap[4], double Mhha, double complex cplHapsigacHp[4], double complex cplHpsigacHap[4], double Msiga, double complex cplHapHhSMcHap[4], double complex cplHhSMHpcHp[4][4][4], double MHhSM[4], double MHap, double MHp[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HapHpc_HpHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHap, double MHp[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HapHpc_sigasiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHap, double Msiga, double MHp[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhahha_AhAh(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplAhAhhhahha[4][4], double complex cplhhahhaHhSM[4], double complex cplAhAhHhSM[4][4][4], double MHhSM[4], double complex cplAhhhasiga[4], double Msiga, double Mhha, double MAh[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhahha_Ahhha(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double Mhha, double MAh[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhahha_AhHhSM(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double Mhha, double MHhSM[4], double MAh[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhahha_Ahsiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double Mhha, double Msiga, double MAh[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhahha_HapHapc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHaphhahhacHap, double complex cplhhahhaHhSM[4], double complex cplHapHhSMcHap[4], double MHhSM[4], double complex cplHaphhacHp[4], double complex cplhhaHpcHap[4], double MHp[4], double Mhha, double MHap); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhahha_HapHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double Mhha, double MHp[4], double MHap); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhahha_hhahha(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplhhahhahhahha, double complex cplhhahhaHhSM[4], double MHhSM[4], double Mhha); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhahha_hhaHhSM(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double Mhha, double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhahha_hhasiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double Mhha, double Msiga); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhahha_HhSMHhSM(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplhhahhaHhSMHhSM[4][4], double complex cplhhahhaHhSM[4], double complex cplHhSMHhSMHhSM[4][4][4], double MHhSM[4], double Mhha); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhahha_HhSMsiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double Mhha, double Msiga, double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhahha_HpHapc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double Mhha, double MHap, double MHp[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhahha_HpHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplhhahhaHpcHp[4][4], double complex cplhhahhaHhSM[4], double complex cplHhSMHpcHp[4][4][4], double MHhSM[4], double complex cplhhaHpcHap[4], double complex cplHaphhacHp[4], double MHap, double Mhha, double MHp[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhahha_sigasiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplhhahhasigasiga, double complex cplhhahhaHhSM[4], double complex cplHhSMsigasiga[4], double MHhSM[4], double complex cplAhhhasiga[4], double MAh[4], double Mhha, double Msiga); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhaHhSM_AhAh(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double Mhha, double MAh[4], double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhaHhSM_Ahhha(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double Mhha, double MHhSM[4], double MAh[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhaHhSM_AhHhSM(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double Mhha, double MHhSM[4], double MAh[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhaHhSM_Ahsiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplAhhhaHhSMsiga[4][4], double complex cplhhahhaHhSM[4], double complex cplAhhhasiga[4], double Mhha, double complex cplHhSMsigasiga[4], double Msiga, double complex cplAhAhHhSM[4][4][4], double MAh[4], double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhaHhSM_HapHapc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double Mhha, double MHap, double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhaHhSM_HapHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHaphhaHhSMcHp[4][4], double complex cplhhahhaHhSM[4], double complex cplHaphhacHp[4], double Mhha, double complex cplHhSMHpcHp[4][4][4], double MHp[4], double complex cplHapHhSMcHap[4], double MHap, double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhaHhSM_hhahha(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double Mhha, double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhaHhSM_hhaHhSM(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplhhahhaHhSMHhSM[4][4], double complex cplhhahhaHhSM[4], double Mhha, double complex cplHhSMHhSMHhSM[4][4][4], double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhaHhSM_hhasiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double Mhha, double Msiga, double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhaHhSM_HhSMHhSM(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double Mhha, double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhaHhSM_HhSMsiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double Mhha, double Msiga, double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhaHhSM_HpHapc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplhhaHhSMHpcHap[4][4], double complex cplhhahhaHhSM[4], double complex cplhhaHpcHap[4], double Mhha, double complex cplHapHhSMcHap[4], double MHap, double complex cplHhSMHpcHp[4][4][4], double MHp[4], double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhaHhSM_HpHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double Mhha, double MHp[4], double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhaHhSM_sigasiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double Mhha, double Msiga, double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhaHp_AhHapc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplAhhhaHpcHap[4][4], double complex cplAhhhasiga[4], double complex cplHpsigacHap[4], double Msiga, double complex cplhhaHpcHap[4], double complex cplAhHpcHp[4][4][4], double MHp[4], double Mhha, double MHap, double MAh[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhaHp_AhHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double Mhha, double MHp[4], double MAh[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhaHp_hhaHapc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double Mhha, double MHap, double MHp[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhaHp_hhaHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplhhahhaHpcHp[4][4], double complex cplhhaHpcHap[4], double complex cplHaphhacHp[4], double MHap, double complex cplhhahhaHhSM[4], double complex cplHhSMHpcHp[4][4][4], double MHhSM[4], double Mhha, double MHp[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhaHp_HhSMHapc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplhhaHhSMHpcHap[4][4], double complex cplhhaHpcHap[4], double complex cplHapHhSMcHap[4], double MHap, double complex cplhhahhaHhSM[4], double Mhha, double complex cplHhSMHpcHp[4][4][4], double MHp[4], double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhaHp_HhSMHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double Mhha, double MHp[4], double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhaHp_sigaHapc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double Mhha, double MHap, double MHp[4], double Msiga); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhaHp_sigaHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplhhaHpcHap[4], double complex cplHapsigacHp[4], double MHap, double complex cplAhhhasiga[4], double complex cplAhHpcHp[4][4][4], double MAh[4], double complex cplHaphhacHp[4], double complex cplHpsigacHap[4], double Mhha, double MHp[4], double Msiga); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhasiga_AhAh(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double Mhha, double MAh[4], double Msiga); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhasiga_Ahhha(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double Mhha, double Msiga, double MAh[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhasiga_AhHhSM(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplAhhhaHhSMsiga[4][4], double complex cplAhhhasiga[4], double complex cplAhAhHhSM[4][4][4], double MAh[4], double complex cplHhSMsigasiga[4], double Msiga, double complex cplhhahhaHhSM[4], double Mhha, double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhasiga_Ahsiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double Mhha, double Msiga, double MAh[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhasiga_HapHapc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHaphhacHp[4], double complex cplHpsigacHap[4], double MHp[4], double complex cplhhaHpcHap[4], double complex cplHapsigacHp[4], double Mhha, double MHap, double Msiga); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhasiga_HapHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double Mhha, double MHp[4], double Msiga, double MHap); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhasiga_hhahha(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double Mhha, double Msiga); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhasiga_hhaHhSM(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double Mhha, double MHhSM[4], double Msiga); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhasiga_hhasiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplhhahhasigasiga, double complex cplAhhhasiga[4], double MAh[4], double complex cplhhahhaHhSM[4], double complex cplHhSMsigasiga[4], double MHhSM[4], double Mhha, double Msiga); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhasiga_HhSMHhSM(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double Mhha, double MHhSM[4], double Msiga); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhasiga_HhSMsiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double Mhha, double Msiga, double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhasiga_HpHapc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double Mhha, double MHap, double Msiga, double MHp[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhasiga_HpHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplAhhhasiga[4], double complex cplAhHpcHp[4][4][4], double MAh[4], double complex cplhhaHpcHap[4], double complex cplHapsigacHp[4], double MHap, double complex cplHaphhacHp[4], double complex cplHpsigacHap[4], double Mhha, double MHp[4], double Msiga); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhasiga_sigasiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double Mhha, double Msiga); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhaHapc_AhHap(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double Mhha, double MHap, double MAh[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhaHapc_AhHp(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplAhhhaHpcHap[4][4], double complex cplhhaHpcHap[4], double complex cplAhHpcHp[4][4][4], double MHp[4], double complex cplAhhhasiga[4], double complex cplHpsigacHap[4], double Msiga, double Mhha, double MHap, double MAh[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhaHapc_Haphha(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHaphhahhacHap, double complex cplhhaHpcHap[4], double complex cplHaphhacHp[4], double MHp[4], double complex cplhhahhaHhSM[4], double complex cplHapHhSMcHap[4], double MHhSM[4], double Mhha, double MHap); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhaHapc_HapHhSM(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double Mhha, double MHhSM[4], double MHap); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhaHapc_Hapsiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplhhaHpcHap[4], double complex cplHapsigacHp[4], double MHp[4], double complex cplHaphhacHp[4], double complex cplHpsigacHap[4], double Mhha, double Msiga, double MHap); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhaHapc_hhaHp(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double Mhha, double MHp[4], double MHap); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhaHapc_HhSMHp(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplhhaHhSMHpcHap[4][4], double complex cplhhaHpcHap[4], double complex cplHhSMHpcHp[4][4][4], double MHp[4], double complex cplhhahhaHhSM[4], double Mhha, double complex cplHapHhSMcHap[4], double MHap, double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhaHapc_Hpsiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double Mhha, double Msiga, double MHap, double MHp[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhaHpc_AhHap(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplAhHaphhacHp[4][4], double complex cplAhhhasiga[4], double complex cplHapsigacHp[4], double Msiga, double complex cplHaphhacHp[4], double complex cplAhHpcHp[4][4][4], double MHp[4], double Mhha, double MHap, double MAh[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhaHpc_AhHp(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double Mhha, double MHp[4], double MAh[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhaHpc_Haphha(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double Mhha, double MHp[4], double MHap); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhaHpc_HapHhSM(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHaphhaHhSMcHp[4][4], double complex cplHaphhacHp[4], double complex cplHapHhSMcHap[4], double MHap, double complex cplHhSMHpcHp[4][4][4], double MHp[4], double complex cplhhahhaHhSM[4], double Mhha, double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhaHpc_Hapsiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double Mhha, double Msiga, double MHp[4], double MHap); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhaHpc_hhaHp(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplhhahhaHpcHp[4][4], double complex cplHaphhacHp[4], double complex cplhhaHpcHap[4], double MHap, double complex cplhhahhaHhSM[4], double complex cplHhSMHpcHp[4][4][4], double MHhSM[4], double Mhha, double MHp[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhaHpc_HhSMHp(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double Mhha, double MHp[4], double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_hhaHpc_Hpsiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHaphhacHp[4], double complex cplHpsigacHap[4], double MHap, double complex cplhhaHpcHap[4], double complex cplHapsigacHp[4], double complex cplAhhhasiga[4], double complex cplAhHpcHp[4][4][4], double MAh[4], double Mhha, double Msiga, double MHp[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HhSMHhSM_AhAh(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplAhAhHhSMHhSM[4][4][4][4], double complex cplHhSMHhSMHhSM[4][4][4], double complex cplAhAhHhSM[4][4][4], double MHhSM[4], double MAh[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HhSMHhSM_Ahhha(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHhSM[4], double Mhha, double MAh[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HhSMHhSM_AhHhSM(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHhSM[4], double MAh[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HhSMHhSM_Ahsiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHhSM[4], double Msiga, double MAh[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HhSMHhSM_HapHapc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHapHhSMHhSMcHap[4][4], double complex cplHhSMHhSMHhSM[4][4][4], double complex cplHapHhSMcHap[4], double MHhSM[4], double MHap); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HhSMHhSM_HapHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHhSM[4], double MHp[4], double MHap); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HhSMHhSM_hhahha(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplhhahhaHhSMHhSM[4][4], double complex cplHhSMHhSMHhSM[4][4][4], double complex cplhhahhaHhSM[4], double MHhSM[4], double Mhha); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HhSMHhSM_hhaHhSM(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHhSM[4], double Mhha); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HhSMHhSM_hhasiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHhSM[4], double Msiga, double Mhha); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HhSMHhSM_HhSMHhSM(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHhSMHhSMHhSMHhSM[4][4][4][4], double complex cplHhSMHhSMHhSM[4][4][4], double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HhSMHhSM_HhSMsiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHhSM[4], double Msiga); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HhSMHhSM_HpHapc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHhSM[4], double MHap, double MHp[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HhSMHhSM_HpHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHhSMHhSMHpcHp[4][4][4][4], double complex cplHhSMHhSMHhSM[4][4][4], double complex cplHhSMHpcHp[4][4][4], double MHhSM[4], double MHp[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HhSMHhSM_sigasiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHhSMHhSMsigasiga[4][4], double complex cplHhSMHhSMHhSM[4][4][4], double complex cplHhSMsigasiga[4], double MHhSM[4], double Msiga); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HhSMHp_AhHapc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHhSM[4], double MHap, double MHp[4], double MAh[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HhSMHp_AhHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplAhHhSMHpcHp[4][4][4][4], double complex cplHhSMHpcHp[4][4][4], double complex cplAhHpcHp[4][4][4], double MHp[4], double complex cplAhAhHhSM[4][4][4], double MAh[4], double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HhSMHp_hhaHapc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplhhaHhSMHpcHap[4][4], double complex cplHhSMHpcHp[4][4][4], double complex cplhhaHpcHap[4], double MHp[4], double complex cplhhahhaHhSM[4], double Mhha, double complex cplHapHhSMcHap[4], double MHap, double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HhSMHp_hhaHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHhSM[4], double MHp[4], double Mhha); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HhSMHp_HhSMHapc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHhSM[4], double MHap, double MHp[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HhSMHp_HhSMHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHhSMHhSMHpcHp[4][4][4][4], double complex cplHhSMHpcHp[4][4][4], double MHp[4], double complex cplHhSMHhSMHhSM[4][4][4], double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HhSMHp_sigaHapc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHhSMHpsigacHap[4][4], double complex cplHhSMHpcHp[4][4][4], double complex cplHpsigacHap[4], double MHp[4], double complex cplHhSMsigasiga[4], double Msiga, double complex cplHapHhSMcHap[4], double MHap, double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HhSMHp_sigaHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHhSM[4], double MHp[4], double Msiga); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HhSMsiga_AhAh(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHhSM[4], double MAh[4], double Msiga); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HhSMsiga_Ahhha(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplAhhhaHhSMsiga[4][4], double complex cplHhSMsigasiga[4], double complex cplAhhhasiga[4], double Msiga, double complex cplAhAhHhSM[4][4][4], double MAh[4], double complex cplhhahhaHhSM[4], double Mhha, double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HhSMsiga_AhHhSM(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHhSM[4], double Msiga, double MAh[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HhSMsiga_Ahsiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHhSM[4], double Msiga, double MAh[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HhSMsiga_HapHapc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHhSM[4], double MHap, double Msiga); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HhSMsiga_HapHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHapHhSMsigacHp[4][4], double complex cplHhSMsigasiga[4], double complex cplHapsigacHp[4], double Msiga, double complex cplHapHhSMcHap[4], double MHap, double complex cplHhSMHpcHp[4][4][4], double MHp[4], double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HhSMsiga_hhahha(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHhSM[4], double Mhha, double Msiga); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HhSMsiga_hhaHhSM(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHhSM[4], double Msiga, double Mhha); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HhSMsiga_hhasiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHhSM[4], double Msiga, double Mhha); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HhSMsiga_HhSMHhSM(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHhSM[4], double Msiga); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HhSMsiga_HhSMsiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHhSMHhSMsigasiga[4][4], double complex cplHhSMsigasiga[4], double Msiga, double complex cplHhSMHhSMHhSM[4][4][4], double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HhSMsiga_HpHapc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHhSMHpsigacHap[4][4], double complex cplHhSMsigasiga[4], double complex cplHpsigacHap[4], double Msiga, double complex cplHhSMHpcHp[4][4][4], double MHp[4], double complex cplHapHhSMcHap[4], double MHap, double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HhSMsiga_HpHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHhSM[4], double MHp[4], double Msiga); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HhSMsiga_sigasiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHhSM[4], double Msiga); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HhSMHapc_AhHap(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHhSM[4], double MHap, double MAh[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HhSMHapc_AhHp(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHhSM[4], double MHp[4], double MHap, double MAh[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HhSMHapc_Haphha(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHhSM[4], double Mhha, double MHap); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HhSMHapc_HapHhSM(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHapHhSMHhSMcHap[4][4], double complex cplHapHhSMcHap[4], double MHap, double complex cplHhSMHhSMHhSM[4][4][4], double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HhSMHapc_Hapsiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHhSM[4], double Msiga, double MHap); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HhSMHapc_hhaHp(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplhhaHhSMHpcHap[4][4], double complex cplHapHhSMcHap[4], double complex cplhhaHpcHap[4], double MHap, double complex cplhhahhaHhSM[4], double Mhha, double complex cplHhSMHpcHp[4][4][4], double MHp[4], double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HhSMHapc_HhSMHp(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHhSM[4], double MHp[4], double MHap); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HhSMHapc_Hpsiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHhSMHpsigacHap[4][4], double complex cplHapHhSMcHap[4], double complex cplHpsigacHap[4], double MHap, double complex cplHhSMHpcHp[4][4][4], double MHp[4], double complex cplHhSMsigasiga[4], double Msiga, double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HhSMHpc_AhHap(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHhSM[4], double MHap, double MHp[4], double MAh[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HhSMHpc_AhHp(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplAhHhSMHpcHp[4][4][4][4], double complex cplHhSMHpcHp[4][4][4], double complex cplAhHpcHp[4][4][4], double MHp[4], double complex cplAhAhHhSM[4][4][4], double MAh[4], double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HhSMHpc_Haphha(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHaphhaHhSMcHp[4][4], double complex cplHhSMHpcHp[4][4][4], double complex cplHaphhacHp[4], double MHp[4], double complex cplHapHhSMcHap[4], double MHap, double complex cplhhahhaHhSM[4], double Mhha, double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HhSMHpc_HapHhSM(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHhSM[4], double MHp[4], double MHap); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HhSMHpc_Hapsiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHapHhSMsigacHp[4][4], double complex cplHhSMHpcHp[4][4][4], double complex cplHapsigacHp[4], double MHp[4], double complex cplHapHhSMcHap[4], double MHap, double complex cplHhSMsigasiga[4], double Msiga, double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HhSMHpc_hhaHp(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHhSM[4], double MHp[4], double Mhha); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HhSMHpc_HhSMHp(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHhSMHhSMHpcHp[4][4][4][4], double complex cplHhSMHpcHp[4][4][4], double MHp[4], double complex cplHhSMHhSMHhSM[4][4][4], double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HhSMHpc_Hpsiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHhSM[4], double Msiga, double MHp[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HpHp_HapcHapc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHpHpcHapcHap[4][4], double complex cplhhaHpcHap[4], double Mhha, double complex cplHpsigacHap[4], double Msiga, double MHp[4], double MHap); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HpHp_HapcHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHp[4], double MHap); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HpHp_HpcHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHpHpcHpcHp[4][4][4][4], double complex cplAhHpcHp[4][4][4], double MAh[4], double complex cplHhSMHpcHp[4][4][4], double MHhSM[4], double MHp[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_Hpsiga_AhHapc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplAhHpsigacHap[4][4], double complex cplAhHpcHp[4][4][4], double complex cplHpsigacHap[4], double MHp[4], double complex cplhhaHpcHap[4], double complex cplAhhhasiga[4], double Mhha, double MHap, double Msiga, double MAh[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_Hpsiga_AhHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHp[4], double Msiga, double MAh[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_Hpsiga_hhaHapc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHp[4], double MHap, double Msiga, double Mhha); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_Hpsiga_hhaHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHpsigacHap[4], double complex cplHaphhacHp[4], double MHap, double complex cplhhaHpcHap[4], double complex cplHapsigacHp[4], double complex cplAhHpcHp[4][4][4], double complex cplAhhhasiga[4], double MAh[4], double MHp[4], double Msiga, double Mhha); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_Hpsiga_HhSMHapc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHhSMHpsigacHap[4][4], double complex cplHpsigacHap[4], double complex cplHapHhSMcHap[4], double MHap, double complex cplHhSMHpcHp[4][4][4], double MHp[4], double complex cplHhSMsigasiga[4], double Msiga, double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_Hpsiga_HhSMHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHp[4], double Msiga, double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_Hpsiga_sigaHapc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHp[4], double MHap, double Msiga); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_Hpsiga_sigaHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHpsigasigacHp[4][4], double complex cplHpsigacHap[4], double complex cplHapsigacHp[4], double MHap, double complex cplHhSMHpcHp[4][4][4], double complex cplHhSMsigasiga[4], double MHhSM[4], double MHp[4], double Msiga); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HpHapc_AhAh(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHp[4], double MAh[4], double MHap); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HpHapc_Ahhha(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplAhhhaHpcHap[4][4], double complex cplHpsigacHap[4], double complex cplAhhhasiga[4], double Msiga, double complex cplAhHpcHp[4][4][4], double complex cplhhaHpcHap[4], double MHp[4], double Mhha, double MHap, double MAh[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HpHapc_AhHhSM(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHp[4], double MHhSM[4], double MHap, double MAh[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HpHapc_Ahsiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplAhHpsigacHap[4][4], double complex cplhhaHpcHap[4], double complex cplAhhhasiga[4], double Mhha, double complex cplAhHpcHp[4][4][4], double complex cplHpsigacHap[4], double MHp[4], double Msiga, double MHap, double MAh[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HpHapc_HapHapc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHp[4], double MHap); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HpHapc_HapHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHapHpcHapcHp[4][4], double complex cplhhaHpcHap[4], double complex cplHaphhacHp[4], double Mhha, double complex cplHpsigacHap[4], double complex cplHapsigacHp[4], double Msiga, double complex cplHhSMHpcHp[4][4][4], double complex cplHapHhSMcHap[4], double MHhSM[4], double MHp[4], double MHap); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HpHapc_hhahha(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHp[4], double Mhha, double MHap); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HpHapc_hhaHhSM(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplhhaHhSMHpcHap[4][4], double complex cplhhaHpcHap[4], double complex cplhhahhaHhSM[4], double Mhha, double complex cplHapHhSMcHap[4], double MHap, double complex cplHhSMHpcHp[4][4][4], double MHp[4], double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HpHapc_hhasiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHp[4], double Msiga, double MHap, double Mhha); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HpHapc_HhSMHhSM(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHp[4], double MHhSM[4], double MHap); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HpHapc_HhSMsiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHhSMHpsigacHap[4][4], double complex cplHpsigacHap[4], double complex cplHhSMsigasiga[4], double Msiga, double complex cplHhSMHpcHp[4][4][4], double MHp[4], double complex cplHapHhSMcHap[4], double MHap, double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HpHapc_HpHapc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHpHpcHapcHap[4][4], double complex cplhhaHpcHap[4], double Mhha, double complex cplHpsigacHap[4], double Msiga, double MHp[4], double MHap); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HpHapc_HpHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHp[4], double MHap); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HpHapc_sigasiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHp[4], double Msiga, double MHap); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HpHpc_AhAh(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplAhAhHpcHp[4][4][4][4], double complex cplHhSMHpcHp[4][4][4], double complex cplAhAhHhSM[4][4][4], double MHhSM[4], double complex cplAhHpcHp[4][4][4], double MHp[4], double MAh[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HpHpc_Ahhha(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHp[4], double Mhha, double MAh[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HpHpc_AhHhSM(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplAhHhSMHpcHp[4][4][4][4], double complex cplAhHpcHp[4][4][4], double complex cplAhAhHhSM[4][4][4], double MAh[4], double complex cplHhSMHpcHp[4][4][4], double MHp[4], double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HpHpc_Ahsiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHp[4], double Msiga, double MAh[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HpHpc_HapHapc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHapHpcHapcHp[4][4], double complex cplHhSMHpcHp[4][4][4], double complex cplHapHhSMcHap[4], double MHhSM[4], double complex cplhhaHpcHap[4], double complex cplHaphhacHp[4], double Mhha, double complex cplHpsigacHap[4], double complex cplHapsigacHp[4], double Msiga, double MHp[4], double MHap); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HpHpc_HapHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHp[4], double MHap); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HpHpc_hhahha(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplhhahhaHpcHp[4][4], double complex cplHhSMHpcHp[4][4][4], double complex cplhhahhaHhSM[4], double MHhSM[4], double complex cplhhaHpcHap[4], double complex cplHaphhacHp[4], double MHap, double MHp[4], double Mhha); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HpHpc_hhaHhSM(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHp[4], double MHhSM[4], double Mhha); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HpHpc_hhasiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplAhHpcHp[4][4][4], double complex cplAhhhasiga[4], double MAh[4], double complex cplhhaHpcHap[4], double complex cplHapsigacHp[4], double MHap, double complex cplHpsigacHap[4], double complex cplHaphhacHp[4], double MHp[4], double Msiga, double Mhha); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HpHpc_HhSMHhSM(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHhSMHhSMHpcHp[4][4][4][4], double complex cplHhSMHpcHp[4][4][4], double complex cplHhSMHhSMHhSM[4][4][4], double MHhSM[4], double MHp[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HpHpc_HhSMsiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHp[4], double Msiga, double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HpHpc_HpHapc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHp[4], double MHap); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HpHpc_HpHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHpHpcHpcHp[4][4][4][4], double complex cplAhHpcHp[4][4][4], double MAh[4], double complex cplHhSMHpcHp[4][4][4], double MHhSM[4], double MHp[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HpHpc_sigasiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHpsigasigacHp[4][4], double complex cplHhSMHpcHp[4][4][4], double complex cplHhSMsigasiga[4], double MHhSM[4], double complex cplHpsigacHap[4], double complex cplHapsigacHp[4], double MHap, double MHp[4], double Msiga); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_sigasiga_AhAh(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplAhAhsigasiga[4][4], double complex cplHhSMsigasiga[4], double complex cplAhAhHhSM[4][4][4], double MHhSM[4], double complex cplAhhhasiga[4], double Mhha, double Msiga, double MAh[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_sigasiga_Ahhha(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double Msiga, double Mhha, double MAh[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_sigasiga_AhHhSM(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double Msiga, double MHhSM[4], double MAh[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_sigasiga_Ahsiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double Msiga, double MAh[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_sigasiga_HapHapc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHapsigasigacHap, double complex cplHhSMsigasiga[4], double complex cplHapHhSMcHap[4], double MHhSM[4], double complex cplHapsigacHp[4], double complex cplHpsigacHap[4], double MHp[4], double Msiga, double MHap); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_sigasiga_HapHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double Msiga, double MHp[4], double MHap); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_sigasiga_hhahha(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplhhahhasigasiga, double complex cplHhSMsigasiga[4], double complex cplhhahhaHhSM[4], double MHhSM[4], double complex cplAhhhasiga[4], double MAh[4], double Msiga, double Mhha); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_sigasiga_hhaHhSM(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double Msiga, double MHhSM[4], double Mhha); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_sigasiga_hhasiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double Msiga, double Mhha); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_sigasiga_HhSMHhSM(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHhSMHhSMsigasiga[4][4], double complex cplHhSMsigasiga[4], double complex cplHhSMHhSMHhSM[4][4][4], double MHhSM[4], double Msiga); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_sigasiga_HhSMsiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double Msiga, double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_sigasiga_HpHapc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double Msiga, double MHap, double MHp[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_sigasiga_HpHpc(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHpsigasigacHp[4][4], double complex cplHhSMsigasiga[4], double complex cplHhSMHpcHp[4][4][4], double MHhSM[4], double complex cplHpsigacHap[4], double complex cplHapsigacHp[4], double MHap, double Msiga, double MHp[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_sigasiga_sigasiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplsigasigasigasiga, double complex cplHhSMsigasiga[4], double MHhSM[4], double Msiga); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_sigaHapc_AhHap(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double Msiga, double MHap, double MAh[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_sigaHapc_AhHp(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplAhHpsigacHap[4][4], double complex cplHpsigacHap[4], double complex cplAhHpcHp[4][4][4], double MHp[4], double complex cplAhhhasiga[4], double complex cplhhaHpcHap[4], double Mhha, double Msiga, double MHap, double MAh[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_sigaHapc_Haphha(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHpsigacHap[4], double complex cplHaphhacHp[4], double MHp[4], double complex cplHapsigacHp[4], double complex cplhhaHpcHap[4], double Msiga, double Mhha, double MHap); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_sigaHapc_HapHhSM(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double Msiga, double MHhSM[4], double MHap); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_sigaHapc_Hapsiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHapsigasigacHap, double complex cplHpsigacHap[4], double complex cplHapsigacHp[4], double MHp[4], double complex cplHhSMsigasiga[4], double complex cplHapHhSMcHap[4], double MHhSM[4], double Msiga, double MHap); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_sigaHapc_hhaHp(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double Msiga, double MHp[4], double MHap, double Mhha); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_sigaHapc_HhSMHp(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHhSMHpsigacHap[4][4], double complex cplHpsigacHap[4], double complex cplHhSMHpcHp[4][4][4], double MHp[4], double complex cplHhSMsigasiga[4], double Msiga, double complex cplHapHhSMcHap[4], double MHap, double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_sigaHapc_Hpsiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double Msiga, double MHap, double MHp[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_sigaHpc_AhHap(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplAhHapsigacHp[4][4], double complex cplAhhhasiga[4], double complex cplHaphhacHp[4], double Mhha, double complex cplHapsigacHp[4], double complex cplAhHpcHp[4][4][4], double MHp[4], double Msiga, double MHap, double MAh[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_sigaHpc_AhHp(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double Msiga, double MHp[4], double MAh[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_sigaHpc_Haphha(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double Msiga, double Mhha, double MHp[4], double MHap); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_sigaHpc_HapHhSM(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHapHhSMsigacHp[4][4], double complex cplHapsigacHp[4], double complex cplHapHhSMcHap[4], double MHap, double complex cplHhSMHpcHp[4][4][4], double MHp[4], double complex cplHhSMsigasiga[4], double Msiga, double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_sigaHpc_Hapsiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double Msiga, double MHp[4], double MHap); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_sigaHpc_hhaHp(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHapsigacHp[4], double complex cplhhaHpcHap[4], double MHap, double complex cplAhhhasiga[4], double complex cplAhHpcHp[4][4][4], double MAh[4], double complex cplHpsigacHap[4], double complex cplHaphhacHp[4], double Msiga, double MHp[4], double Mhha); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_sigaHpc_HhSMHp(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double Msiga, double MHp[4], double MHhSM[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_sigaHpc_Hpsiga(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHpsigasigacHp[4][4], double complex cplHapsigacHp[4], double complex cplHpsigacHap[4], double MHap, double complex cplHhSMsigasiga[4], double complex cplHhSMHpcHp[4][4][4], double MHhSM[4], double Msiga, double MHp[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HapcHapc_HapHap(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHapHapcHapcHap, double complex cplHapHhSMcHap[4], double MHhSM[4], double MHap); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HapcHapc_HapHp(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHap, double MHp[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HapcHapc_HpHp(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHpHpcHapcHap[4][4], double complex cplhhaHpcHap[4], double Mhha, double complex cplHpsigacHap[4], double Msiga, double MHap, double MHp[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HapcHpc_HapHap(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHap, double MHp[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HapcHpc_HapHp(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHapHpcHapcHp[4][4], double complex cplHapHhSMcHap[4], double complex cplHhSMHpcHp[4][4][4], double MHhSM[4], double complex cplhhaHpcHap[4], double complex cplHaphhacHp[4], double Mhha, double complex cplHpsigacHap[4], double complex cplHapsigacHp[4], double Msiga, double MHap, double MHp[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HapcHpc_HpHp(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHap, double MHp[4]); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HpcHpc_HapHap(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHapHapcHpcHp[4][4], double complex cplHaphhacHp[4], double Mhha, double complex cplHapsigacHp[4], double Msiga, double MHp[4], double MHap); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HpcHpc_HapHp(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double MHp[4], double MHap); 



       /* ----------------------------------------------------------------------------------------------------- */


double complex a0_HpcHpc_HpHp(int *Pole_Present, int RemoveTUpoles[99], const int* myTUcut, const double* ss, const int* ii1, const int* ii2, const int* ii3, const int* ii4, const int* iind1, const int* iind2, double complex cplHpHpcHpcHp[4][4][4][4], double complex cplAhHpcHp[4][4][4], double MAh[4], double complex cplHhSMHpcHp[4][4][4], double MHhSM[4], double MHp[4]); 

/* End module DarkS3 */
