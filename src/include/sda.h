

void odeSDA(double ystart[], int nvar, double x1, double x2, double eps, double h1,
	double hmin, int *nok, int *nbad,
	void (*derivs)(double, double [], double [], double [], double **, double **, 
	          double [], double [], double),
	void (*rkqsSDA)(double [], double [], int, double *, double, double, double [],
	double *, double *, void (*)(double, double [], double [], double [], double **, double **, 
	          double [], double [], double), 
	double *, double [], double **, double **, 
	          double [], double [], double),
	void (*stiffSDA)(double [], double [], int, double *, double, double, double [],
	double *, double *, void (*)(double, double [], double [], double [], double **, double **, 
	          double [], double [], double), 
	double *, double [], double **, double **, 
	          double [], double [], double), 
	double lambdas[], double **ZH, double **ZA, 
	          double Mdm[], double Mlight[], double theta);
void stiffSDA(double y[], double dydx[], int n, double *x, double htry, double eps,
	double yscal[], double *hdid, double *hnext,
	void (*derivs)(double, double [], double [], double [], double **, double **, 
	          double [], double [], double), 
	double *lambda, double lambdas[], double **ZH, double **ZA, 
	          double Mdm[], double Mlight[], double theta);
void jacobn(double x, double y[], double dfdx[], double **dfdy, int n, 
	          double lambdas[], double **ZH, double **ZA, double Mdm[], double Mlight[], double theta);
void derivs(double x, double y[], double dydx[], double lambdas[], double **ZH, double **ZA, 
	          double Mdm[], double Mlight[], double theta);
void rkqsSDA(double y[], double dydx[], int n, double *x, double htry, double eps,
	double yscal[], double *hdid, double *hnext,
	void (*derivs)(double, double [], double [], double [], double **, double **, 
	          double [], double [], double), 
	double *lambda, double lambdas[], double **ZH, double **ZA, 
	          double Mdm[], double Mlight[], double theta);
void rkckSDA(double y[], double dydx[], int n, double x, double h, double yout[],
	double yerr[], void (*derivs)(double, double [], double [], double [], double **, double **, 
	          double [], double [], double), 
	double *lambda, double lambdas[], double **ZH, double **ZA, 
	          double Mdm[], double Mlight[], double theta);
void lubksb(double **a, int n, int *indx, double b[]);
void ludcmp(double **a, int n, int *indx, double *d);
void balanc(double **a, int n);
void elmhes(double **a, int n);
void hqr(double **a, int n, double wr[], double wi[]);
double VNRij(int j, double x, double lambdas[], double **ZH, double **ZA, double Mdm[], double Mlight[],
	         double theta);
double diffVNRij(int j, double x, double lambdas[], double **ZH, double **ZA, double Mdm[], double Mlight[],
	         double theta);
void eigsrt(double d[], double **v, int n);
void jacobi(double **a, int n, double d[], double **v, int *nrot);
