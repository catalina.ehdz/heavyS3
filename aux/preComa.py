#! /home/catalina/anaconda2/envs/ctaenv/bin/python
# ==========================================================================

import gammalib
import ctools
import cscripts

obssim = ctools.ctobssim()
obssim['ra']        = 12.45
obssim['dec']       = 24.80
obssim['rad']       = 5
obssim['tmin']      = 0
obssim['tmax']      = 72000
obssim['emin']      = 0.1
obssim['emax']      = 100.0
obssim['edisp']     = True                 # Set to True (is False by default)
obssim['caldb']     = 'prod3b-v2'
obssim['irf']       = 'South_z40_5h'
obssim['inmodel']   = 'CTA_ComaNull.xml'
obssim['outevents'] = 'eve_Coma0.fits'
obssim['nthreads']  = 1
obssim['chatter']   = 0
obssim.execute()

select = ctools.ctselect()
select['inobs']     = 'eve_Coma0.fits'
select['outobs']    = 'eve_Coma.fits'
select['ra']        = 12.45
select['dec']       = 24.80
select['rad']       = 3
select['tmin']      = 'NONE'
select['tmax']      = 'NONE'
select['emin']      = 'NONE'
select['emax']      = 'NONE'
select['chatter']   = 0
select.execute()

evbin = ctools.ctbin()
evbin['inobs']    = 'eve_Coma.fits'
evbin['xref']     = 12.45  # deg; we center the cube at the position of the 
evbin['yref']     = 24.80  # deg
evbin['proj']     = 'CAR'
evbin['coordsys'] = 'CEL'
evbin['binsz']    = 0.1   # deg/bin
evbin['nxpix']    = 30
evbin['nypix']    = 30
evbin['ebinalg']  = 'LOG'
evbin['emin']     = 0.1    # TeV
evbin['emax']     = 100.0  # TeV
evbin['enumbins'] = 40
evbin['outobs']   = 'cubeComa.fits'
evbin['chatter']  = 0
evbin['nthreads'] = 1
evbin.execute()

expcube = ctools.ctexpcube()
expcube['inobs']    = 'eve_Coma.fits'
expcube['caldb']    = 'prod3b-v2'
expcube['irf']      = 'South_z40_5h'
expcube['incube']   = 'cubeComa.fits'  # exposure cube definition is copied from counts cube
expcube['outcube']  = 'expComa.fits'
expcube['chatter']  = 0
expcube.execute()

psfcube = ctools.ctpsfcube()
psfcube['inobs']     = 'eve_Coma.fits'
psfcube['caldb']     = 'prod3b-v2'
psfcube['irf']       = 'South_z40_5h'
psfcube['incube']    = 'NONE'
psfcube['xref']      = 12.45
psfcube['yref']      = 24.80
psfcube['proj']      = 'CAR'
psfcube['coordsys']  = 'CEL'
psfcube['binsz']     = 1.0   # deg/bin; the PSF only varies slowly
psfcube['nxpix']     = 10
psfcube['nypix']     = 10
psfcube['ebinalg']   = 'LOG'
psfcube['emin']      = 0.1
psfcube['emax']      = 100.0
psfcube['enumbins']  = 40
psfcube['outcube']   = 'psfComa.fits'
psfcube['chatter']   = 0
psfcube.execute()

edispcube = ctools.ctedispcube()
edispcube['inobs']     = 'eve_Coma.fits'
edispcube['caldb']     = 'prod3b-v2'
edispcube['irf']       = 'South_z40_5h'
edispcube['incube']    = 'NONE'
edispcube['xref']      = 12.45
edispcube['yref']      = 24.80
edispcube['proj']      = 'CAR'
edispcube['coordsys']  = 'CEL'
edispcube['binsz']     = 1.0   # deg/bin; the energy dispersion only varies slowly
edispcube['nxpix']     = 10
edispcube['nypix']     = 10
edispcube['ebinalg']   = 'LOG'
edispcube['emin']      = 0.1
edispcube['emax']      = 100.0
edispcube['enumbins']  = 40
edispcube['outcube']   = 'edispComa.fits'
edispcube['chatter']   = 0
edispcube.execute()

bkgcube = ctools.ctbkgcube()
bkgcube['inobs']     = 'eve_Coma.fits'
bkgcube['caldb']     = 'prod3b-v2'
bkgcube['irf']       = 'South_z40_5h'
bkgcube['incube']    = 'cubeComa.fits'
bkgcube['inmodel']   = 'CTA_ComaNull.xml'
bkgcube['outcube']   = 'bkgComa.fits'
bkgcube['outmodel']  = 'modelComa.xml'
bkgcube['chatter']   = 0
bkgcube.execute()


