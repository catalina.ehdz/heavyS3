#! /home/catalina/anaconda2/envs/ctaenv/bin/python
# ==========================================================================

import gammalib
import ctools
import cscripts

bkgcube = ctools.ctbkgcube()
bkgcube['inobs']     = 'eve_Coma.fits'
bkgcube['caldb']     = 'prod3b-v2'
bkgcube['irf']       = 'South_z40_5h'
bkgcube['incube']    = 'cubeComa.fits'
bkgcube['inmodel']   = 'CTA_ComaNull.xml'
bkgcube['outcube']   = 'bkgComa.fits'
bkgcube['outmodel']  = 'modelComa.xml'
bkgcube['chatter']   = 0
bkgcube.execute()

