#! /home/catalina/anaconda2/envs/ctaenv/bin/python
# ==========================================================================

import gammalib
import ctools
import cscripts


phagen = cscripts.csphagen()
phagen['inobs']      = 'eve_Coma.fits'     
phagen['caldb']      = 'prod3b-v2'
phagen['irf']        = 'South_z40_5h'
phagen['inmodel']    = 'CTA_Coma.xml'
phagen['srcname']    = 'Coma'
phagen['ebinalg']    = 'LOG'
phagen['emin']       = 0.1
phagen['emax']       = 100.0
phagen['enumbins']   = 40
phagen['coordsys']   = 'CEL'
phagen['ra']         = 12.45
phagen['dec']        = 23.90
phagen['rad']        = 0.3
phagen['outobs']     = 'onoff_obs.xml'
phagen['outmodel']   = 'onoff_model.xml'
phagen['bkgmethod']  = 'REFLECTED'
phagen['stack']      = False
phagen['nthreads']   = 1
phagen['debug']      = 0
phagen['chatter']    = 0
phagen.execute()

like = ctools.ctlike()   
like['caldb']      = 'prod3b-v2'
like['irf']        = 'South_z40_5h'
like['inobs']      = 'onoff_obs.xml'
like['inmodel']    = 'onoff_model.xml'
like['expcube']    = 'expComa.fits'
like['psfcube']    = 'psfComa.fits'
like['bkgcube']    = 'bkgComa.fits'
like['edisp']      = True                 # Set to True (is False by default) 
like['edispcube']  = 'edispComa.fits'     # Provide energy dispersion cube
like['outmodel']   = 'Coma_res.xml'
like['nthreads']   = 1
like['debug']      = 0
like['chatter']    = 0
like.execute()



like1 = -1*like.opt().value()

# opt().status() = 0 ---> means "converged" 
statusF = like.opt().status()

models = like.obs().models()
model  = models["Coma"]
tsF  = model.ts()

print(statusF)
print(like1)
print(tsF)

